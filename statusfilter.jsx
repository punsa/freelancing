import React, { Component } from "react";
import ReactDOM from "react-dom";
import Dropdown from 'react-dropdown';
//import Select from 'react-select';
import 'react-dropdown/style.css';


export default class StatusFilter extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            selectedOption: '',
            statusOptions: props.filterOptions
        };

        this.valueGetter = this.props.valueGetter;
        //filterResetCallback
    }

    isFilterActive() {
        return this.state.selectedOption !== null && this.state.selectedOption !== undefined && this.state.selectedOption !== '';
    }

    getModel() {
        let val = this.state.selectedOption.value == 'CLEAR FILTER' ? '' : this.state.selectedOption.value;
        return {filterType: 'text', type: 'equals', filterkey: this.props.filterKey, filter: val};
    }

    setModel(model) {
        return '';
    }

    afterGuiAttached(params) {
        this.focus();
    }

    focus() {
        setTimeout(() => {
            let container = ReactDOM.findDOMNode(this.refs.input);
            if (container) {
                container.focus();
            }
        })
    }

    handleChange = (selectedOption) => {
        this.setState({selectedOption: selectedOption});
        if (selectedOption.value == 'CLEAR FILTER') {
            this.props.filterResetCallback();
            return;
        }
        this.props.filterChangedCallback();
    }

    render() {
        let style = {
            fontSize: "11px",
            width: "200px",
            height: "270px",
            overflowX: "hidden"
        };

        return (
            <div style={style}>
                <div style={{paddingLeft: '9px', paddingTop: '5px'}}>
                    Filter:
                </div>
                {/*<image style={{paddingLeft:'104px'}} className="mlp-cursor-pointer" onClick={this.handleChange}>CLEAR ALL</image>*/}
                <Dropdown name="form-field-name" value={this.state.selectedOption} onChange={this.handleChange}
                          options={this.state.statusOptions}/>
            </div>
        );
    }
}