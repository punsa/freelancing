/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import Api from '../services/Api';
import Loader from './loaderpopup';
import './UsecaseForm.css';

class UsecaseForm extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      useCaseForm: props.data,
      isNew: props.isNew,
      isFormOk: (props.isNew === 'false'),
      loading: false,
      validationObj: this.getValidationObj(props.data, props.isNew),
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    document.body.style.overflow = 'hidden';
  }

  componentWillReceiveProps = (nextProps) => {
    this.setState({
      useCaseForm: nextProps.data,
      validationObj: this.getValidationObj(nextProps.data),
    });
  }

  componentWillUnmount() {
    document.body.style.overflow = 'auto';
  }


    getValidationObj = (obj, isNew = true) => {
      const keys = Object.keys(obj);
      const validationObj = {};
      const touched = (isNew === 'false');
      for (let i = 0; i < keys.length; i += 1) {
        validationObj[keys[i]] = {
          touched,
          error: false,
          errorMessage: '',
        };
      }
      return validationObj;
    }

    submitActivate = (usecaseForm) => {
      const { validationObj } = this.state;
      const tmpObj = validationObj;
      const keys = Object.keys(tmpObj);
      const value = Object.values(usecaseForm);
      let flag = true;
      for (let i = 0; i < keys.length; i += 1 ) {
        if (!(tmpObj[keys[i]].touched === true && tmpObj[keys[i]].error === false && value[i])) {
          flag = false;
        }
      }
      this.setState({ isFormOk: flag });
    }


    validateusecaseName = (event, tmpValidationObj) => {
      const format = /[^a-zA-Z0-9\-\/]/;
      const { value } = event.target;
      const validationObj = tmpValidationObj;
      if (format.test(value) || value.length >= 50) {
        validationObj[event.target.name].error = true;
        validationObj[event.target.name].errorMessage = 'Special characters are not allowed';
      } else {
        validationObj[event.target.name].error = false;
        validationObj[event.target.name].errorMessage = '';
      }
    }

    validateOwnerEmailId = (event, tmpValidationObj) => {
      const ucOwnerEmailIdformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      const validationObj = tmpValidationObj;
      if (ucOwnerEmailIdformat.test(event.target.value) === false) {
        validationObj[event.target.name].error = true;
        validationObj[event.target.name].errorMessage = 'Invalid email';
      } else {
        validationObj[event.target.name].error = false;
        validationObj[event.target.name].errorMessage = '';
      }
    }

    validateTotalTrainingHours = (event, tmpValidationObj) => {
      const format = /[^a-zA-Z0-9\-\/]/;
      const validationObj = tmpValidationObj;
      const { value } = event.target;
      if (format.test(value) || value.length >= 4) {
        validationObj[event.target.name].error = true;
        validationObj[event.target.name].errorMessage = 'Total training hours should be 4 digits.';
      } else {
        validationObj[event.target.name].error = false;
        validationObj[event.target.name].errorMessage = '';
      }
    }

    validateModelIterations = (event, tmpValidationObj) => {
      const validationObj = tmpValidationObj;
      if (event.target.value.length >= 4) {
        validationObj[event.target.name].error = true;
        validationObj[event.target.name].errorMessage = 'Model iterations should be 4 digits.';
      } else {
        validationObj[event.target.name].error = false;
        validationObj[event.target.name].errorMessage = '';
      }
    }

    validateInstanceType = (event, tmpValidationObj) => {
      const validationObj = tmpValidationObj;
      if (event.target.value.length >= 4) {
        validationObj[event.target.name].error = true;
        validationObj[event.target.name].errorMessage = 'Instance type should be 4 digits.';
      } else {
        validationObj[event.target.name].error = false;
        validationObj[event.target.name].errorMessage = '';
      }
    }

    validateComments = (event, tmpValidationObj) => {
      const validationObj = tmpValidationObj;
      if (event.target.value.length >= 250) {
        validationObj[event.target.name].error = true;
        validationObj[event.target.name].errorMessage = 'Comments shouldn\'t exceed more than 250 characters.';
      } else {
        validationObj[event.target.name].error = false;
        validationObj[event.target.name].errorMessage = '';
      }
    }

    validateAllocatedBudget = (event, tmpValidationObj) => {
      const validationObj = tmpValidationObj;
      if (event.target.value.length >= 8) {
        validationObj[event.target.name].error = true;
        validationObj[event.target.name].errorMessage = 'Allocated budget should be 4 digits.';
      } else {
        validationObj[event.target.name].error = false;
        validationObj[event.target.name].errorMessage = '';
      }
    }


    validation = (event) => {
      const { validationObj } = this.state;
      const tmpValidationObj = validationObj;
      if (!tmpValidationObj[event.target.name]) {
        tmpValidationObj[event.target.name] = {};
      }
      tmpValidationObj[event.target.name].touched = true;
      switch (event.target.name) {
        case 'useCaseName':
          this.validateusecaseName(event, tmpValidationObj);
          break;
        case 'ownerEmailId':
          this.validateOwnerEmailId(event, tmpValidationObj);
          break;
        case 'totalTrainingDetails':
          this.validateTotalTrainingHours(event, tmpValidationObj);
          break;
        case 'noOfMdlngIterations':
          this.validateModelIterations(event, tmpValidationObj);
          break;
        case 'instanceType':
          this.validateInstanceType(event, tmpValidationObj);
          break;
        case 'comments':
          this.validateComments(event, tmpValidationObj);
          break;
        case 'allocatedBudget':
          this.validateAllocatedBudget(event, tmpValidationObj);
          break;
        default:
          break;
      }
      this.setState({ validationObj: tmpValidationObj });
    }

    onSubmitClick = () => {
      const { useCaseForm } = this.state;
      const { userInfo } = this.props;
      const data = {
        seqId: useCaseForm.seqId ? useCaseForm.seqId : undefined,
        leaderEmailId: useCaseForm.seqId ? useCaseForm.leaderEmailId : undefined,
        ownerEmailId: useCaseForm.ownerEmailId,
        useCaseName: useCaseForm.useCaseName,
        noOfMdlngIterations: useCaseForm.noOfMdlngIterations,
        totalTrainingDetails: useCaseForm.totalTrainingDetails,
        instanceType: useCaseForm.instanceType,
        comments: useCaseForm.comments,
        allocatedBudget: useCaseForm.allocatedBudget,
        createdByUserAdsId: userInfo.adsId.replace('@ads.aexp.com', ''),
        createdByUserEmailId: userInfo.emailId,
      };
      this.setState({ loading: true });
      // eslint-disable-next-line no-undef
      const origin = APP_CONFIG.MLP_ORCH_API_URL;
      if (useCaseForm.seqId && useCaseForm.seqId !== '' ) {
        const url = `${origin}/api/v1/usecase/updateusecasecostrecords`;
        const apiConf = {
          url,
          method: 'PUT',
          data,
        };

        Api.Service(apiConf)
          .then((res) => {
            if (!res.success) {
              const { errorInfo } = this.state;
              this.setState({ statusPopup: true, errorInfo: res.message, loading: false });
              this.handleClose(0, errorInfo);
            } else {
              this.handleClose(2);
            }
          })
          .catch((error) => {
            console.error(`AJAX error occured :${error}`);
          });
      } else {
        const url = `${origin}/api/v1/usecase/insertusecasecostrecords`;
        const apiConf = {
          url,
          method: 'POST',
          data,
        };

        Api.Service(apiConf)
          .then((res) => {
            if (!res.success) {
              const { errorInfo } = this.state;
              this.setState({ errorInfo: res.message, loading: false });
              this.handleClose(0, errorInfo);
            } else {
              this.handleClose(1);
            }
          })
          .catch((error) => {
            const { errorInfo } = this.state;
            console.error(`AJAX error occured :${error}`);
            this.setState({ errorInfo: error, loading: false });
            this.handleClose(0, errorInfo);
          });
      }
    }

    onChangeValue = (event) => {
      const { useCaseForm } = this.state;
      this.validation(event);
      const tmpObj = useCaseForm;
      tmpObj[event.target.name] = event.target.value;
      this.setState({ useCaseForm: tmpObj });
      this.submitActivate(tmpObj);
    }


    handleClickOutside = (event) => {
      if (this.modalRef && !this.modalRef.contains(event.target)) {
        this.handleClose();
      }
    }

    handleClose = (status, errorMessage) => {
      const { onClose } = this.props;
      onClose(status, errorMessage);
    }

    setModalRef = (node) => {
      this.modalRef = node;
    }

    render() {
      const {
        loading, validationObj, useCaseForm, isNew, isFormOk,
      } = this.state;
      return (
        <div className="clusterInfoDiv">
          <div className="modal-screen" style={{ zIndex: 10 }} />
          <div className="hidden-sm-down position-relative">
            <div
              className="modal"
              style={{
                zIndex: 10, top: '5%', left: '320px', width: '50%',
              }}
            >
              {loading && <Loader />}
              {!loading && (
              <div className="container" ref={this.setModalRef}>
                <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                  <div className="mlp-modal-heading">
                    <h2 className="mlp-no-margin pad-std heading-3">
                      {isNew.toString() === 'true' ? 'Create New' : 'Edit'}
                      {' '}
                      AWS Usecase
                    </h2>
                  </div>
                  <div className="pad-std mlp-modal-close">
                    <span
                      onClick={() => { this.handleClose(4); }}
                      data-toggle="tooltip-auto"
                      title="Close"
                      className="mlp-cursor-pointer dls-glyph-close"
                    />
                  </div>
                </div>
                <div className="usecase-form background-white">
                  {/* name */}
                  <div className="row">
                    <div className={`col-md-6 ${validationObj.ownerEmailId.error ? 'has-warning' : ''}`}>
                      <label htmlFor="email">Director</label>
                      <input
                        type="text"
                        id="email"
                        name="ownerEmailId"
                        placeholder="Enter valid email address"
                        value={useCaseForm.ownerEmailId}
                        onChange={this.onChangeValue}
                        className={`form-control ${validationObj.ownerEmailId.error ? 'form-control-warning' : ''}`}
                      />
                      <span
                        className={validationObj.ownerEmailId.error ? 'block' : 'hide'}
                      >
                        {validationObj.ownerEmailId.errorMessage}
                      </span>
                    </div>

                    <div
                      className={`col-md-6 ${validationObj.useCaseName.error ? 'has-warning' : ''}`}
                    >
                      <label htmlFor="name">Use Case</label>
                      <input
                        type="text"
                        id="name"
                        name="useCaseName"
                        placeholder="Enter usecase name"
                        value={useCaseForm.useCaseName}
                        className={`form-control ${validationObj.useCaseName.error ? 'form-control-warning' : ''}`}
                        onChange={this.onChangeValue}
                      />
                      <span
                        className={validationObj.useCaseName.error ? 'block' : 'hide'}
                      >
                        {validationObj.useCaseName.errorMessage}
                      </span>
                    </div>

                  </div>

                  <div className="row">
                    <div
                      className={`col-md-6 ${validationObj.totalTrainingDetails.error ? 'has-warning' : ''}`}
                    >
                      <label htmlFor="totalTrainingDetails">Total training hours</label>
                      <input
                        type="text"
                        id="totalTrainingDetails"
                        name="totalTrainingDetails"
                        placeholder="Enter details eg: 50 (10hours/Iteration)"
                        value={useCaseForm.totalTrainingDetails}
                        className={`form-control ${validationObj.totalTrainingDetails.error ? 'form-control-warning' : ''}`}
                        onChange={this.onChangeValue}
                      />
                      <span
                        className={validationObj.totalTrainingDetails.error ? 'block' : 'hide'}
                      >
                        {validationObj.totalTrainingDetails.errorMessage}
                      </span>
                    </div>


                    <div
                      className={`col-md-6 ${validationObj.noOfMdlngIterations.error ? 'has-warning' : ''}`}
                    >
                      <label htmlFor="noOfMdlngIterations"># of modeling iterations</label>
                      <input
                        type="text"
                        id="noOfMdlngIterations"
                        name="noOfMdlngIterations"
                        placeholder="Enter modeling Iterations eg: 100"
                        value={useCaseForm.noOfMdlngIterations}
                        className={`form-control ${validationObj.noOfMdlngIterations.error ? 'form-control-warning' : ''}`}
                        onChange={this.onChangeValue}
                      />
                      <span
                        className={validationObj.noOfMdlngIterations.error ? 'block' : 'hide'}
                      >
                        {validationObj.noOfMdlngIterations.errorMessage}
                      </span>
                    </div>
                  </div>
                  <div className="row">

                    <div
                      className={`col-md-6 ${validationObj.instanceType.error ? 'has-warning' : ''}`}
                    >
                      <label htmlFor="instanceType">Instance types</label>
                      <input
                        type="text"
                        id="instanceType"
                        name="instanceType"
                        placeholder="Enter Instance type eg: m4.16x large"
                        value={useCaseForm.instanceType}
                        className={`form-control ${validationObj.instanceType.error ? 'form-control-warning' : ''}`}
                        onChange={this.onChangeValue}
                      />
                      <span
                        className={validationObj.instanceType.error ? 'block' : 'hide'}
                      >
                        {validationObj.instanceType.errorMessage}
                      </span>
                    </div>
                    <div
                      className={`col-md-6 ${validationObj.allocatedBudget.error ? 'has-warning' : ''}`}
                    >
                      <label htmlFor="allocatedBudget">Allocated Budget</label>
                      <input
                        type="text"
                        id="allocatedBudget"
                        name="allocatedBudget"
                        placeholder="Enter budget in '$'"
                        value={useCaseForm.allocatedBudget}
                        className={`form-control ${validationObj.allocatedBudget.error ? 'form-control-warning' : ''}`}
                        onChange={this.onChangeValue}
                      />
                      <span
                        className={validationObj.allocatedBudget.error ? 'block' : 'hide'}
                      >
                        {validationObj.allocatedBudget.errorMessage}
                      </span>
                    </div>
                  </div>

                  <div className="row">
                    <div
                      className={`col-md-6 ${validationObj.comments.error ? 'has-warning' : ''}`}
                    >
                      <label htmlFor="comments">Comments</label>
                      <input
                        type="text"
                        id="comments"
                        name="comments"
                        placeholder="Enter comments within 250 characters..."
                        value={useCaseForm.comments}
                        className={`form-control ${validationObj.comments.error ? 'form-control-warning' : ''}`}
                        onChange={this.onChangeValue}
                      />
                      <span
                        className={validationObj.comments.error ? 'block' : 'hide'}
                      >
                        {validationObj.comments.errorMessage}
                      </span>
                    </div>
                  </div>

                  <div className="row" style={{ marginTop: '20px' }}>
                    <div className="col-md-3" />
                    <div className="col-md-4">
                      <button className="btn btn-primary" type="button" style={{ marginLeft: '-92px' }} onClick={() => { this.handleClose(4); }}>Cancel</button>
                    </div>
                    <div className="col-md-3">
                      <button
                        className="btn btn-primary"
                        type="button"
                        onClick={this.onSubmitClick}
                        disabled={!isFormOk}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              )}
            </div>
          </div>
        </div>
      );
    }
}

UsecaseForm.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  isNew: PropTypes.string.isRequired,
  userInfo: PropTypes.objectOf(PropTypes.any).isRequired,
  onClose: PropTypes.func.isRequired,
};

export default UsecaseForm;
