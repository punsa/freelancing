import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import ClusterGrid from './containers/ClusterGrid.jsx';
import ScoreCard from './containers/ScoreCard.jsx';
import DataManagement from './containers/DataManagement.jsx';
import InstancesCard from './components/InstancesCard.jsx';
import HomeMenu from './components/HomeMenu.jsx';
import AdminMenu from './components/AdminMenu.jsx';
import Footer from './components/Footer.jsx'
import Usecase from "./containers/Usecase.jsx";
import Dashboard from "./containers/Dashboard.jsx";
import MLPHubLoading from "./components/MLPHubLoading.jsx";
import DmUserActions from "./containers/dmuseractions.jsx";
import ManagerActions from "./containers/manageractions.jsx";
import Services from "./services/Services.js";
import appConfig from "./config/web-config";


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data:"",
          cardSubMenu: false,
          userInfo: {
              username:"",
              emailId: "",
              firstName: "",
              lastName: "",
              role: [],
              hasAdminRole: false,
              hasNormalRole: false,
              mlphubLoadingActive: false
          },
          usecaseComponent: <Usecase />
        }
    }

    componentDidMount() {
        var parent = this;
        Services.getUserInfo()
            .then(function (response) {
                var userObj = parent.state.userInfo;
                userObj["username"] = response["data"]["username"];
                userObj["adsId"] = response["data"]["username"] + "@ads.aexp.com";
                userObj["firstName"] = response["data"]["firstName"];
                userObj["lastName"] = response["data"]["lastName"];
                userObj["emailId"] = response["data"]["emailId"];
                userObj["role"] = response["data"]["role"];
                if(userObj && userObj.role && userObj.role.includes("Admin")){
                  userObj.hasAdminRole = true;
                }else{
                  userObj.hasNormalRole = true;
                }
                parent.setState({ userInfo: userObj, usecaseComponent: <Usecase userInfo={userObj} /> });
            })
            .catch(function (error) {})
    }

    logout = () => {
        Services.getLogout().then(function(){
          window.location = '/logout';
        }).catch(function (error){})
    }
    //
    // app = () => {
    //     window.location = '/dashboard';
    // }

    closeMenu = () => {
    this.setState({
      cardSubMenu: false
    }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }


   cardSubMenu = (event) => {
    event.preventDefault();

    this.setState({
      cardSubMenu: true
    }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

    render() {
        return (
            <BrowserRouter>
                <div>
                 <div className="nav nav-header nav-large nav-sticky nav-inverse dls-accent-blue-02-bg"
                        role="navigation" data-toggle="nav" aria-current="horizontal">
                        <div className="nav-overlay"></div>
                        <div className="nav-brand" style={{paddingBottom: "0.5rem", paddingTop: "0.5rem"}}>
                            {/* <span className="dls-logo-horizontal-white dls-logo-horizontal-md"></span> */}
                            <span style={{"width" : "350px","marginTop" : "0px", cursor: "pointer"}}>
                          <img src="/public/images/mlp-logo.svg" /></span>
                        </div>
                        <div className="nav-shadow"></div>
                        <ul className="nav-menu">
                          {!this.state.mlphubLoadingActive && <li className="nav-item" role="presentation">
                              <Link to="/app/home" className="nav-link" aria-selected={location.href.indexOf("/app/home") !== -1 ? "true" : "false"}>Home</Link>
                          </li>}
                            {!this.state.mlphubLoadingActive && this.state.userInfo && this.state.userInfo.hasAdminRole &&
                                (<li className="nav-item" role="presentation">
                                    <Link to="/app/admin/clusters" className="nav-link" aria-selected={location.href.indexOf("/app/admin/clusters") !== -1 ? "true" : "false" &&
                                        location.href.indexOf("/app/admin/scorecard") !== -1 ? "true" : "false"
                                            && location.href.indexOf("/app/admin/datamanagement") !== -1 ? "true" : "false"}>Admin</Link>
                                </li>)
                            }
                            {/* {(location.href.indexOf("/app/home") < 0 ) && this.state.userInfo && (this.state.userInfo.hasNormalRole || this.state.userInfo.hasAdminRole) &&
                                (<li className="nav-item" role="presentation">
                                    <Link to="/app/usecase" className="nav-link" aria-selected={location.href.indexOf("/app/usecase") !== -1 ? "true" : "false"}>UseCase</Link>
                                </li>)
                            } */}
                            {/* <li className="nav-item" role="presentation">
                                <Link to="/app/help/aboutus" className="nav-link" aria-selected={location.href.indexOf("/app/help/aboutus") !== -1 ? "true" : "false"}>Help</Link>
                            </li> */}
                            {!this.state.mlphubLoadingActive && <li className="nav-item" role="presentation">
                                {/* <button className="nav-link btn-sm" onClick={this.logout}>Logout</button> */}
                                <Link to="#" className="nav-link"  onClick={this.logout}>Logout</Link>
                        </li> }
                        </ul>

                </div>
                    {this.state.userInfo && (this.state.userInfo.hasAdminRole || this.state.userInfo.hasNormalRole) &&
                    (<Route path="/app/home" render={(props) => <Dashboard {...props} userinfo={this.state.userInfo}/>}/>)}
                    {this.state.userInfo && (this.state.userInfo.hasAdminRole || this.state.userInfo.hasNormalRole) &&
                    (<Route path="/app/datamanagement/actions/:id" render={(props) => <DmUserActions {...props} userinfo={this.state.userInfo}/>}/>)}
                     {this.state.userInfo && (this.state.userInfo.hasNormalRole || this.state.userInfo.hasAdminRole) &&
                    (<Route path="/app/manager/actions" render={(props) => <ManagerActions {...props} userinfo={this.state.userInfo}/>}/>   )}
                    {/* {this.state.userInfo && (this.state.userInfo.hasAdminRole || this.state.userInfo.hasNormalRole) &&
                        (<Route path="/app/home" render={(props) => <HomeMenu {...props} userinfo={this.state.userInfo} />} />)} */}
                    
                    {this.state.userInfo &&  this.state.userInfo.hasAdminRole &&
                    <Route path="/app/admin/clusters"  render={(props) => <ClusterGrid {...props} userinfo={this.state.userInfo}  />} /> }
                    <Route path="/app/admin/scorecard"  component={ScoreCard} />
                    {this.state.userInfo && (this.state.userInfo.hasAdminRole || this.state.userInfo.hasNormalRole) &&
                        (<Route path="/app/user/clusters" render={(props) => <ClusterGrid {...props} isHomeScreen={true} userinfo={this.state.userInfo} tab={"0"} />} />)}
                     {this.state.userInfo && (this.state.userInfo.hasAdminRole || this.state.userInfo.hasNormalRole) &&
                        (<Route path="/app/user/datatransfer" render={(props) => <DataManagement {...props}
                            isHomeScreen={true} userinfo={this.state.userInfo} tab={"1"} />} />)}
                    <Route path="/app/admin/datamanagement" render={(props) => <DataManagement {...props} userInfo={this.state.userInfo} />} />
                    <Route path="/app/mlphub/:clusterId/:projectName" render={(props) => <MLPHubLoading {...props} userInfo={this.state.userInfo} updateMLPHubLoadingActive={() => { this.setState({ mlphubLoadingActive: true }) } } />} />
                    {/* <Route path="/app/help" component={Help} /> */}
                    {/* <Route path="/app/help/aboutus" component={AboutUs} /> */}
                    {this.state.userInfo && (this.state.userInfo.hasNormalRole || this.state.userInfo.hasAdminRole) &&
                        (<Route path="/app/admin/usecase" render={(props) => <Usecase {...props} userInfo={this.state.userInfo} />} />
                        )}
                        {this.state.userInfo && (this.state.userInfo.hasAdminRole || this.state.userInfo.hasNormalRole) &&
                        (<Route path="/app/user/usecase" render={(props) => <Usecase {...props}
                            isHomeScreen={true} userinfo={this.state.userInfo} tab={"2"} />} />)}
                        <Footer/>
                      </div>
            </BrowserRouter>
        )
    }
}
//console.log("hi" + document.getElementById('app'));
//ReactDOM.render(<Apps />, document.getElementById('app'));
