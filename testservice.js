'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const logger = require('winston')
const appConfig = require('./../config/app-config');
var cryptoSvc = require('./cryptoService');
const env = process.env.EPAAS_ENV || 'e0';

router.get('/userinfo', function (req, res) {
    try {
      console.log(req.headers);
      res.status('200').send(req.headers);
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});

router.get('/crypto', function (req, res) {
    try {
      if(env === 'e0'){
        let action = req.body.action;
        let secretkey = req.body.secretkey;
        let key = req.body.key;

        if(action === 'encrypt'){
          let ciphertext = cryptoSvc.encryptByEnv(key,secretkey);
          res.status('200').send(key + "  "+ ciphertext);
        }else if(action === 'decrypt'){
          let deciphertext = cryptoSvc.decryptByEnv(key,secretkey);
          res.status('200').send(key + "  "+ deciphertext);
        }else{
          res.status('500').send("No matching action !!");
        }
      } else{
        res.status('200').send("Its Working !!");
      }
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});

module.exports = router
