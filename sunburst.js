var a = { "name": "clusters", "children": [{ "name": "2018-06-13", "children": [{ "name": "EMR", "size": 6, "children": [{ "name": "USER", "size": 5 }, { "name": "AWS", "size": 1 }] }] }, { "name": "2018-06-14", "children": 0 }, { "name": "2018-06-15", "children": 0 }, { "name": "2018-06-16", "children": [{ "name": "EMR", "size": 4, "children": [{ "name": "USER", "size": 4 }] }] }, { "name": "2018-06-17", "children": [{ "name": {}, "size": 1, "children": [{ "name": "USER", "size": 1 }] }, { "name": "EC2", "size": 1, "children": [{ "name": "USER", "size": 1 }] }, { "name": "EMR", "size": 5, "children": [{ "name": "AWS", "size": 1 }, { "name": "USER", "size": 4 }] }] }, { "name": "2018-06-18", "children": [{ "name": {}, "size": 2, "children": [{ "name": "USER", "size": 2 }] }, { "name": "EMR", "size": 1, "children": [{ "name": "USER", "size": 1 }] }] }] }

function formatter(obj) {
    var result = {};
    var keys = Object.keys(obj);
    if(obj["name"] == "SYSTEM" || obj["name"] == "USER" || obj["name"] == "AWS") {
        return obj;
    }
    else {
        for(var i=0;i<keys.length;i++) {
            switch(keys[i]) {
                case "name":
                result["name"] = (obj["name"] == {}) ? "" : obj["name"];
                break;
                case "size":
                break;
                case "children":
                result["children"] = [];
                for(var j=0;j<obj["children"].length;j++) {
                    result["children"][j] = formatter(obj["children"][j]);
                }
            }
        }
        return result;
    }
}