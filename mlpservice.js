'use strict'

const request = require('request');
const mlpLogger = require('./loggerService');
const crypto = require('./cryptoService');
const appConfig = require('./../config/app-config');
const authService = require('./authService');
const moment = require('moment');
const logger = mlpLogger.createLogger();
const fs = require('fs');
const env = process.env.EPASS || 'e0';

var privateKey, certificate, pass;

privateKey = fs.readFileSync(appConfig.MLP_API_AUTH_KEY);
certificate = fs.readFileSync(appConfig.MLP_API_AUTH_CERT);
pass = fs.readFileSync(appConfig.MLP_API_AUTH_KEY_PASS, 'ascii');


function postCall(endpoint, token, payload, callback) {
  executeRequest(endpoint, 'POST', token, payload, callback)
}

function putCall(endpoint, token, payload, callback) {
  executeRequest(endpoint, 'PUT', token, payload, callback)
}

function getCall(endpoint, token, callback) {
  executeRequest(endpoint, 'GET', token, {}, callback)
}

function deleteCall(endpoint, token, payload, callback) {
  executeRequest(endpoint, 'DELETE', token, payload, callback)
}

function getLogoff(token, callback) {
  let endpoint = '/logoff';
  executeRequest(endpoint, 'GET', token, {}, callback);
}

function convertTime(field, timezone, offset) {
  var res = moment(field);
  if (timezone == "LOCAL") {
    // offset = new Date(field).getTimezoneOffset();
    res = res.subtract(parseInt(offset), 'minutes');
  }
  res = res.format("YYYY-MM-DD HH:mm:ss");
  return res;
}


function convertTimeFieldAsTimezone(data, fieldNames, timezone, offset) {
  if (typeof (data) == "object" && data.length > 0 && fieldNames.length > 0) {
    for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < fieldNames.length; j++) {
        if (data[i] && data[i][fieldNames[j]]) {
          data[i][fieldNames[j]] = convertTime(data[i][fieldNames[j]], timezone, offset);
        }
      }
    }
    return data;
  }
  else {
    return data;
  }
}

function determineOriginEndpoint(endpoint) {
  if(endpoint.includes('/clusters') || endpoint.includes('/utility')) {
    return appConfig.MLP_API_URL;
  }
  else if(endpoint.includes('/dm')) {
    return appConfig.DM;
  }
  else {
    return appConfig.MLP_API_URL;
  }
}

function executeRequest(endpoint, method, token, data, callback) {

  let headers = {
    'Content-Type': 'text/json',
    'Authorization': 'aexp-okta-token ' + token
  }

  var options = {
    url: determineOriginEndpoint(endpoint) + endpoint,
    headers: headers,
    body: JSON.stringify(data),
    method: method,
    key: privateKey,
    cert: certificate,
    passphrase: pass,
    proxy: false
  };

  logger.info('MLP Request URL:' + options.url + ' Header : ' + JSON.stringify(headers) + ' Body : ' + JSON.stringify(data));

  request(options, function (error, response, body) {

    if (response) {
      // logger.debug('MLP Response Status :' + response.statusCode + ' Body :' + body);
    }

    if (!error && response && (response.statusCode >= 200 && response.statusCode < 400)) {
      if (response.statusCode == 204) {
        callback(error, "{}");
      }
      else if (callback) {
        callback(error, body);
      }
    }
    else if (response && response.statusCode == 409) {
      callback(error, response);
    } else if (response && response.statusCode == 401) {
      callback(error, response);
     } else if (response && response.statusCode == 403) {
      callback(error, response);
     }
     //else if (response && response.statusCode == 204) {
    //   callback(error, response);
    // }
    else {
      if (!error) {
        logger.error('MLP API Result :' + body);
        error = new Error('MLP Request Failed. Status Code: ' + response.statusCode);
        error.name = 'MLP721'
      } else {
        error = new Error('MLP Request Failed ' + error.toString());
        error.name = 'MLP722'
      }
      logger.error('MLP Error :' + error.toString());
      if (callback)
        callback(error, response);
    }
  })
}
module.exports = { executeRequest, getLogoff, postCall, putCall, getCall, deleteCall, convertTimeFieldAsTimezone, convertTime };
