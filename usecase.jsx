import React, { Component } from 'react';
import { AgGridReact, AgGridColumn } from '@ag-grid-community/react';
import { AllModules } from '@ag-grid-enterprise/all-modules';
import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-balham.css';
import appConfig from '../config/web-config';
import UsecaseForm from './UsecaseForm';
import StatusPopup from './statuspopup';
import UsecaseOverview from './UsecaseOverview';
import AdminMenu from '../components/AdminMenu';
import HomeMenu from '../components/HomeMenu';
import UcTypeFilter from '../components/filters/ucTypefilter';
import { awsUsecase } from '../mock/awsUseCase';
import Api from '../services/Api';


export default class Usecase extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      modules: AllModules,
      userInfo: props.userinfo,
      selectedRow: -1,
      rowData: null,
      selectedUsecase: null,
      useCaseFormModal: null,
      statusPopup: null,
      overviewPopup: null,
      isHomeScreen: props.isHomeScreen == null ? false : props.isHomeScreen,
      sortBy: null,
      startRow: 1,
      filterKey: null,
      filterValue: null,
      filterType: null,
      endRow: appConfig.USECASE_GRID_BLOCK_SIZE,
      awsUsecaseData: awsUsecase,
      components: {
        loadingRenderer: this.loadingRendererMethod,
      },
      frameworkComponents: {
        ucTypeFilter: UcTypeFilter,
      },
      rowBuffer: 0,
      rowModelType: 'infinite',
      paginationPageSize: appConfig.USECASE_GRID_PAGE_SIZE,
      cacheBlockSize: appConfig.USECASE_GRID_BLOCK_SIZE,
      maxConcurrentDatasourceRequests: 2,
      infiniteInitialRowCount: 0,
      overlayLoadingTemplate: '<div class=\'progress-circle progress-indeterminate progress\'></div>',
      overlayNoRowsTemplate: '<div class=\'alert alert-warn alert-dismissible anim-fade in\' role=\'alert\'>'
        + '<span class=\'alert-icon dls-icon-warning-filled\'></span><span>No Record Found.</span></div>',


    };

    this.loadingRendererMethod = (params) => {
      if (params.value !== undefined) {
        return params.value;
      }
      return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
    };

    // this.ucTypeFilterOptions = [
    //   { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
    //   { value: 'APPLICATION', label: 'APPLICATION' },
    //   { value: 'ANALYTICAL', label: 'ANALYTICAL' },
    //   { value: 'POC', label: 'POC' },
    // ];

    // this.ucRoleFilterOptions = [
    //   { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
    //   { value: 'DataScientistFraudModeler', label: 'DataScientistFraudModeler' },
    //   { value: 'DataScientistPOC', label: 'DataScientistPOC' },
    //   { value: 'GRBCModeler', label: 'GRBCModeler' },
    //   { value: 'PlatformModeler', label: 'PlatformModeler' },
    //   { value: 'Admin', label: 'Admin' },
    //   { value: 'VendorModeler1', label: 'VendorModeler1' },
    //   { value: 'VendorModeler2', label: 'VendorModeler2' },
    //   { value: 'DataScientistCreditModeler', label: 'DataScientistCreditModeler' },
    // ];
    // this.ucStatusFilterOptions = [
    //   { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
    //   { value: 'REQUEST', label: 'REQUEST' },
    //   { value: 'IN_PROGRESS', label: 'IN-PROGRESS' },
    //   { value: 'APPROVED', label: 'APPROVED' },
    //   { value: 'PENDING', label: 'PENDING' },
    //   { value: 'DENIED', label: 'DENIED' },
    // ];
    // this.ucEnvironmentFilterOptions = [
    //   { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
    //   { value: 'APPLICATION', label: 'APPLICATION' },
    //   { value: 'ANALYTICAL', label: 'ANALYTICAL' },
    //   { value: 'POC', label: 'POC' },
    // ];
  }


  totalCost = () => {
    const { awsUsecaseData } = this.state;
    const finalResponse = [...awsUsecaseData];
    const remainingBudget = awsUsecaseData.reduce((acc, usecase) => acc + usecase.remainingBudget, 0);
    const totalIterations = awsUsecaseData.reduce((acc, usecase) => acc + usecase.noOfMdlngIterations, 0);
    const totalAllocatedBudget = awsUsecaseData.reduce((acc, usecase) => acc + usecase.allocatedBudget, 0);
    const appendDollar = awsUsecaseData.map(obj => {
      var response = {
        allocatedBudget: `$${obj.allocatedBudget}`,
        comments: obj.comments,
        instanceType: obj.instanceType,
        leaderEmailId: obj.leaderEmailId,
        leaderName: obj.leaderName,
        noOfMdlngIterations: obj.noOfMdlngIterations,
        remainingBudget: `$${obj.remainingBudget}`,
        totalTrainingDetails: obj.totalTrainingDetails,
        useCaseName: obj.useCaseName,
      }
      return response;
    });


    this.setState({ awsUsecaseData: appendDollar });

    for (let i = 0; i < awsUsecaseData.length; i += 1) {
      if (i % 14 === 0) {
        finalResponse.push({
          allocatedBudget: `$${totalAllocatedBudget}`,
          comments: '',
          instanceType: '',
          leaderEmailId: 'Total',
          leaderName: '',
          noOfMdlngIterations: totalIterations,
          remainingBudget: `$${remainingBudget}`,
          totalTrainingDetails: '',
          useCaseName: '',
        });
      }
    }
    return finalResponse;
  }


  onCloseModal = (status, message, refreshPageStatus = true) => {
    if (refreshPageStatus) {
      this.refreshPage();
    }
    this.setState({ overviewPopup: null });
    switch (status) {
      case 0:
        var ele = <StatusPopup onClose={this.onPopupClose} headingMessage="Error message" message={message || 'Something went wrong. Please try again!!'} />;
        this.setState({ statusPopup: ele });
        break;
      case 1:
        ele = <StatusPopup onClose={this.onPopupClose} message="UseCase is created!!" />;
        this.setState({ statusPopup: ele, useCaseFormModal: null });
        break;
      case 2:
        ele = <StatusPopup onClose={this.onPopupClose} message="UseCase is edited!!" />;
        this.setState({ statusPopup: ele, useCaseFormModal: null });
        break;
      case 3:
        ele = <StatusPopup onClose={this.onPopupClose} message="UseCase is deleted!!" />;
        this.setState({ statusPopup: ele });
        break;
      case 4:
        this.setState({ useCaseFormModal: null });
        break;
      default:
        this.setState({ statusPopup: null });
        break;
    }
  }

  loadData = (callback) => {
    const parent = this;
    const origin = APP_CONFIG.MLP_ORCH_API_URL;
    const url = `${origin}/api/v1/usecase/getusecasecostrecords`;
    const apiConf = {
      url,
      method: 'GET',
    };
    Api.Service(apiConf)
      .then((res) => {
        this.loadDataServiceCallback(res, parent, callback);
      })
      .catch((error) => {
        this.loadDataServiceCallbackError(error, parent);
      });
  }

  loadDataServiceCallbackError = (error, parent) => {
    parent.updateNoDataTemplate(error);
    parent.gridApi.showNoRowsOverlay();
  }

  loadDataServiceCallback = (res, parent, callback) => {
    this.setState({ awsUsecaseData: res.data.Records });
    let data = parent.state.awsUsecaseData;
    if (data && parent.state.startRow > 1) {
      data = data.concat(res.data && res.data.Records && res.data.Records.length > 0 ? res.data.Records : []);
    } else {
      data = res.data && res.data.Records && res.data.Records.length > 0 ? res.data.Records : [];
    }
    parent.state.awsUsecaseData = data;
    callback();
  }


  updateNoDataTemplate = (info) => {
    const msg = info.message ? info.message : 'No Record Found';
    this.setState({
      overlayNoRowsTemplate: `${'<div class=\'alert alert-warn alert-dismissible anim-fade in\' role=\'alert\'>'
        + '<span class=\'alert-icon dls-icon-warning-filled\'>'
        + '</span>' + '<span>'}${msg}</span>` + '</div>',
    });
  }

  // onRowDoubleClicked = (row) => {
  //   const svcReq = {
  //     timezone: 'LOCAL',
  //     timezoneOffset: new Date().getTimezoneOffset(),
  //   };
  //   Services.get(`/api/usecases/${row.data.ucId}`, svcReq).then((res) => {
  //     this.setState({ selectedUsecase: res.data });
  //     const modalEle = (
  //       <UsecaseOverview
  //         onClose={() => { this.onCloseModal(status, false); }}
  //         data={this.convertIntoFormData()}
  //         userInfo={this.state.userInfo}
  //       />
  //     );
  //     this.setState({ overviewPopup: modalEle });
  //   });
  // }

  onDeleteClick = () => {
    const { enableEditBtn } = this.state;
    if (enableEditBtn) {
      const ele = (
        <StatusPopup
          headingMessage="Seeking confirmation"
          onClose={this.onPopupClose}
          message="Are you sure you want to delete?"
          alertBox
        />
      );
      this.setState({ statusPopup: ele });
    }
  }

  onConfirmDelete = () => {
    const parent = this;
    const { selectedUsecase, userInfo } = this.state;
    const payload = {
      seqId: selectedUsecase.seqId,
      createdByUserAdsId: userInfo.adsId.replace('@ads.aexp.com', ''),
      createdByUserEmailId: userInfo.emailId,
    };
    const origin = APP_CONFIG.MLP_ORCH_API_URL;
    const url = `${origin}/api/v1/usecase/deleteusecaserecord`;
    const apiConf = {
      url,
      method: 'DELETE',
      data: payload,
    };

    Api.Service(apiConf)
      .then((res) => {
        const selrow = parent.state.selectedRow;
        if (selrow === parent.state.awsUsecaseData.length - 1) {
          parent.setState({ selectedRow: -1, selectedUsecase: null });
        } else {
          parent.setState({ selectedUsecase: parent.state.awsUsecaseData[selrow + 1] });
        }
        this.onCloseModal(3);
      });
  }

  onPopupClose = (status) => {
    this.setState({ statusPopup: null });
    if (status === 1) {
      this.onConfirmDelete();
    }
  }

  convertIntoFormData = () => {
    const { selectedUsecase } = this.state;
    return {
      seqId: selectedUsecase.Records[0].seqId,
      ownerEmailId: selectedUsecase.Records[0].ownerEmailId,
      useCaseName: selectedUsecase.Records[0].useCaseName,
      noOfMdlngIterations: selectedUsecase.Records[0].noOfMdlngIterations,
      totalTrainingDetails: selectedUsecase.Records[0].totalTrainingDetails,
      comments: selectedUsecase.Records[0].comments,
      allocatedBudget: selectedUsecase.Records[0].allocatedBudget,
      createdByUserAdsId: selectedUsecase.Records[0].createdByUserAdsId,
      createdByUserEmailId: selectedUsecase.Records[0].createdByUserEmailId,
      instanceType: selectedUsecase.Records[0].instanceType,
      leaderEmailId: selectedUsecase.Records[0].leaderEmailId,
    };
  }

  onEditClickCallback = (res, userInfo) => {
    this.setState({ selectedUsecase: res.data });
    const modalEle = (
      <UsecaseForm
        onClose={this.onCloseModal}
        userInfo={userInfo}
        loadData={this.loadData}
        data={this.convertIntoFormData()}
        isNew="false"
      />
    );
    this.setState({ useCaseFormModal: modalEle });
  }

  onEditClick = () => {
    const origin = APP_CONFIG.MLP_ORCH_API_URL;
    const { selectedUsecase, userInfo } = this.state;
    const url = `${origin}/api/v1/usecase/getownerrecords/${selectedUsecase.seqId}`;
    const apiConf = {
      url,
      method: 'GET',
    };
    Api.Service(apiConf)
      .then((res) => {
        this.onEditClickCallback(res, userInfo);
      });
  }

  onCreateClick = () => {
    const { userInfo } = this.state;
    const defaultUseCase = this.setDefaultSelectedUsecase();
    const modalEle = <UsecaseForm onClose={this.onCloseModal} data={defaultUseCase} userInfo={userInfo} isNew="true" />;
    this.setState({ useCaseFormModal: modalEle });
  }

  toolTipRef = (elem) => {
    this.tooltipDiv = elem;
  }

  getRowsCallback = (params, parent) => {
    parent.gridApi.showLoadingOverlay();
    parent.state.startRow = params.startRow + 1;
    parent.state.endRow = params.endRow;
    parent.loadData(() => {
      setTimeout(() => {
        const data = parent.totalCost();
        const rowsThisPage = data.slice(params.startRow, params.endRow);
        let lastRow = -1;
        if (data && data.length < params.endRow) {
          lastRow = data.length;
        }
        if (rowsThisPage && rowsThisPage.length > 0) {
          parent.gridApi.hideOverlay();
        } else if (data && data.length > 0 && params.startRow === data.length) {
          parent.gridApi.hideOverlay();
        } else {
          parent.gridApi.showNoRowsOverlay();
        }
        params.successCallback(rowsThisPage, lastRow);
      }, 500);
    });
  }

  onGridReady = (params) => {
    const parent = this;
    this.gridApi = params.api;
    this.columnApi = params.columnApi;

    const dataSource = {
      rowCount: null,
      getRows(params) {
        parent.getRowsCallback(params, parent);
      },
    };
    if (params.api)
      params.api.setDatasource(dataSource);
  };

  onClkExportToExcel = (event) => {
    this.refs.usecaseGridRef.gridOptions.api.exportDataAsCsv();
  }

  setDefaultSelectedUsecase = () => ({
    ownerEmailId: '',
    useCaseName: '',
    noOfMdlngIterations: '',
    totalTrainingDetails: '',
    instanceType: '',
    comments: '',
    allocatedBudget: '',
  })

  onSelectionChanged = (params) => {
    const { selectedRow } = this.state;
    this.gridApi = params.api;
    if (params.node.rowIndex === selectedRow) {
      this.setState({ selectedRow: -1 });
      this.gridApi.getSelectedNodes()[0].setSelected(false);
      this.gridApi.deselectAll();
      this.setState({ selectedUsecase: null });
      const selector = `div[row-index='${params.node.rowIndex}']`;
      document.querySelector(selector).classList.remove('ag-row-focus');
    } else {
      this.setState({ selectedRow: params.node.rowIndex });
      const selectedRows = this.gridApi.getSelectedRows();
      this.setState({ selectedUsecase: selectedRows[0] });
    }
  }

  onSortChanged = (params) => {
    const sortBy = params.api.getSortModel();
    if (sortBy.length > 0) {
      this.state.sortBy = sortBy[0].colId;
      this.state.sortType = sortBy[0].sort;
      if (sortBy[0].colId.includes('.')) {
        this.state.sortBy = sortBy[0].colId.split('.')[1];
      }
    }
    else {
      this.state.sortBy = null;
      this.state.sortType = null;
    }
  }

  onRowSelectionChanged = (event) => {
    const { userInfo } = this.state;
    if (event.api.getSelectedRows().length && userInfo && userInfo.hasAdminRole) {
      this.setState({ enableEditBtn: true });
    }
  }

  refreshPage = () => {
    const params = { type: 'gridReady', api: this.gridApi, columnApi: this.columnApi };
    this.onGridReady(params);
  }

  onFilterModified = (params) => {
    const filters = Object.keys(params.api.getFilterModel());
    const filterLength = filters.length;
    const oldFilterKey = this.state.filterKey;
    let newFilterKey = null;
    if (filterLength === 1) {
      if (oldFilterKey && filters[0] !== oldFilterKey) {
        var filterInstance = params.api.getFilterInstance(oldFilterKey);
        if (filterInstance) { filterInstance.setModel(null); }
      }
      newFilterKey = filters[0];
    } else {
      for (let i = 0; i < filterLength; i++) {
        if (filters[i] === oldFilterKey) {
          filterInstance = params.api.getFilterInstance(oldFilterKey);
          filterInstance.setModel(null);
        } else {
          newFilterKey = filters[i];
        }
      }
    }
    this.state.filterKey = newFilterKey;
  }

  onFilterChanged = (params) => {
    const filters = params.api.getFilterModel();
    if (Object.keys(filters).length == 1) {
      const filterKey = Object.keys(filters)[0];
      this.state.filterKey = filterKey;
      if (filters[filterKey].type === 'inRange') {
        this.state.filterType = 'between';
        this.state.filterValue = `${filters[filterKey].filter}~${filters[filterKey].filterTo}`;
      } else {
        this.state.filterType = filters[filterKey].type;
        this.state.filterValue = filters[filterKey].filter;
      }
      if (filterKey.includes('.')) {
        this.state.filterKey = filterKey.split('.')[1];
      }
      if (filters[filterKey].filterkey) {
        this.state.filterKey = filters[filterKey].filterkey;
      }
    } else if (Object.keys(filters).length == 0) {
      this.state.filterKey = null;
      this.state.filterType = null;
      this.state.filterValue = null;
    }
  }

  resetFilter = () => {
    this.gridApi.destroyFilter('useCaseType');
    this.gridApi.destroyFilter('onBoardStatus');
    this.gridApi.destroyFilter('useCaseMetadata.awsIamRole');
  }


  render() {
    const {
      isHomeScreen,
      enableEditBtn,
      modules,
      awsUsecaseData,
      components,
      rowBuffer,
      rowModelType,
      cacheBlockSize,
      cacheOverflowSize,
      maxConcurrentDatasourceRequests,
      pagination,
      infiniteInitialRowCount,
      maxBlocksInCache,
      paginationPageSize,
      overlayLoadingTemplate,
      overlayNoRowsTemplate,
      frameworkComponents,
    } = this.state;
    return (
      <div>
        {(this.isHomeScreen && <HomeMenu tab="2" />)}
        {(!isHomeScreen && <AdminMenu cluster={false} notifications={false} scorecard={false} usecase />)}
        <div className="main-container-usecase">
          <span className="heading-3">AWS USECASE</span>
          <span className="cluster-excel">
            <span
              onClick={this.onClkExportToExcel}
              data-toggle="tooltip-auto"
              title="Export to Excel"
              className="mlp-cursor-pointer icon-md dls-icon-download"
            />
          </span>
          <span className="main-refresh">
            <span
              className="mlp-cursor-pointer icon-md dls-icon-refresh"
              data-toggle="tooltip-auto"
              title="Reload"
              onClick={this.refreshPage}
            />
          </span>
          <button className="btn-sm ucdelete" onClick={this.onDeleteClick} disabled={!enableEditBtn}>
            <span className="invisible">Delete</span>
          </button>
          <button className="btn-sm ucedit" onClick={this.onEditClick} disabled={!enableEditBtn}>
            <span className="invisible">Edit</span>
          </button>
          <button className="btn-sm uccreate" onClick={this.onCreateClick}>
            <span className="invisible"> Create</span>
          </button>
        </div>


        <div className="cluster-grid" height="500px">
          <div id="myGrid" style={{ height: '90%', width: '100%' }} className="ag-theme-balham">
            <AgGridReact
              modules={modules}
              id="clusterGridId"
              ref="usecaseGridRef"
              enableSorting
              enableServerSideSorting
              enableServerSideFilter
              rowData={awsUsecaseData}
              components={components}
              headerHeight="50"
              rowHeight="40"
              rowSelection="single"
              onFilterChanged={this.onFilterChanged}
              refreshPage={this.refreshPage}
              onFilterModified={this.onFilterModified}
              animateRows
              enableColResize
              rowBuffer={rowBuffer}
              rowDeselection
              rowModelType={rowModelType}
              cacheBlockSize={cacheBlockSize}
              cacheOverflowSize={cacheOverflowSize}
              maxConcurrentDatasourceRequests={maxConcurrentDatasourceRequests}
              infiniteInitialRowCount={infiniteInitialRowCount}
              maxBlocksInCache={maxBlocksInCache}
              pagination={pagination}
              onSortChanged={this.onSortChanged}
              onRowClicked={this.onSelectionChanged}
              onRowDoubleClicked={this.onRowDoubleClicked}
              paginationPageSize={paginationPageSize}
              overlayLoadingTemplate={overlayLoadingTemplate}
              overlayNoRowsTemplate={overlayNoRowsTemplate}
              frameworkComponents={frameworkComponents}
              onGridReady={this.onGridReady}
              onSelectionChanged={this.onRowSelectionChanged}
            >
              <AgGridColumn
                headerName="Director"
                field="ownerEmailId"
                width={250}
                filter="agTextColumnFilter"
                filterParams={{
                  applyButton: true,
                  resetButton: true,
                  suppressAndOrCondition: true,
                  clearButton: true,
                  newRowsAction: 'keep',
                  caseSensitive: true,
                  filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'],
                }}
              />
              <AgGridColumn
                headerName="VP"
                field="leaderEmailId"
                width={250}
                filter="ucTypeFilter"
                filterParams={{
                  filterOptions: this.ucTypeFilterOptions,
                  filterKey: 'useCaseType',
                  filterResetCallback: this.resetFilter,
                  filterType: 'equals',
                }}
              />
              <AgGridColumn
                headerName="Use Case"
                field="useCaseName"
                width={250}
                filter="agTextColumnFilter"
                filterParams={{
                  applyButton: true,
                  resetButton: true,
                  suppressAndOrCondition: true,
                  clearButton: true,
                  newRowsAction: 'keep',
                  caseSensitive: true,
                  filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'],
                }}
              />
              <AgGridColumn
                headerName="# of Modeling Iterations"
                field="noOfMdlngIterations"
                width={250}
                filterkey="ucOwnerEmailId"
                filter="agTextColumnFilter"
                filterParams={{
                  applyButton: true,
                  resetButton: true,
                  suppressAndOrCondition: true,
                  clearButton: true,
                  newRowsAction: 'keep',
                  caseSensitive: true,
                  filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'],
                }}
              />
              <AgGridColumn
                headerName="Total Training Hours"
                field="totalTrainingDetails"
                width={250}
                suppressFilter
                suppressSorting
              />
              <AgGridColumn
                headerName="Instance Type"
                field="instanceType"
                width={300}
                filter="ucTypeFilter"
                filterParams={{
                  filterOptions: this.ucStatusFilterOptions,
                  filterKey: 'onBoardStatus',
                  filterResetCallback: this.resetFilter,
                  filterType: 'equals',
                }}
              />
              <AgGridColumn
                headerName="Comments"
                field="comments"
                width={250}
                suppressFilter
                suppressSorting
              />
              <AgGridColumn
                headerName="Allocated Budget"
                field="allocatedBudget"
                width={250}
                suppressFilter
                suppressSorting
              />
              <AgGridColumn
                headerName="Remaining Budget"
                field="remainingBudget"
                width={250}
                suppressFilter
                suppressSorting
              />
            </AgGridReact>
          </div>
        </div>
        <div>
          {this.state.useCaseFormModal}
        </div>
        <div>
          {this.state.statusPopup}
        </div>
        <div>
          {this.state.overviewPopup}
        </div>
      </div>
    );
  }
}



