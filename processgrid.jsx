import React  from "react";
import appConfig from "../config/web-config"
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import moment from "moment";
import Services from "../services/Services";
import DateFilter from '../components/filters/datefilter.jsx';

class ProcessGrid extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.gridErrMsg = props.errorInfo ? props.errorInfo.message : "No Record Found";
        //this.gridblock = "<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert'>" + "<span class='alert-icon dls-icon-info-filled'>" + "</span>" + "<span>" + this.gridErrMsg + "</span>" + "</div>";
        this.state = {
            paginationPageSize: appConfig.PROCESS_GRID_PAGE_SIZE,
            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span>" +
            "<span>No Record Found.</span>" +
            "</div>",
            rowModelType: "infinite",
            filterKey: null,
            filterType: null,
            filterValue: null,
            sortType: null,
            sortBy: null,
            rowData: null,
            rowBuffer: 0,
            startRow: 1,
            endRow: appConfig.PROCESS_GRID_BLOCK_SIZE,
            cacheBlockSize: appConfig.PROCESS_GRID_BLOCK_SIZE,
            maxConcurrentDatasourceRequests: 2,
            infiniteInitialRowCount: 0,
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
                    }
                }
            },
            frameworkComponents: {
                dateFilter: DateFilter
            },
             getRowNodeId: function (item) {
                return item.clusterId;
            },
            filterParams: {
                comparator: function (filterLocalDateAtMidnight, cellValue) {

                    var cellDate = new Date(cellValue);

                    var cellDateSplitter = [cellDate.getFullYear(), cellDate.getMonth(), cellDate.getDate()];
                    var filterDateSPlitter = [filterLocalDateAtMidnight.getFullYear(), filterLocalDateAtMidnight.getMonth(), filterLocalDateAtMidnight.getDate()];

                    var flag = true;
                    for(var i=0;i<3;i++) {
                        if(cellDateSplitter[i] != filterDateSPlitter[i]) {
                            flag = false;
                            break;
                        }
                    }
                    if(flag)
                        return 0;
                    // Now that both parameters are Date objects, we can compare
                    if (cellDate < filterLocalDateAtMidnight) {
                        return -1;
                    } else if (cellDate > filterLocalDateAtMidnight) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        };
        this.onClkExportToExcel = this.onClkExportToExcel.bind(this);
    }

     onFilterModified = (params) => {

        var filters = Object.keys(params.api.getFilterModel());
        var filterLength = filters.length;
        var oldFilterKey = this.state.filterKey;
        var newFilterKey = null;
        if (filterLength == 1) {
            if (oldFilterKey && filters[0] != oldFilterKey) {
                var filterInstance = params.api.getFilterInstance(oldFilterKey);
                filterInstance.setModel(null);
            }
            newFilterKey = filters[0];
        } else if (filterLength > 1) {

            for (var i = 0; i < filterLength; i++) {
                if (filters[i] == oldFilterKey) {
                    filterInstance = params.api.getFilterInstance(oldFilterKey);
                    filterInstance.setModel(null);
                } else {
                    newFilterKey = filters[i];
                }
            }
        }
        this.state.filterKey = newFilterKey;
    }

    onFilterChanged = (params) => {
        //this.disableOtherFilters(params);
        var filters = params.api.getFilterModel();
        if (Object.keys(filters).length == 1) {
            var filterKey = Object.keys(filters)[0];
            this.state.filterKey = filterKey;
            if (filters[filterKey].type == "inRange") {
                this.state.filterType = "between";
                //this.state.filterTo = filters[filterKey].filterTo;
                this.state.filterValue = filters[filterKey].filter + '~' + filters[filterKey].filterTo;
            }
            else {
                this.state.filterType = filters[filterKey].type;
                this.state.filterValue = filters[filterKey].filter;
            }

        } else if (Object.keys(filters).length == 0) {
            this.state.filterKey = null;
            this.state.filterType = null;
            this.state.filterValue = null;
        }
    }

    onSortChanged = (params) => {
        var sortBy = params.api.getSortModel();
        if (sortBy.length > 0) {
            this.state.sortBy = sortBy[0].colId;
            this.state.sortType = sortBy[0].sort;
        } else {
            this.state.sortBy = null;
            this.state.sortType = null;
        }
    }

    componentWillReceiveProps = (nextProps) => {

        if (nextProps.rowData && nextProps.rowData.length > 0) {
            this.setState({
                rowData: nextProps.rowData
            })
        } else if (nextProps.rowData && nextProps.rowData.length == 0 || []) {
            let infoMsg = "No process data for this cluster";
            this.setState({
                rowData: nextProps.rowData,
                overlayNoRowsTemplate: "<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-info-filled'></span><span>" + infoMsg + "</span></div>"
            })
        }
        else {
            let errorMsg = nextProps.errorInfo ? nextProps.errorInfo.message : 'Error Processing Request'
            this.setState({
                rowData: nextProps.rowData,
                overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span><span>" + errorMsg + "</span></div>"
            })
        }
    }

    onClkExportToExcel = (event) => {
        this.refs.processGridRef.gridOptions.api.exportDataAsCsv();
    }

     onGridReady = (params) => {

        var parent = this;
        this.gridApi = params.api;
        this.columnApi = params.columnApi;


var dataSource = {
            rowCount: null,
            getRows: function (params) {
                parent.gridApi.showLoadingOverlay()
                parent.state.startRow = params.startRow + 1;
                parent.state.endRow = params.endRow;

               parent.loadData(function () {

                    setTimeout(function () {
                        var data = parent.state.rowData || [];
                        var rowsThisPage = data.slice(params.startRow, params.endRow);
                        var lastRow = -1;
                        if (data.length < params.endRow) {
                            lastRow = data.length;
                        }
                        if (rowsThisPage.length > 0) {

                            parent.gridApi.hideOverlay();

                        } else if (data.length > 0 && params.startRow == data.length) {

                            parent.gridApi.hideOverlay();

                        } else {
                            parent.gridApi.showNoRowsOverlay();
                        }
                        params.successCallback(rowsThisPage, lastRow);
                    }, 500);
                });
            }
        };

        params.api.setDatasource(dataSource);
    };

     
    loadData = (callback) => {

        var parent = this;
        var svcReq = {
           startRow: parent.state.startRow,
            endRow: parent.state.endRow,
            filterKey: parent.state.filterKey,
            filterType: parent.state.filterType,
            filterValue: parent.state.filterValue,
            sortType: parent.state.sortType,
            sortBy: parent.state.sortBy,
            timezone: "LOCAL",
            timezoneOffset: new Date().getTimezoneOffset()
        }
          var url = "/api/clusters/"+ parent.props.clusterId + "/processes";
        Services.get(url, svcReq)
           .then(res => {
            
                var data = parent.state.rowData;
                if (data && parent.state.startRow > 1) {
                    data = data.concat(res.data.processes);
                } else {
                    data = res.data.processes;
                }
                parent.state.rowData = data;
                callback()
            })
            .catch(function (error) {
                parent.updateNoDataTemplate(error);
                parent.gridApi.showNoRowsOverlay();
            })
    }

    durationFormatter = (params) => {
        if (params && params.data && params.data.duration) {
            return this.hhmmss(params.data.duration);
        } else return "";
    }

    refreshPage = () =>{

        var params = { type: "gridReady", api: this.gridApi, columnApi: this.columnApi };
        this.onGridReady(params);
    }


    hhmmss = (totalSeconds) => {

        var hours = 0;
        totalSeconds = Math.round(totalSeconds);

        if (totalSeconds > 3600) {
            hours = Math.floor(totalSeconds / 3600);
            totalSeconds = totalSeconds - (hours * 3600);
        }

        var minutes = 0;
        if (totalSeconds > 60) {
            minutes = Math.floor(totalSeconds / 60);
            totalSeconds = totalSeconds - (minutes * 60);
        }

        var seconds = Math.round(totalSeconds);

        // round seconds
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds < 10 ? "0" + seconds : seconds);

        return result;
    }
     resetDateFilter = () => {
       this.gridApi.destroyFilter("processStartTs");
       this.gridApi.destroyFilter("processEndTs");
    }


    render() {
        return (
            <div className="modal-container">
                <div className="modal-excel-div">
                    <span className="modal-excel"><image onClick={this.onClkExportToExcel}
                                                         data-toggle="tooltip-auto" title="Export to Excel"
                                                         className="mlp-cursor-pointer icon-md dls-icon-download"></image>
                    </span>
                    <span className="main-refresh">
                    <image className="mlp-cursor-pointer icon-md dls-icon-refresh"
                           data-toggle="tooltip-auto"
                           title="Reload" onClick={this.refreshPage}></image>
                    </span>
                </div>
                <div className="modal-grid">
                    <div id="myProcessGrid" style={{ height: '100%', width: '100%' }} className="ag-mlp">
                        <AgGridReact ref="processGridRef"
                                     rowData={this.state.rowData}
                                     headerHeight='50' rowHeight='40' rowSelection="single"
                                     components={this.state.components}
                                     rowBuffer={this.state.rowBuffer}
                                      cacheBlockSize={this.state.cacheBlockSize}
                                    cacheOverflowSize={this.state.cacheOverflowSize}
                                    maxConcurrentDatasourceRequests={this.state.maxConcurrentDatasourceRequests}
                                    infiniteInitialRowCount={this.state.infiniteInitialRowCount}
                                    maxBlocksInCache={this.state.maxBlocksInCache}
                                    getRowNodeId={this.state.getRowNodeId}
                                      onFilterChanged={this.onFilterChanged}
                                      onFilterModified={this.onFilterModified}
                                       onSortChanged={this.onSortChanged}
                                     enableServerSideSorting
                                     enableServerSideFilter
                                     enableColResize
                                     rowModelType={this.state.rowModelType}
                                     overlayLoadingTemplate={this.state.overlayLoadingTemplate}
                                     overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                                     frameworkComponents={this.state.frameworkComponents}
                                     pagination={true} paginationPageSize={this.state.paginationPageSize}
                                     animateRows enableCellChangeFlash={false}
                                     onGridReady={this.onGridReady}>
                             <AgGridColumn headerName="USER ID" field="processOwner" width={170} comparator={this.idSort}  filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true, filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'] }}></AgGridColumn>
                            <AgGridColumn headerName="INSTANCE ID" field="instanceId" width={170} comparator={this.idSort}  suppressFilter={true}></AgGridColumn>
                            <AgGridColumn headerName="PROCESS ID" field="processId" width={120} comparator={this.idSort} suppressFilter></AgGridColumn>
                            <AgGridColumn headerName="START TIME (UTC)" field="processStartTs" filter="dateFilter" filterParams={{ filterOptions: this.fileSizeFilterOptions,
                            clearButton: true, filterKey: 'processStartTs', filterType: 'equals', resetDateFilter: this.resetDateFilter }} width={210} ></AgGridColumn>
                            <AgGridColumn headerName="END TIME (UTC)" field="processEndTs" width={210} filter="dateFilter"filterParams={{ filterOptions: this.fileSizeFilterOptions,
                            clearButton: true, filterKey: 'processEndTs', filterType: 'equals', resetDateFilter: this.resetDateFilter }}></AgGridColumn>
                            <AgGridColumn headerName="DURATION" valueFormatter={this.durationFormatter} field="duration" filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep' }} width={160} ></AgGridColumn>
                            <AgGridColumn headerName="PROCESS" field="command"  filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true, filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'] }} width={440}></AgGridColumn>
                        </AgGridReact>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProcessGrid;
