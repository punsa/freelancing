import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Services from "../services/Services.js";
import './LandingPage.css';



export default class LandingPage extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            options: ['CLUSTER PROVISIONING', 'MLPLATFORM', 'JUPYTER NOTEBBOK', 'ACCERALATOR'],
            images: [''],
            // images: ['/public/Assets/images/blue.png', '/public/Assets/images/blue1.jpg', '/public/Assets/images/blue2.jfif', '/public/Assets/images/blue3.jfif'],
            activePage: 0,
            activeCarousel: 0
        }
        this.handleScroll = this.handleScroll.bind(this)
    }
    componentDidMount() {
        var parent = this;
    }

    handleScroll(e) {
        var activePage = 0;
        topPos = document.querySelector('.right-side-container').offsetTop;
        for(var i=0;i<4;i++) {
            var id = "#right-side-" + i;
            var elementPos = document.querySelector(id).offsetTop;
            if(topPos >= elementPos) {
                activePage = i;
            }
        }
        this.setState({activePage: activePage});
    }

    scrollToDesired(index) {
        document.querySelector('.right-side-container').scrollTo(document.querySelector('#right-side-'+index));
    }

    render() {

        return (
            <div>
                <div className="landing-page-container">
                    {/*<img alt="Responsive image" className="img-fluid" src={this.state.images[this.state.activePage]} />*/}
                    <div className="left-side-container">
                        {
                            this.state.options.map((item, index) => {
                                return (
                                    <p className={(this.state.activePage == index ? "selected " : "") + "left-side-text scroll"} onClick={() => { this.setState({ activePage: index }); this.scrollToDesired(index); }}>{item}</p>
                                )
                            })
                        }
                    </div>
                    <div className="right-side-container" onScroll={this.handleScroll}>
                        {
                             (

                                <div id={"right-side-" + this.state.activePage} className="content">
                                    <div className="row">
                                        <div className="col-md-5 col-sm-12" style={{ "width": "500px" }}>

                                            <div className="card">
                                                <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue.png')" }}>
                                                    <div className="card-img-tint card-img-hero-tint dls-core-black-01-bg"></div>
                                                </div>
                                                <div className="card-block text-align-center dls-accent-white-01">
                                                    {/*<span className="icon icon-lg dls-icon-merchandise margin-1-t margin-b"></span>*/}
                                                    <h1 className="heading-3 margin-1-b">ECP(Internal)</h1>
                                                    <p className="body-1 margin-3-b"> The orchestration service provides capability to execute the machine learning models on cloud and internal infrastructure. It abstract cloud providers resource provisioning process, identify optimal compute environment based on ML goals and monitor resource utilization for cost efficiencies.</p>
                                                    <div className="btn-stack">
                                                        <button className="btn-block margin-auto-lr">Visit ECP(Internal)</button>
                                                        <button className="btn-block margin-auto-lr btn-white-secondary">Learn More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-12" style={{ "width": "500px" }}>
                                            <div className="card">
                                                <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue1.jpg')" }}>
                                                    <div className="card-img-tint card-img-hero-tint dls-core-black-01-bg"></div>
                                                </div>
                                                <div className="card-block text-align-center dls-accent-white-01">
                                                    {/*<span className="icon icon-lg dls-icon-merchandise margin-1-t margin-b"></span>*/}
                                                    <h1 className="heading-3 margin-1-b">AWS(External)</h1>
                                                    <p className="body-1 margin-3-b"> The orchestration service provides capability to execute the machine learning models on cloud and internal infrastructure. It abstract cloud providers resource provisioning process, identify optimal compute environment based on ML goals and monitor resource utilization for cost efficiencies.</p>
                                                    <div className="btn-stack">
                                                        <button className="btn-block margin-auto-lr">Visit AWS(External site)</button>
                                                        <button className="btn-block margin-auto-lr btn-white-secondary">Learn More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        {
                             (
                                <div id={"right-side-" + this.state.activePage} className="row">
                                    <div className="col-md-10 col-sm-12">
                                        <div className="card" style={{ "width": "800px" }}>
                                            <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue3.jfif')", "width": "800px" }}>
                                                <div className="card-img-tint card-img-hero-tint dls-core-black-01-bg"></div>
                                            </div>
                                            <div className="card-block text-align-center dls-accent-white-01">
                                                {/*<span className="icon icon-lg dls-icon-merchandise margin-1-t margin-b"></span>*/}
                                                <h1 className="heading-3 margin-1-b">MLPLATFORM</h1>
                                                <p className="body-1 margin-3-b"> The orchestration service provides capability to execute the machine learning models on cloud and internal infrastructure. It abstract cloud providers resource provisioning process, identify optimal compute environment based on ML goals and monitor resource utilization for cost efficiencies.</p>
                                                <div className="btn-stack">
                                                    <button className="btn-block margin-auto-lr">Visit MLPLATFORM</button>
                                                    <button className="btn-block margin-auto-lr btn-white-secondary">Learn More</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            )
                        }
                        {
                             (
                                <div id={"right-side-" + this.state.activePage} className="col-md-8 col-sm-12">
                                    <div id="example-carousel-1" className="carousel dls-accent-white-01" data-toggle="carousel" data-autoplay="false" data-animate="false" style={{ "height": "400px", "width": "845px" }}>

                                        <div className="carousel-inner">
                                            <div className={"carousel-item card pad-3-tb" + (this.state.activeCarousel == 0 ? "carousel-item-active" : "")} aria-hidden={this.state.activeCarousel == 0 ? "true" : "false"}>
                                                <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue.png')" }}>
                                                    <div className="card-img-tint dls-rtp-black-bg"></div>
                                                </div>
                                                <div className="card-block flex flex-align-items-center pad-3-tb height-full">
                                                    <div className="card-content margin-3-tb stack">
                                                        <h1 className="heading-5 margin-1-b anim-slide-up anim-delay-2" style={{ "fontWeight": "600" }}>MLSi-GOLD</h1>
                                                        <h2 className="body-3 anim-slide-up anim-delay-3">A The Machine Learning Studio interactive workspace. To develop a predictive analysis model, you typically use data from one or more sources, transform and analyze that data through various data manipulation and statistical functions, and generate a set of results.</h2>
                                                        <div className="anim-slide-up anim-delay-4">
                                                            <button>Explore</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className={"carousel-item card pad-3-tb" + (this.state.activeCarousel == 1 ? "carousel-item-active" : "")} aria-hidden={this.state.activeCarousel == 1 ? "true" : "false"}>
                                                <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue1.jpg')" }}>
                                                    <div className="card-img-tint dls-rtp-black-bg"></div>
                                                </div>
                                                <div className="card-block flex flex-align-items-center pad-3-tb height-full">
                                                    <div className="card-content margin-3-tb stack">
                                                        <h1 className="heading-5 margin-1-b anim-slide-up anim-delay-2" style={{ "fontWeight": "600", "marginTop": "-50px" }}>GOLD-ML</h1>
                                                        <h2 className="body-3 anim-slide-up anim-delay-3">A Qantas Points earning Credit Card rewards you when you spend on everyday purchases, so you can embark on exciting adventures more often.</h2>
                                                        <div className="anim-slide-up anim-delay-4">
                                                            <button>Explore</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className={"carousel-item card pad-3-tb" + (this.state.activeCarousel == 2 ? "carousel-item-active" : "")} aria-hidden={this.state.activeCarousel == 2 ? "true" : "false"}>
                                                <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue2.jfig')" }}>
                                                    <div className="card-img-tint dls-rtp-black-bg"></div>
                                                </div>
                                                <div className="card-block flex flex-align-items-center pad-3-tb height-full">
                                                    <div className="card-content margin-3-tb stack">
                                                        <h1 className="heading-5 margin-1-b anim-slide-up anim-delay-2" style={{ "fontWeight": "600" }}>ECP</h1>
                                                        <h2 className="body-3 anim-slide-up anim-delay-3">A Qantas Points earning Credit Card rewards you when you spend on everyday purchases, so you can embark on exciting adventures more often.</h2>
                                                        <div className="anim-slide-up anim-delay-4">
                                                            <button>Explore</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-controls">
                                            <a className="carousel-control" style={{ "color": "aliceblue" }} data-control="prev" onClick={() => this.setState({ activeCarousel: this.state.activeCarousel == 0 ? 2 : this.state.activeCarousel - 1 })} alt="Previous Slide" href="#"></a>
                                            <ol className="carousel-indicators" ></ol>
                                            <a className="carousel-control" style={{ "color": "aliceblue" }} data-control="next" onClick={() => this.setState({ activeCarousel: this.state.activeCarousel == 2 ? 0 : this.state.activeCarousel + 1 })} alt="Next Slide" href="#"></a>
                                        </div>
                                    </div>
                                </div>

                            )
                        }
                        {
                             (
                                <div id={"right-side-" + this.state.activePage} className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="card" style={{ "width": "400px" }}>
                                            <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue3.jfif')" }}>
                                                <div className="card-img-tint card-img-hero-tint dls-core-black-01-bg"></div>
                                            </div>
                                            <div className="card-block text-align-center dls-accent-white-01">
                                                {/*<span className="icon icon-lg dls-icon-merchandise margin-1-t margin-b"></span>*/}
                                                <h1 className="heading-3 margin-1-b">Tools</h1>
                                                <p className="body-1 margin-3-b">With more Merchants choosing to accept American Express, including the big brand stores you spend at every day, your points will add up fast.</p>
                                                <div className="btn-stack">
                                                    <button className="btn-block margin-auto-lr">View Nearby Place</button>
                                                    <button className="btn-block margin-auto-lr btn-white-secondary">Learn More</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-6 col-sm-12">
                                        <div className="card" style={{ "width": "400px", "margin-left": "105px" }}>
                                            <div className="card-img-bg" style={{ "backgroundImage": "url('/public/Assets/images/blue.png')" }}>
                                                <div className="card-img-tint card-img-hero-tint dls-core-black-01-bg"></div>
                                            </div>
                                            <div className="card-block text-align-center dls-accent-white-01">
                                                {/*<span className="icon icon-lg dls-icon-merchandise margin-1-t margin-b"></span>*/}
                                                <h1 className="heading-3 margin-1-b">Other Information</h1>
                                                <p className="body-1 margin-3-b">With more Merchants choosing to accept American Express, including the big brand stores you spend at every day, your points will add up fast.</p>
                                                <div className="btn-stack">
                                                    <button className="btn-block margin-auto-lr">View Nearby Place</button>
                                                    <button className="btn-block margin-auto-lr btn-white-secondary">Learn More</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>

        );
    }
}
