import React from 'react';
import appConfig from "../config/web-config"
import {AgGridReact,AgGridColumn} from "ag-grid-react";
import moment from "moment";
import Services from "../services/Services";

class UserSessionGrid extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.gridErrMsg = props.errorInfo ? props.errorInfo.message  : "NO DATA";
        //this.gridblock = "<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert'>"+"<span class='alert-icon dls-icon-info-filled'>"+"</span>"+"<span>" + this.gridErrMsg + "</span>"+"</div>";
        this.state = {
            paginationPageSize : appConfig.PROCESS_GRID_PAGE_SIZE,
            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate : this.gridblock,
            //rowModelType: "infinite",
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
                    }
                }
            },
            filterParams: {

                // provide comparator function
                comparator: function (filterLocalDateAtMidnight, cellValue) {

                    var cellDate = new Date(cellValue);

                    var cellDateSplitter = [cellDate.getFullYear(), cellDate.getMonth(), cellDate.getDate()];
                    var filterDateSPlitter = [filterLocalDateAtMidnight.getFullYear(), filterLocalDateAtMidnight.getMonth(), filterLocalDateAtMidnight.getDate()];

                    var flag = true;
                    for(var i=0;i<3;i++) {
                        if(cellDateSplitter[i] != filterDateSPlitter[i]) {
                            flag = false;
                            break;
                        }
                    }
                    if(flag)
                        return 0;
                    // Now that both parameters are Date objects, we can compare
                    if (cellDate < filterLocalDateAtMidnight) {
                        return -1;
                    } else if (cellDate > filterLocalDateAtMidnight) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        };
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.rowData && nextProps.rowData.length > 0) {
            this.setState({
                rowData: nextProps.rowData
            })
        } else if (nextProps.rowData && nextProps.rowData.length == 0) {
            let infoMsg = "No user session data for this cluster";
            this.setState({
                rowData: nextProps.rowData,
                overlayNoRowsTemplate: "<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-info-filled'></span><span>" + infoMsg + "</span></div>"
            })
        }
        else {
            let errorMsg = nextProps.errorInfo ? nextProps.errorInfo.message : 'Error Processing Request'
            this.setState({
                rowData: nextProps.rowData,
                overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span><span>" + errorMsg + "</span></div>"
            })
        }
    }

    onClkExportToExcel = (event) => {
        this.refs.userSessionGridRef.gridOptions.api.exportDataAsCsv();
    }

    onGridReady = (params) => {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
    };


    getNodeId = (params) =>{
        var srNo = ++params.node.id;
        return srNo;
    }
    durationFormatter = (params) => {
        if (params.data.duration){
            return this.hhmmss(params.data.duration);
        } else return "";
    }
    refreshPage = () =>{
        var clstrInfo = this;
        clstrInfo.gridApi.setRowData([]);
        clstrInfo.gridApi.showLoadingOverlay()
        setTimeout(function() {
            Services.get("/api/clusters/" + clstrInfo.props.clusterId + "/processes/sshd")
                .then(function (response) {
                    clstrInfo.gridApi.hideOverlay();
                    clstrInfo.gridApi.setRowData(response.data.processes);
                })
                .catch(function (error) {
                    clstrInfo.setState({pGridError: error});
                    console.error('AJAX error occured : ' + error);
                })
        },1000)
        clstrInfo.gridApi.hideOverlay();
    }

    hhmmss = (totalSeconds) => {

        var hours   = 0;
        totalSeconds = Math.round(totalSeconds);

        if(totalSeconds > 3600){
            hours   = Math.floor(totalSeconds / 3600);
            totalSeconds = totalSeconds - (hours * 3600);
        }

        var minutes  = 0;
        if(totalSeconds > 60){
            minutes = Math.floor(totalSeconds / 60) ;
            totalSeconds = totalSeconds - (minutes * 60);
        }

        var seconds = Math.round(totalSeconds);

        // round seconds
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds  < 10 ? "0" + seconds : seconds);

        return result;
    }

    render() {
        return (
            <div className="modal-container" style={{width: '647px'}}>
                <div className="modal-excel-div">
              <span className="modal-excel"><image onClick={this.onClkExportToExcel}
                                                   data-toggle="tooltip-auto" title="Export to Excel"
                                                   className="mlp-cursor-pointer icon-md dls-icon-download"></image>
              </span>
                    <span className="main-refresh">
                    <image className="mlp-cursor-pointer icon-md dls-icon-refresh"
                           data-toggle="tooltip-auto"
                           title="Reload" onClick={this.refreshPage}></image>
                    </span>
                </div>
                <div className="modal-grid">
                    <div id="myUserSessionGrid" style={{height: '100%', width : '100%'}} className="ag-mlp-sm">
                        <AgGridReact ref="userSessionGridRef"
                                     rowData={this.state.rowData}
                                     headerHeight='50' rowHeight='40' rowSelection="single"
                            //ensureDomOrder='false'
                                     onGridReady={this.onGridReady.bind(this)}
                                     components={this.state.components}
                                     enableColResize
                            //rowModelType={this.state.rowModelType}
                                     overlayLoadingTemplate={this.state.overlayLoadingTemplate}
                                     overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                                     pagination={true} paginationPageSize={25}
                                     enableSorting={true} enableFilter={true}>
                            <AgGridColumn headerName="START TIME (UTC)" field="processStartTs" filter="agDateColumnFilter" filterParams={this.state.filterParams}  width={215} ></AgGridColumn>
                            <AgGridColumn headerName="END TIME (UTC)" field="processEndTs" filter="agDateColumnFilter" filterParams={this.state.filterParams} width={215} ></AgGridColumn>
                            <AgGridColumn headerName="DURATION" valueFormatter={this.durationFormatter} filter="agNumberColumnFilter" field="duration" width={200} ></AgGridColumn>
                        </AgGridReact>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserSessionGrid;
