'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const mlp = require('./mlpService');
const orch = require('./orchestratorService');
const crypto = require('./cryptoService');
const appConfig = require('./../config/app-config');
const env = process.env.EPAAS_ENV || 'e0';
var moment = require('moment');
const clusterForm = require('./../config/config.json');
const mlpLogger = require('./loggerService');
const logger = mlpLogger.createLogger();

// middleware that is specific to this router

function setFilters(filterType, filterValue) {
    var clusterStatusMapper = {
        'request': 'REQUEST',
        'initializing': 'INITIALIZING',
        'ready': 'READY',
        'terminated': 'TERMINATED',
        'terminating': 'TERMINATING',
        'created': 'CREATED',
        'running': 'RUNNING',
        'idle': 'IDLE'
    };
    logger.debug("filtertype: :- " + filterType);
    logger.debug("filtervalue: :- " + filterValue);
    if (filterType == null || filterValue == null)
        return [filterType, filterValue];
    var pattern = null;
    var a = filterValue;
    switch (filterType) {
        case "equals": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "notEqual": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "startsWith": pattern = new RegExp("^" + a + ".*$", "i");
            break;
        case "endsWith": pattern = new RegExp("^.*" + a + "$", "i");
            break;
        case "contains": pattern = new RegExp("^.*" + a + ".*$", "i");
            break;
        default: logger.debug("Not Found Filter Type : " + filterType);
    }

    if (pattern == null)
        return [filterType, filterValue];
    for (var key in clusterStatusMapper) {
        if (pattern.test(key)) {
            if (filterType === "equals")
                return ["contains", clusterStatusMapper[key]];
            return [filterType, clusterStatusMapper[key]];
        }
    }
    logger.debug("filterVal :- " + filterValue + " does not match any key in clusterStatusMapper");
    return [filterType, filterValue];
}

function formatClusterData(arr) {
    var clusterStatusMapper = {
        'REQUEST': 'Request',
        'INITIALIZING': 'Initializing',
        'READY': 'Ready',
        'CREATED': 'Created',
        'RUNNING': 'Running',
        'IDLE': 'Idle',
        'TERMINATING': 'Terminating',
        'TERMINATED': 'Terminated'
    }
    for (var i = 0; i < arr.length; i++) {
        arr[i]["clusterStatus"] = clusterStatusMapper[arr[i]["clusterStatus"]];
    }
    return arr;
}

function getClusters(token, userId, svcReq, callback) {
    let endpoint = '/api/v1/clusters'
    if (userId != 'ALL') {
        endpoint += '/user/' + userId;
    }
    if (svcReq) {
        endpoint = endpoint + "?"
    }

    for (var key in svcReq) {
        if (svcReq[key]) {
            endpoint = endpoint + key + "=" + svcReq[key] + "&"
        }
    }

    orch.executeRequest(endpoint, 'GET', token, {}, callback)
}

function getProcesses(token, clusterId, svcReq, callback) {

    let endpoint = '/api/v1/clusters/' + clusterId + '/processes'

    if (svcReq) {
        endpoint = endpoint + "?"
    }

    for (var key in svcReq) {
        if (svcReq[key]) {
            endpoint = endpoint + key + "=" + svcReq[key] + "&"
        }
    }

    orch.executeRequest(endpoint, 'GET', token, {}, callback)

}

router.use(function validate(req, res, next) {
    logger.info('Request : ' + req.url);
    next()
})

function getClusterHelper(req, res) {
    try {
        var token = req.userContext.tokens['access_token']
        //var id = req.userContext.userinfo['email']
        //jwt.claims['email']

        if (!validateFilterInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }
        var filterType = null;
        if (req.query.filterType) {
            switch (req.query.filterType) {
                case 'lessThanOrEqual': filterType = "lessThanEqual"
                    break;
                case 'greaterThanOrEqual': filterType = "greaterThanEqual"
                    break;
                default: filterType = req.query.filterType;
                    break;
            }
        }

        var obj = [filterType, req.query.filterValue]
        if (req.query.filterKey === 'clusterStatus') {
            obj = setFilters(filterType, req.query.filterValue);
        }

        var startTimestamp = null;
        var endTimestamp = null;

        if (req.query.startDate) {
            startTimestamp = req.query.startDate + "_00:00:00";
        }

        if (req.query.endDate) {
            endTimestamp = req.query.endDate + "_23:59:59";
        }

        let svcReq = {
            startRow: req.query.startRow, endRow: req.query.endRow,
            filterKey: req.query.filterKey, filterType: obj[0], filterValue: obj[1],
            sortBy: req.query.sortBy, sortType: req.query.sortType,
            startTimestamp: startTimestamp, endTimestamp: endTimestamp
        }

        getClusters(token, req.params.id, svcReq, function (err, results) {
            var st = new Date().getTime();
            if (!err) {
                results = JSON.parse(results);
                if (results['clusters']) {
                    results['clusters'] = formatClusterData(results['clusters']);
                    results['clusters'] = orch.convertTimeFieldAsTimezone(results['clusters'], ["clusterStartTs", "clusterEndTs", "statusUpdateTs"], req.query.timezone, req.query.timezoneOffset);
                }
                else {
                    results['clusters'] = [];
                }
                var et = new Date().getTime();
                logger.info('Get clusters loading time', et - st + " milliseconds");
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
}

// define the register route
router.get('/user/:id', function (req, res) {
    getClusterHelper(req, res);
});

router.get('/clusters/:id', function (req, res) {
    getClusterHelper(req, res);
});


router.get('/update', function (req, res) {
    try {
        var token = req.userContext.tokens['access_token']
        var data = JSON.parse(req.query.data);
        orch.putSuppressDetails(token, req.query.clusterId, data, function (err, results) {
            if (!err) {
                res.status('200').send('{"status":  "updated"}');
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        });
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/terminated_aws', function (req, res) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']

        let timeStamp = "?startTimestamp=" + req.query.startTimestamp + "&endTimestamp=" + req.query.endTimestamp;
        let endpoint = "/api/v1/clusters/terminated/type/" + req.query.type + "/by/" + req.query.by + timeStamp;
        orch.getCall(endpoint, token, function (err, results) {
            var et = new Date().getTime();
            logger.info('scorecard grid loading time', et - st + " milliseconds");
            if (!err) {
                results = JSON.parse(results);
                results["clusters"] = orch.convertTimeFieldAsTimezone(results['clusters'], ["clusterStartTs", "clusterEndTs", "statusUpdateTs"], req.query.timezone, req.query.timezoneOffset);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});

// define the register route
router.get('/status/:statusId/terminatedBy/:terminatedBy', function (req, res) {

    try {
        var token = req.userContext.tokens['access_token']

        if (!validateFilterInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }

        var filterType = "equals";

        var startTimestamp = null;
        var endTimestamp = null;

        if (req.query.startDate) {
            startTimestamp = req.query.startDate + "_00:00:00";
        }

        if (req.query.endDate) {
            endTimestamp = req.query.endDate + "_23:59:59";
        }

        let svcReq = {
            startRow: req.query.startRow, endRow: req.query.endRow,
            filterKey: "terminatedBy", filterType: filterType, filterValue: req.params.terminatedBy,
            startTimestamp: startTimestamp, endTimestamp: endTimestamp
        }

        let endpoint = '/api/v1/clusters/status/TERMINATED'

        if (svcReq) {
            endpoint = endpoint + "?"
        }

        for (var key in svcReq) {
            if (svcReq[key]) {
                endpoint = endpoint + key + "=" + svcReq[key] + "&"
            }
        }

        orch.getCall(endpoint, token, (err, results) => { clusterCallback(err, results, res) });

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});

function clusterCallback(err, results, res) {

    if (!err) {
        results = JSON.parse(results);
        if (results['clusters']) {
            results['clusters'] = formatClusterData(results['clusters']);
        }
        res.status('200').send(results);
    } else {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
}

// define the register route
router.get('/status/terminated', function (req, res) {

    try {
        var token = req.userContext.tokens['access_token']
        if (!validateFilterInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }
        var startTimestamp = null;
        var endTimestamp = null;

        if (req.query.startDate) {
            startTimestamp = req.query.startDate + "_00:00:00";
        }

        if (req.query.endDate) {
            endTimestamp = req.query.endDate + "_23:59:59";
        }

        let svcReq = {
            startRow: req.query.startRow, endRow: req.query.endRow,
            startTimestamp: startTimestamp, endTimestamp: endTimestamp
        }

        let endpoint = '/api/v1/clusters/status/TERMINATED'

        if (svcReq) {
            endpoint = endpoint + "?"
        }

        for (var key in svcReq) {
            if (svcReq[key]) {
                endpoint = endpoint + key + "=" + svcReq[key] + "&"
            }
        }

        orch.getCall(token, endpoint, (err, results) => { clusterCallback(err, results, res); })

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});

router.get('/:clusterId/overview', function (req, res) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        let endpoint = '/api/v1/clusters/' + req.params.clusterId;
        orch.getCall(endpoint, token, function (err, results) {
            var et = new Date().getTime();
            logger.info('cluster overview loading time', et - st + " milliseconds");
            if (!err) {
                results = JSON.parse(results);
                var resp = results["metadata"];
                if (!resp) {
                    resp = {};
                }
                if (results) {
                    resp["clusterType"] = results["clusterType"];
                    resp["clusterName"] = results["clusterName"];
                    resp["masterNodeIP"] = results["masterNodeIP"];
                    res.status('200').send(resp);
                }
                else {
                    res.status('200').send({});
                }
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

// define the projects route
router.get('/:clusterId/processes', function (req, res) {

    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        if (!validateProcessInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }

        let clusterId = req.params.clusterId

        var filterType = null;

        if (req.query.filterType && req.query.filterType === 'lessThanOrEqual') {
            filterType = "lessThanEqual"
        } else if (req.query.filterType && req.query.filterType === 'greaterThanOrEqual') {
            filterType = "greaterThanEqual"
        } else {
            filterType = req.query.filterType;
        }

        let svcReq = {
            startRow: req.query.startRow, endRow: req.query.endRow,
            filterKey: req.query.filterKey, filterType: filterType, filterValue: req.query.filterValue,
            sortBy: req.query.sortBy, sortType: req.query.sortType
        }

        getProcesses(token, clusterId, svcReq, function (err, results) {
            var et = new Date().getTime();
            logger.info('cluster process loading time', et - st + " milliseconds");
            if (!err) {
                results = JSON.parse(results);
                if (results && results.statusCode == 204) {
                    status = results.statusCode;
                }
                results["processes"] = orch.convertTimeFieldAsTimezone(results['processes'], ["processStartTs", "processEndTs"], req.query.timezone, req.query.timezoneOffset);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

// define the projects route
router.get('/:clusterId/processes/sshd', function (req, res) {

    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        if (!validateProcessInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }

        let clusterId = req.params.clusterId

        let svcReq = { filterKey: 'command', filterType: 'startsWith', filterValue: 'sshd' }

        getProcesses(token, clusterId, svcReq, function (err, results) {
            var et = new Date().getTime();
            logger.info('cluster session loading time', et - st + " milliseconds");
            if (!err) {
                results = JSON.parse(results);
                logger.debug("Results : " + results);
                results["processes"] = orch.convertTimeFieldAsTimezone(results['processes'], ["processStartTs", "processEndTs"], req.query.timezone, req.query.timezoneOffset);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/instances', function (req, res) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        var endPoint = "/api/v1/instances";
        orch.postCall(endPoint, token, { instStackType: req.query.stacktype.toLowerCase(), instEnv: req.query.provider.toLowerCase() }, function (err, results) {
            var et = new Date().getTime();
            logger.info('Instances loading time', et - st + " milliseconds");
            if (!err) {
                results = JSON.parse(results);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        });

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/getCluster', function (req, res) {
    try {
        res.status('200').send(clusterForm);
    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.post('/', function (req, res) {
    var st = new Date().getTime();
    var token = req.userContext.tokens['access_token']
    var endPoint = "/api/v1/stack";

    if (req.body.stackName && req.body.stackName.match(/[a-zA-Z0-9\-]*/)[0] == req.body.stackName && req.body.stackType && req.body.stackType.length > 0) {
        if (req.body.createdBy && req.body.createdBy.match(/[a-zA-Z0-9]*/) == req.body.createdBy && req.body.createdByEmail && req.body.createdByEmail.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)[0] == req.body.createdByEmail) {
            if ((req.body.stackType.toLowerCase() == "ec2" && req.body.parameters.flavor && parseInt(req.body.parameters.flavor.length
            ) > 0 && parseInt(req.body.parameters.flavor.length) < 100) || (req.body.stackType.toLowerCase() == "emr" && req.body.parameters.computeNum && req.body.parameters.controlNum && parseInt(req.body.parameters.computeNum) > 0 && parseInt(req.body.parameters.computeNum) < 100 && parseInt(req.body.parameters.controlNum) > 0 && parseInt(req.body.parameters.controlNum) < 100 && req.body.parameters.computeFlavor && req.body.parameters.controlFlavor && req.body.parameters.computeFlavor.length > 0 && req.body.parameters.controlFlavor.length > 0)) {

                orch.postCall(endPoint, token, req.body, function (err, results) {
                    var et = new Date().getTime();
                    logger.info('clusterform data loading time', et - st + " milliseconds");
                    if (!err) {
                        logger.debug(results);
                        var status = '200';
                        if (results && results.statusCode == 409) {
                            status = results.statusCode;
                        }

                        res.status(status).send('{"status": "created", "stackId" : ' + results + '}');

                    } else {
                        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
                    }
                });
            }
            else {
                res.status('500').send('{"status" : "failed","statusCode":"Invalid flavor"}');
            }
        } else {
            res.status('500').send('{"status" : "failed","statusCode":"Invalid createdBy and createdByEmail"}');
        }
    } else {
        res.status('500').send('{"status" : "failed","statusCode":"Invalid parameters"}');
    }
})

 router.delete('/:clusterId', function (req, res) {
    var st = new Date().getTime();
    var token = req.userContext.tokens['access_token']
    var endPoint = "/api/v1/stack/"+ req.params.clusterId;
    orch.deleteCall(endPoint, token,req.body,function (err, results) {
        var et = new Date().getTime();
        logger.info('Delete cluster loading time', et - st + " milliseconds");
        if (!err) {
            logger.debug(results);
            var status = '200';
            if (results && results.statusCode == 409) {
                status = results.statusCode;
            }
            res.status(status).send('{"status":  "created"}');
        } else {
            res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
        }
    }, req.body);
})

function validateFilterInput(req) {

    var cluster = req.query;
    var filterValueRegex = /^[a-zA-Z0-9\-\_~\.@\d+%$]/;
    logger.info("Current Date : " + moment().format('YYYY-MM-DD'));
    if ((!cluster.filterKey || cluster.filterKey && cluster.filterKey.length <= 25 && validator.isAlpha(cluster.filterKey)) &&
        (!cluster.filterValue || cluster.filterValue && cluster.filterValue.length <= 75 && filterValueRegex.test(cluster.filterValue)) &&
        (!cluster.filterType || cluster.filterType && cluster.filterType.length <= 25 && validator.isAlpha(cluster.filterType)) &&
        (!cluster.startDate || cluster.startDate && validator.isAfter(cluster.startDate, "2000-01-01") && validator.isBefore(cluster.startDate, moment().format('YYYY-MM-DD'))) &&
        (!cluster.endDate || cluster.endDate && validator.isAfter(cluster.endDate, "2000-01-01") && validator.isBefore(cluster.endDate, moment().format('YYYY-MM-DD'))) &&
        (!cluster.startRow || cluster.startRow && validator.isNumeric(cluster.startRow) && cluster.startRow <= 100000) &&
        (!cluster.endRow || cluster.endRow && validator.isNumeric(cluster.endRow) && cluster.endRow <= 100000) &&
        (!cluster.sortBy || cluster.sortBy && cluster.sortBy.length <= 25 && validator.isAlpha(cluster.sortBy)) &&
        (!cluster.sortType || cluster.sortType && cluster.sortType.length <= 5 && validator.isAlpha(cluster.sortType))) {
        return true;
    } else {
        logger.info("Validation Failed");
        return false;
    }

}

function validateProcessInput(req) {

    var filterValueRegex = /^(?=.*[a-zA-Z0-9.!@#&*\-\u0080-\u052F])[\/a-zA-Z0-9\s.!@#&*',\-\u0080-\u052F]*%$/;
    var clusterId = req.params.clusterId

    if (clusterId && clusterId.length <= 250 && filterValueRegex.test(clusterId)) {
        return validateFilterInput(req);
    } else if
        ((!clusterId.filterKey || clusterId.filterKey && clusterId.filterKey.length <= 25 && validator.isAlpha(clusterId.filterKey)) &&
        (!clusterId.filterValue || clusterId.filterValue && clusterId.filterValue.length <= 50 && filterValueRegex.test(clusterId.filterValue)) &&
        (!clusterId.filterType || clusterId.filterType && clusterId.filterType.length <= 25 && validator.isAlpha(clusterId.filterType)) &&
        (!clusterId.startRow || clusterId.startRow && validator.isNumeric(clusterId.startRow) && clusterId.startRow <= 100000) &&
        (!clusterId.endRow || clusterId.endRow && validator.isNumeric(clusterId.endRow) && clusterId.endRow <= 100000) &&
        (!clusterId.sortBy || clusterId.sortBy && clusterId.sortBy.length <= 25 && validator.isAlpha(clusterId.sortBy)) &&
        (!clusterId.sortType || clusterId.sortType && clusterId.sortType.length <= 5 && validator.isAlpha(clusterId.sortType))) {
        return true;
    } else {
        logger.info("Validation Failed");
        return false;
    }
}

module.exports = {router, getClusterHelper }
