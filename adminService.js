'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const clusterService = require('./clusterService');


// middleware that is specific to this router

router.get('/clusters/:id', function (req, res) {
    clusterService.getClusterHelper(req, res);
})

module.exports = router
