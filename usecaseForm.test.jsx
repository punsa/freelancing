import React from 'react';
import { shallow } from 'enzyme';
import UsecaseForm from '../../src/containers/UsecaseForm';
import Services from '../../src/services/Services';

Services.get = jest.fn((url, payload) => Promise.resolve({
  data: [ {}, {} ],
}));

describe('UsecaseForm', () => {
  let props = {
    isHomeScreen: false,
  };

  it('render snapshot', () => {
    const wrapper = shallow(<UsecaseForm {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('render snapshot with isHomeScreen', () => {
    props = {
      isHomeScreen: null,
    };
    const wrapper = shallow(<UsecaseForm {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});
