import React, { Component } from "react";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import ClusterGrid from './ClusterGrid.jsx';
import DataManagement from './DataManagement.jsx';
import { Link } from 'react-router-dom';


export default class HomeGrid extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tab: props.tab
        }
    }

    render() {
        return (
            <div className="nav-primary">
                <div className="nav nav-bordered nav-chevron" role="navigation" data-toggle="nav"
                    aria-current="horizontal">
                    <div className="nav-overlay"></div>
                    <ul className="nav-menu">
                        <li className="nav-item" role="presentation"  value = "0">
                        <Link to="/app/home/clusters" aria-selected={this.state.tab == "0"} className="nav-link caret" role="navigation-section">Instances</Link>
                        </li>
                        <li className="nav-item" role="presentation" value = "1" >
                        <Link to="/app/home/datatransfer" aria-selected={this.state.tab == "1"} className="nav-link caret" role="navigation-section">Data Transfer</Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}
