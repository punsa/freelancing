import React from 'react';

class StatusPopup extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.onClose = this.onClose.bind(this);
        this.state = {
            message: props.message,
            alertBox: props.alertBox,
            headingMessage: props.headingMessage ? props.headingMessage : "Confirmation message"
        };
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({ message: nextProps.message });
    }

    onClose = (e) => {
        this.props.onClose(e);
    }

    onOkClick = () => {
        this.handleClose();

    }

    onYesClick = () => {
        this.onClose(1);

    }

    componentDidMount = () => {
        document.body.style.overflow = 'hidden';
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount = () => {
        document.body.style.overflow = 'auto';
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.modalRef && !this.modalRef.contains(event.target)) {
            this.handleClose();
        }
    }

    handleClose = () => {
        this.onClose();
    }

    setModalRef = (node) => {
        this.modalRef = node;
    }


    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen" style={{ zIndex: "100" }} ></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": "100px", "maxWidth": "300px !important" }}>
                    <div className="modal" style={{ top: "20%" }}>
                        <div className="container" style={{ width: "35%" }} ref={this.setModalRef}>
                        <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01"><h2 className="mlp-no-margin pad-std heading-3">{this.state.headingMessage}</h2>
                        <div className="mlp-modal-heading">
                        </div></div>
                            <div className="usecase-form background-white" style={{ "overflow": "hidden", "height": "150px" }}>
                                {/* name */}
                                <div className="row">
                                    <div className="col-md-12" style={{ textAlign: "center" }}>
                                        <p style = {{"fontFamily": "comic sans-serif","fontSize": "1rem","color":"#53565a"}}>{this.state.message}</p>
                                    </div>
                                </div>

                                {!this.state.alertBox && (<div className="row">
                                    <div className="col-md-4"></div>
                                    <div className="col-md-4">
                                        <button className="btn btn-sm btn-primary" style={{ "minWidth": "5.25rem !important","marginTop": "10px","position":"center","left":"5px" }} onClick={this.onOkClick}>Ok</button>
                                    </div>
                                    <div className="col-md-5"></div>
                                </div>)
                                }

                                {this.state.alertBox && (<div className="row" style = {{"marginTop":"19px"}}>
                                    <div className="col-md-3"></div>
                                    <div className="col-md-4">
                                        <button className="btn btn-sm btn-primary" style={{ "marginLeft": "-85px" }} onClick={this.onOkClick}>No</button>
                                    </div>
                                    <div className="col-md-5">
                                        <button className="btn btn-sm btn-primary" style={{"marginLeft":"30px"}} onClick={this.onYesClick}>Yes</button>
                                    </div>
                                    {/*<div className="col-md-1"></div>*/}
                                </div>)
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default StatusPopup;
