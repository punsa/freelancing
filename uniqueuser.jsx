import React, { Component } from "react";
import * as d3 from "d3";



export default class UniqueUsers extends Component {
    constructor(props) {
        super(props);
        this.sbholderDiv = null;
        this.updateChart = this.updateChart.bind(this);
        this.sbholder = this.sbholder.bind(this);

        var data = {'Success Transfers': 300, 'Failed Transfers': 400, 'In-Progress': 200, 'Hold': 100}
        this.state = {
            data: props.data ? props.data : data
        }
    }



    updateChart() {
        // Default settings
        var xData = ["totalSuccess", "totalFailures","totalInProgress","totalTransfersHold","others"];
        var legendsMapper = {
             "totalSuccess": "Total Success",
            "totalFailures": "Total Failure",
            "totalInProgress":"In-Progress",
            "totalTransfersHold":"Hold",
            "others":"Others"
        };
        
        var legendsColor = {       
            "totalSuccess":"#007EEB",
            "totalFailures": "#00A0F1",
            "totalInProgress": "#00BDD9",
            "totalTransfersHold":"#00D6AC",
            "others":  "#1DEA76"
        };   


        var data = this.state.data;
        console.log(data);
        // var showTitle = true;
        var width = 600,
            height = 400,
            radius = Math.min(width, height) / 2;

    var color = d3.scaleOrdinal(d3.schemeCategory20);
        var pie = d3.pie()
            .sort(null)
            .value(function (d) { return d.value; });

        var svg, g, arc;

        var arc = d3.arc()
            .outerRadius(radius)
            .innerRadius(radius - (radius / 2.5));

        svg = d3.select(this.sbholderDiv).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("id", "container")
            .attr("transform", "translate(" + width / 2 + "," + (height / 2) + ")");

        g = svg.selectAll(".arc")
            .data(pie(d3.entries(data)))
            .enter().append("g")
            .attr("class", "arc");

        g.append("path")
            // Attach current value to g so that we can use it for animation
            .each(function (d) { this._current = d; })
            .attr("d", arc)
            .style("fill", function (d) { return color(d.data.key); });
        // g.append("text")
        //     .attr("transform", function (d) { return "translate(" + arc.centroid(d) + ")"; })
        //     .attr("dy", ".35em")
        //     .style("text-anchor", "middle");
        // g.select("text").text(function (d) { return d.data.key; });

        svg.append("text")
            .datum(data)
            .attr("x", 0)
            .attr("y", 0 + radius / 10)
            .attr("class", "text-tooltip")
            .style("text-anchor", "middle")
            .attr("font-weight", "bold")
            .style("font-size", radius / 10 + "px");

        g.on("mouseover", function (obj) {
            console.log(obj)
            svg.select("text.text-tooltip")
                .attr("fill", function (d) { return "red" })
                .text(function (d) {
                    return (obj.data.key + " : " + d[obj.data.key]);
                });
        });

        g.on("mouseout", function (obj) {
            svg.select("text.text-tooltip").text("");
        });

            // d3.select("#centerDataText").text("hi");

        drawLegend();

        function drawLegend() {

            // Dimensions of legend item: width, height, spacing, radius of rounded rect.
            var li = {
                w: 125, h: 30, s: 3, r: 3
            };
            //   d3.select("#centerDataText").html(centerText);

            var legend = d3.select("#legend").append("svg:svg")
                .attr("id", "legendSvg")
                .attr("width", data.length * (li.w + li.s))
                .attr("height", li.h);

            var g = legend.selectAll("g")
                .data(d3.entries(data))
                .enter().append("svg:g")
                .attr("transform", function (d, i) {
                    return "translate(" + i * (li.w + li.s) + ",10)";
                });

            g.append("svg:rect")
                .attr("rx", li.r / 2)
                .attr("ry", li.r / 2)
                .attr("width", li.w)
                .attr("height", li.h)
                .style("fill", function (d) { return color(d.key); });

            g.append("svg:text")
                .attr("x", li.w / 2)
                .attr("y", li.h / 2)
                .attr("dy", "0.35em")
                .attr("text-anchor", "middle")
                .text(function (d) { return d.key; });
        }
    }






    sbholder(element) {
        this.sbholderDiv = element;
    }


    componentDidMount() {
        this.updateChart();
    }

    render() {
        return (
            <div>
                <div className="row border">
                    <div id="main">
                        <div style = {{"width": "80%"}}>
                            {/* <div id="sequence"></div> */}
                            <div className="legend-text"><span>Legends : </span></div>
                            <div id="legend"></div>
                        </div>
                        <div style={{marginTop: '30px'}} ref={this.sbholder} className="UniqueUsers"></div>
                    </div>
                </div>
            </div>
        );
    }
}


