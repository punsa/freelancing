import React, { Component } from "react";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import 'ag-grid/dist/styles/ag-grid.css';
import '../public/css/mlp-ag-grid.scss';
import appConfig from "../config/web-config"


export default class ScoreClusterGrid extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            showRowTooltip: false,
            clusterData: null,
            rowModelType: "inMemory",
            clusterMouseOver: null,
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
                    }
                }
            },
            rowBuffer: 0,
            paginationPageSize: appConfig.CLUSTER_GRID_PAGE_SIZE,

            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span><span>No Record Found.</span></div>",

            getRowNodeId: function (item) {
                return item.clusterId;
            },
            tooltipInfo: {}
        };
    }
    toolTipRef = (elem) => {
        this.tooltipDiv = elem;
    }
    onGridReady = (params) => {

        var parent = this;
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                parent.gridApi.showLoadingOverlay()
                parent.state.startRow = params.startRow + 1;
                parent.state.endRow = params.endRow;

                parent.loadData(function () {

                    setTimeout(function () {
                        var clusterStatusMapper = {
                            'Request': 'REQUEST',
                            'Initializing': 'INITIALIZING',
                            'Ready': 'READY',
                            'Created': 'CREATED',
                            'Running': 'RUNNING',
                            'Idle': 'IDLE',
                            'Terminating': 'TERMINATING',
                            'Terminated': 'TERMINATED'
                        }
                        var data = parent.props.data;
                        data.forEach(function (currentValueParam, indexParam, data) {
                            data[indexParam].clusterStatus = clusterStatusMapper[data[indexParam].clusterStatus];
                        });

                        var rowsThisPage = data.slice(params.startRow, params.endRow);
                        var lastRow = -1;
                        if (data.length < params.endRow) {
                            lastRow = data.length;
                        }
                        if (rowsThisPage.length > 0) {

                            parent.gridApi.hideOverlay();

                        } else if (data.length > 0 && params.startRow == data.length) {

                            parent.gridApi.hideOverlay();

                        } else {
                            parent.gridApi.showNoRowsOverlay();
                        }
                        params.successCallback(rowsThisPage, lastRow);
                    }, 500);
                });
            }
        };

        params.api.setDatasource(dataSource);


    };

    onClkExportToExcel = (event) => {
        this.refs.clusterGridRef.gridOptions.api.exportDataAsCsv();
    }

    onCellMouseOver = (row) => {
        if (row.colDef.field == "clusterStatus" && row.data.clusterStatus == "Idle") {
            let x = row.event.clientX, y = row.event.clientY;

            this.tooltipDiv.style.top = (y + 2) + 'px';
            this.tooltipDiv.style.left = (x + 2) + 'px';
            let tStamp = row.data.statusUpdateTs, newDate, newDate30;
            if (tStamp) {
                newDate = new Date(tStamp);
                newDate30 = moment(newDate).add(30, 'm').toDate();
            } else newDate30 = "";

            this.setState({
                tooltipInfo:
                {
                    clusterStatus: "To Be Terminated By ", statusUpdateTs: newDate30.toString()
                }, showRowTooltip: true
            });
        }
    }

    loadData = (callback) => {
        callback()
    }

    onCellMouseOut = () => {
        this.setState({ showRowTooltip: false });
    }


    idleTimeFormatter = (params) => {
        if (params.data.idleTime) {
            return this.hhmmss(params.data.idleTime);
        } else return "";
    }
    minUsrFormatter = (params) => {
        if (params.data.minUsrSession) {
            return this.hhmmss(params.data.minUsrSession);
        } else return "";
    }
    maxUsrFormatter = (params) => {
        if (params.data.maxUsrSession) {
            return this.hhmmss(params.data.maxUsrSession);
        } else return "";
    }
    avgUsrFormatter = (params) => {
        if (params.data.avgUsrSession) {
            return this.hhmmss(params.data.avgUsrSession);
        } else return "";
    }
    updateNoDataTemplate = (info) => {
        let msg = info.message ? info.message : "No Record Found";
        this.setState({
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'>" +
            "<span class='alert-icon dls-icon-warning-filled'>" +
            "</span>" + "<span>" + msg + "</span>" + "</div>"
        });
    }
    hhmmss = (totalSeconds) => {

        var hours = 0;
        totalSeconds = Math.round(totalSeconds);
        if (totalSeconds > 3600) {
            hours = Math.floor(totalSeconds / 3600);
            totalSeconds = totalSeconds - (hours * 3600);
        }
        var minutes = 0;
        if (totalSeconds > 60) {
            minutes = Math.floor(totalSeconds / 60);
            totalSeconds = totalSeconds - (minutes * 60);
        }
        var seconds = Math.round(totalSeconds);

        // round seconds
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds < 10 ? "0" + seconds : seconds);
        return result;

    }

    render() {
        return (
            <div>
                <div ref={this.toolTipRef}>
                    {this.state.showRowTooltip ?
                        <spanhover style={{ top: 'inherit', left: 'inherit' }}>
                            {this.state.tooltipInfo.clusterStatus}:
                            {this.state.tooltipInfo.statusUpdateTs}</spanhover> : ''}
                </div>
                <div className="main-container">
                    <span className="heading-3">CLUSTERS</span>
                    <span className="cluster-excel"><image onClick={this.onClkExportToExcel}
                        data-toggle="tooltip-auto" title="Export to Excel"
                        className="mlp-cursor-pointer icon-md dls-icon-download"></image>
                    </span>

                </div>
                <div className="scorecluster-grid" height="500px">
                    <div id="scoreGrid" style={{ height: '92%', width: '100%' }} className="ag-mlp">
                        <AgGridReact ref="clusterGridRef"
                            rowData={this.props.data}
                            headerHeight='50' rowHeight='40' //rowSelection="single"
                            onCellMouseOver={this.onCellMouseOver.bind(this)}
                            onCellMouseOut={this.onCellMouseOut.bind(this)}
                            animateRows={true}
                            enableColResize={true}
                            components={this.state.components}
                            rowDeselection={true}
                            rowModelType={this.state.rowModelType}
                            pagination={true}
                            paginationPageSize={this.state.paginationPageSize}
                            getRowNodeId={this.state.getRowNodeId}
                            overlayLoadingTemplate={this.state.overlayLoadingTemplate}
                            overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                            onGridReady={this.onGridReady}>
                            <AgGridColumn headerName="CLUSTER ID" field="clusterId" width={175}></AgGridColumn>
                            <AgGridColumn headerName="TYPE" field="clusterType" width={200}></AgGridColumn>
                            <AgGridColumn headerName="CREATOR" field="createdByEmail" width={250}></AgGridColumn>
                            <AgGridColumn headerName="STATUS" field="clusterStatus" width={200}></AgGridColumn>
                            <AgGridColumn headerName="TERMINATED BY" field="terminatedBy" width={150} suppressFilter="true"></AgGridColumn>
                            <AgGridColumn headerName="START TIME"  field="clusterStartTs" width={165} suppressFilter="true"></AgGridColumn>
                            <AgGridColumn headerName="END TIME" field="clusterEndTs" width={170} suppressFilter="true"></AgGridColumn>
                            <AgGridColumn headerName="TOTAL PROCESSES" field="processCount" width={170} filter="agNumberColumnFilter"></AgGridColumn>
                            <AgGridColumn headerName="NO. OF SESSIONS" field="usrSessionCount" width={160} suppressFilter="true"></AgGridColumn>
                            <AgGridColumn headerName="AVG USER SESSION" valueFormatter={this.avgUsrFormatter} field="avgUsrSession" width={170} suppressFilter="true"></AgGridColumn>
                            <AgGridColumn headerName="FAILURE REASON" field="failureReason" width={200}></AgGridColumn>
                        </AgGridReact>
                    </div>
                </div >
            </div >
        );
    }
}
