import React from 'react';
import Services from "../services/Services.js";
import "./ProjectForm.css";
import StatusPopup from '../containers/statuspopup.jsx';

class ProjectForm extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
          showInvalidMsg: false,
          userinfo: props.userinfo,
          newProjectName: '',
          newGitUrl: '',
          statusPopup: null,
          projectData: props.getPData,
          onModalClose: props.onClose
      };
      this.getProjectData = this.getProjectData.bind(this);
    }

    componentWillReceiveProps = (nextProps) => {
       
    }


    getPostPayload = (event) =>{

      let reqPayload = {
                  
                  projectName: this.state.newProjectName,
                  repositoryURL: this.state.newGitUrl,
                  userId: this.state.userinfo.username
      };
      return reqPayload;
  }


    onSubmitClick = () => {
        this.handleClose();

    }

    
    componentDidMount =() =>{
      this.getProjectData();
    }


    handleClickOutside = (event) => {
      if (this.modalRef && !this.modalRef.contains(event.target)) {
          this.handleClose();
      }
  }

    handleClose = () => {
        this.props.onClose();
    }

    setModalRef = (node) => {
        this.modalRef = node;
    }

    onChangeValue = (event) => {
      this.setState({ newProjectName: event.target.value });
      
  }

  onChange = (event) =>{
    this.setState({ newGitUrl: event.target.value});
  }

  getProjectData =()=>{
    var parent = this;
    var url = "/api/project/"+ this.state.userinfo.username ;   
         Services.get(url)
         .then(function (response) {
           var resData = response.data.projects;
              parent.setState({tableData: resData || [], showDataLoading: false});
            
           })
           .catch(function (error) { 
             //parent.setState({updateNoDataTemplate: error});
             if (error.status == undefined) {
              parent.updateNoDataTemplate(error);
              }
           })
         }

    onSubmit = (event) => {
      let parent = this;
      let payload = this.getPostPayload(event)
      Services.create("/api/project", payload).then(res => {
        //this.state.projectData; 
        if (res.status == 409) {
          this.duplicateMsg(err);
           } else if (res.status == 200){
            this.onCloseForm();
           }
        //this.onCloseForm();
        this.getProjectData();
          //parent.handleClose();
      }) .catch (function(err){
        if (err && err.response.status === 500) {
          parent.updateNoDataTemplate(err);
           
        }else if (err && err.response.status === 409) {
          parent.duplicateMsg(err);
        }
        
      })

  }



  onCloseForm = () => {
      
      //window.location.href ="https://jalahb";
      var modalEle = <StatusPopup onClose={this.state.onModalClose}  message="Project is created successfully!!" />
      this.setState({ statusPopup: modalEle});
  }

  duplicateMsg = (info) =>
{
  var modalEle = <StatusPopup onClose={this.state.onModalClose}  message="Duplicate Entry" />
      this.setState({ statusPopup: modalEle});
}
  updateNoDataTemplate = (info) => {
    let msg = "Invalid Repositary URL";
    //alert(msg);
    this.setState({showInvalidMsg: true, invalidUrlMsg: msg})
}



    render() {
        return (
            <div className="clusterInfoDiv">
              <div className="modal-screen" style={{ zIndex : 10}}></div>
                <div className="hidden-sm-down position-relative" style={{ "overflowY": "hidden" }}>
                    <div className="modal" style={{ zIndex : 10, top: "18%" }}>
                        <div className="project-container" style={{ "overflowY": "hidden"}} ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">CREATE PROJECT</h2>
                                  </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.handleClose}
                                        data-toggle="tooltip-auto" title="Close"
                                        className="mlp-cursor-pointer dls-glyph-close"></image>
                                  </div>
                              </div>
                            <div style={{ "paddingTop": "10px", "height": "225px", "overflowX": "hidden", overflowY:"hidden"}} className="usecase-form background-white">
                              <div className="row">
                                <div className="col-md-6">
                                        <label htmlFor="egselect">PROJECT NAME</label>
                                        <textarea value={this.state.newProjectName} onChange={this.onChangeValue}
                                              type="text" placeholder="Enter the Name of the Project" 
                                               rows="1" className="form-control" style={{ "background": "transparent" }} >
                                          </textarea>
                                  </div>
                                  <div className="col-md-6">
                                        <label htmlFor="egselect">GIT REPO URL</label>
                                        <textarea value={this.state.newGitUrl} onChange={this.onChange}
                                              type="text" placeholder="Enter the GIT REPO URL" 
                                              rows="1" className="form-control" style={{ "background": "transparent" }} >
                                          </textarea>
                                    </div>
                                
                                { this.state.showInvalidMsg && <div style={{ paddingLeft: "276px", color: "#AD3117"}}> {this.state.invalidUrlMsg}</div> }
                                <div>
                            <button className="btn-md" style={{ marginTop:"20%", marginLeft: "101%"}} onClick={this.onSubmit} disabled={this.state.newProjectName == 0 || this.state.newGitUrl ==0}>Save</button>
                            </div>
                          </div>
                      </div>
                  </div>          
                </div>
              </div>
              <div>
          {this.state.statusPopup}
        </div>
            </div >
        );
    }
}

export default ProjectForm;




