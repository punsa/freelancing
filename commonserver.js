'use strict'

const fs = require('fs');
const path = require('path');
const https = require('https');
const http = require('http');
const express = require('express');
const { Router } = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');

var clusterSvc = require('./../api/clusterService');
var insightSvc = require('./../api/insightService');
var usecaseSvc = require('./../api/usecaseService.js');
var applicationSvc = require('./../api/applicationService.js');
var userSvc = require('./../api/userService.js');
var dmSrvc = require('./../api/dmService.js');
var authSvc = require('./../api/authService.js');
var mlsiSvc = require('./../api/mlsiService.js');
var projectSvc = require('./../api/projectService.js');
var suppressSvc = require('./../api/suppressService.js');
var mlpLogger = require('../api/loggerService');
var logger = mlpLogger.createLogger();

const startTime = new Date();

module.exports = function WebServer(options, routes) {

  var app = express();

  //Add session module if the session management is required and provided
  if ( options && options.session){
    app.use(options.session)
  }

  if(routes){
    app.use(routes)
  }

  app.use(helmet())
  app.use(bodyParser.json());
  app.use(/\//, (req,res) => res.redirect('/app/home'));
  app.use('/app/*',authenticate(options),(req,res) => res.sendFile(path.resolve(__dirname, './../index.html')));
  app.use('/unauthorized', (req,res) => res.sendFile(path.resolve(__dirname, './../unauthorized.html')));
  app.use('/logout', (req,res) => res.sendFile(path.resolve(__dirname, './../logoff.html')));
  app.use('/api/*',authenticate(options));
  app.use('/api', projectSvc);
  app.use('/api/clusters',clusterSvc);
  app.use('/api/utility',suppressSvc);
  app.use('/api/insights',insightSvc);
  app.use('/api/usecases', usecaseSvc);
  app.use('/api/applications',  applicationSvc);
  app.use('/api/users',  userSvc);
  app.use('/api/dm',dmSrvc);
  app.use('/api/mlphub',mlsiSvc);
  app.use('/api/logout',authSvc.logout);

  app.use('/public',express.static(path.join(__dirname,'../public')));
  app.use('/status', (req,res) => res.status(200).send('Running !!!!!! Started at '+startTime));

  var serverOptions  = null

  if(options && options.key){
    var privateKey = fs.readFileSync(options.key);
    var certificate = fs.readFileSync(options.certificate);
    var ca = fs.readFileSync(options.truststore);
    var pass = fs.readFileSync(options.keypass,'ascii');
    serverOptions = {
        key: privateKey,
        cert: certificate,
        ca: ca,
        passphrase: pass,
        requestCert: true,
        rejectUnauthorized: false
    };
    // options.agent = new https.Agent(options);
  }

  if(options && options.mode === "dev"){
      const webpack = require('webpack');
      const webpackDevMiddleware = require('webpack-dev-middleware');
      const webpackHotMiddleware = require('webpack-hot-middleware');
      const config = require('./../../webpack-dev.config.js');
      const compiler = webpack(config);
      app.use(webpackDevMiddleware(compiler, {
          publicPath: config.output.publicPath,
          hot : {
            https: true
          },
          stats: {
              colors: true,
              chunks: false,
              'errors-only': true
          }
      }));
      app.use(webpackHotMiddleware(compiler, {
          log: console.log
      }));
  }

  var port = options.port || process.env.NODEJS_PORT ;
  var host = options.host || process.env.NODEJS_IP ;
  var env = options.env || process.env.EPAAS_ENV ;

  if(serverOptions){

    https.createServer(serverOptions, app).listen(port, host, function (err) {
      if(err){
        console.log("Failed starting server ..."+err)
      }else{
        console.log("Server Started on :"+host+":"+port+" env : "+env)
      }
    })

  }else{

    http.createServer(app).listen(port, host, function (err) {
      if(err){
        console.log("Failed starting server "+err)
      }else{
        console.log("Server Started on :"+host+":"+port+" env : "+env)
      }
    })
  }
};

function authenticate(options){
  return function(req,res,next){
    // logger.debug("Authenticate Path : "+JSON.stringify(req.userContext))
    if(options && options.authenticate){
      options.authenticate(req,res,next)
    }else{
      next()
    }
  }
}
