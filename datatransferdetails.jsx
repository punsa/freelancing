import React from 'react';
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import Services from "../services/Services.js";
import appConfig from "../config/web-config";
import DmApprovalButton from "./dmApprovalButton.jsx";
import moment from "moment";


class DataTransfer extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.gridErrMsg = props.errorInfo ? props.errorInfo.message : "NO DATA";
        this.state = {
            userInfo: props.userInfo,
            //rowData:null,
            DMRetryCommentBox: null,
            selectedRow: -1,
            enableCommentBtn: false,
            paginationPageSize: appConfig.DMDETAILS_GRID_PAGE_SIZE,
            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate: this.gridblock,
            showRowTooltip: false,
            //rowModelType: "infinite",
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
                    }
                }
            }

        };
        this.onGridReady = this.onGridReady.bind(this);
        this.dmBoxMoveExists = false;
    }

    componentWillReceiveProps = (nextProps) => {

        if (nextProps.rowData && nextProps.rowData.length > 0) {
            this.setState({
                rowData: nextProps.rowData
            })
        } else if (nextProps.rowData && nextProps.rowData.length == 0) {
            let infoMsg = "No data for this data transfer";
            this.setState({
                rowData: nextProps.rowData,
                overlayNoRowsTemplate: "<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-info-filled'></span><span>" + infoMsg + "</span></div>"
            })
        }
        else {
            let errorMsg = nextProps.errorInfo ? nextProps.errorInfo.message : 'Error Processing Request'
            this.setState({
                rowData: nextProps.rowData,
                overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span><span>" + errorMsg + "</span></div>"
            });
        }
    }


    onRowDoubleClicked = (event) => {
        Services.get("/api/dm/dataTransfer/" + this.props.transferId + "?extended=true").then(res => {

           // this.prevComments = res.data.piicomment;

            const rowData = event.data;
            if (rowData.moduleStatus && rowData.moduleStatus == "HOLD" &&
                rowData.moduleName && rowData.moduleName == "PIICHECKER" && this.state.userInfo.hasAdminRole) {
                //this.approvalStatus = event.target.dataset.status;
                //this.setState({ enableCommentBtn : enableCommentBtn});

                var dataTransferComponent = <DmApprovalButton onClose={this.onModalClose}
                                                              transferId={this.props.transferId}
                                                              data={res.data.piicomment}
                                                              emailId={this.props.userInfo.emailId}
                                                              approvalStatus={this.approvalStatus}/>
                this.setState({dataTransfer: dataTransferComponent});

            }
        });
    }

    onModalClose = () => {
        this.setState({ dataTransfer: null });
        this.setState({ DMRetry: null });
        this.setState({ DMRetryCommentBox: null });
    }

    onRowSelectionChanged = (event) => {
        if (event.api.getSelectedRows().length && this.state.userInfo.hasAdminRole)
            this.setState({ enableEditBtn: true })
    }

    onGridReady = (params) => {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;

    };

    onClkExportToExcel = (event) => {
        this.refs.dmdetailsRef.gridOptions.api.exportDataAsCsv();
    }

    durationFormatter = (params) => {
        if (params.data.duration) {
            return this.hhmmss(params.data.duration);
        } else return "";
    }

    hhmmss = (totalSeconds) => {

        var hours = 0;
        totalSeconds = Math.round(totalSeconds);

        if (totalSeconds > 3600) {
            hours = Math.floor(totalSeconds / 3600);
            totalSeconds = totalSeconds - (hours * 3600);
        }

        var minutes = 0;
        if (totalSeconds > 60) {
            minutes = Math.floor(totalSeconds / 60);
            totalSeconds = totalSeconds - (minutes * 60);
        }

        var seconds = Math.round(totalSeconds);

        // round seconds
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds < 10 ? "0" + seconds : seconds);

        return result;
    }
    onBeforeNameCol = (param) =>{
      if (param.value == 'DMBOXMOVE') this.dmBoxMoveExists = true;
      return param.value;
    }
    onBeforeStatusCol = (param) =>{
      const val = param.value;
      if (this.dmBoxMoveExists === true && val == 'HOLD')
        return 'BYPASSED';
      return val;
    }
    refreshPage = () =>{
        var parent = this;
        parent.gridApi.setRowData([]);
        parent.gridApi.showLoadingOverlay()
        setTimeout(function() {
            Services.get("/api/dm/dataTransfer/"+ parent.props.transferId + "?extended=true")
                .then(function (response) {
                    parent.gridApi.hideOverlay();
                  //let rowData = response.data.extendedTransResult ? response.data.extendedTransResult.moduleStats : []
                //return  parent.setState({ rowData:rowData });
                     parent.gridApi.hideOverlay();
                     parent.gridApi.setRowData(response.data.extendedTransResult.moduleStats ||[]);
                })
                .catch(function (error) {
                    parent.setState({pGridError: error});
                    console.error('AJAX error occured : ' + error);
                })
        },1000)
        parent.gridApi.hideOverlay();
    }

    render() {
        return (
            <div>
            <div className="modal-container">
              <div style={{height : '35px'}}>
              <span className="modal-excel"><image onClick={this.onClkExportToExcel}
                                                   data-toggle="tooltip-auto" title="Export to Excel"
                                                   className="mlp-cursor-pointer icon-md dls-icon-download"></image>
              </span>
              <span className="main-refresh">
              <image className="mlp-cursor-pointer icon-md dls-icon-refresh"
                     data-toggle="tooltip-auto"
                     title="Reload" onClick={this.refreshPage}></image>
              </span>
            </div>
                <div className="dm-modal-grid">
                    <div id="dmdetailsGrid" style={{ height: '100%', width: '100%' }} className="ag-mlp">
                        <AgGridReact ref="dmdetailsRef"
                                     rowData={this.state.rowData}
                                     headerHeight='50' rowHeight='40' rowSelection="single"
                                     components={this.state.components}
                                     enableColResize
                                     onGridReady={this.onGridReady.bind(this)}
                                     overlayLoadingTemplate={this.state.overlayLoadingTemplate}
                                     overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                                     pagination paginationPageSize={this.state.paginationPageSize}
                                     onRowDoubleClicked={this.onRowDoubleClicked.bind(this)}
                                      animateRows enableCellChangeFlash={false} autoHeight={true}>
                            <AgGridColumn headerName="NAME" field="moduleName" width={170} comparator={this.idSort} autoHeight={true} cellClass="cell-wrap-text" suppressFilter cellRenderer={this.onBeforeNameCol}></AgGridColumn>
                            <AgGridColumn headerName="STATUS" field="moduleStatus" width={160} comparator={this.idSort} autoHeight={true} cellClass="cell-wrap-text" suppressFilter cellRenderer={this.onBeforeStatusCol} ></AgGridColumn>
                            <AgGridColumn headerName="START TIME" field="startTime" width={220} comparator={this.idSort} autoHeight={true} cellClass="cell-wrap-text" suppressFilter></AgGridColumn>
                            <AgGridColumn headerName="END TIME" field="endTime" width={180} comparator={this.idSort} autoHeight={true} cellClass="cell-wrap-text" suppressFilter></AgGridColumn>
                            <AgGridColumn headerName="DURATION" valueFormatter={this.durationFormatter} field="duration" autoHeight={true} cellClass="cell-wrap-text" width={160} comparator={this.idSort} suppressFilter></AgGridColumn>
                            <AgGridColumn headerName="Failure Reason" field="failureReason" width={280} suppressFilter={true} autoHeight={true} cellClass="cell-wrap-text"></AgGridColumn>
                        </AgGridReact>
                    </div>
                </div>
                <div>
                    {this.state.dataTransfer}
                </div>
                <div>
                    {this.state.DMRetryCommentBox}
                </div>
            </div>
            </div>
        );
    }
}

export default DataTransfer;
