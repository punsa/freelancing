import React, { Component } from 'react';
import AdminMenu from '../components/AdminMenu.jsx';
import Services from '../services/Services.js';
import Loader from '../components/notifications/LoadingMsg';
import ErrorMsg from '../components/notifications/ErrorMsg';
import HomeMenu from '../components/HomeMenu';
import '../public/css/resourceLookUp.css';

export default class ResourceLookup extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      dataSizeValue: '',
      columnsValue: '',
      validDataSize: '',
      validColumns: '',
      columnsErrorMsg: '',
      dataSizeErrorMsg: '',
      result: '',
      emptyResult: false,
      loader: false,
      displayErrMsg: null,
      isHomeScreen: props.isHomeScreen == null ? false : props.isHomeScreen,
    };
  }

  dataSizeChange = (e) => {
    if (
      (e.target.value > 0 && e.target.value <= 100)
      || e.target.value === ''
    ) {
      this.setState({
        dataSizeValue: e.target.value,
        validDataSize: true,
      });
    } else {
      this.setState({
        dataSizeValue: e.target.value,
        validDataSize: false,
        dataSizeErrorMsg: 'Data size should be greater than 0 and les than 100 TB.',
      });
    }
  };

  columnsChange = (e) => {
    const maxValue = 1000000000;
    if (
      (e.target.value > 0 && e.target.value < maxValue)
      || e.target.value === ''
    ) {
      this.setState({
        columnsValue: e.target.value,
        validColumns: true,
      });
    } else {
      this.setState({
        columnsValue: e.target.value,
        validColumns: false,
        columnsErrorMsg:
          'No.of Columns size should be within 1 and 1000000000.',
      });
    }
  };

  onSubmit = () => {
    const { state } = this;
    this.setState({ loader: true, displayErrMsg: false, emptyResult: false });
    Services.get(`/api/resourcelookup?modelName=xgboost&fileSize=${state.dataSizeValue}&` + `colNums=${state.columnsValue}`)
      .then((res) => {
        if (Object.keys(res.data).length != 0) {
          this.setState({
            result: res.data,
            emptyResult: false,
            submit: true,
            loader: false,
            displayErrMsg: null,
          });
        } else {
          this.setState({
            result: res.data,
            emptyResult: true,
            loader: false,
            displayErrMsg: null,
          });
        }
      })
      .catch((err) => {
        if (err && err.response.status === 501) {
          const errorMessage = err.response.data.slice(36, 107);
          this.setState({
            result: null,
            submit: false,
            loader: false,
            displayErrMsg: errorMessage,
          });
        } else {
          this.setState({ displayErrMsg: 'Internal server error', result: null, loader: false });
        }
      });
  };

  render() {
    const { state } = this;
    const {
      dataSizeValue,
      columnsValue,
      validDataSize,
      validColumns,
      columnsErrorMsg,
      dataSizeErrorMsg,
      result,
      emptyResult,
      loader,
      displayErrMsg,
    } = state;
    return (
      <div>
        {(this.isHomeScreen && <HomeMenu />)}
        {!this.state.isHomeScreen && (
          <AdminMenu
            cluster={false}
            scorecard={false}
            usecase={false}
            notifications={false}
            resourceLookup
          />
        )}
        <div className="main-container-notif border containerHeight">
          <span className="heading-3 fontSize">
            AWS Resource Lookup
          </span>
          <p className="description">
            This tool uses the historical logs of Axgboost jobs in AWS to provide an ML based recommendation for the optimized capacity and compute configuration needed for Axgboost model trainings.
            All you need to provide is the size of the training dataset and the number of features in the model and the recommender will return the node type and the number of instances required.
          </p>
          <div className="row rowMargin">
            <div className="col-xs-6 col-sm-6 col-md-3">
              <label htmlFor="dataSize">Data Size(in TB)</label>
              <input
                id="inputExample3"
                type="number"
                className="form-control"
                value={dataSizeValue}
                data-toggle="inputfield"
                onChange={this.dataSizeChange}
              />
              {!validDataSize && (
                <div className="errorMessage">
                  {dataSizeErrorMsg}
                </div>
              )}
            </div>
            <div className="col-xs-6 col-sm-6 col-md-3">
              <label htmlFor="dataSize">Number of columns</label>
              <input
                id="inputExample3"
                type="number"
                className="form-control"
                value={columnsValue}
                data-toggle="inputfield"
                onChange={this.columnsChange}
              />
              {!validColumns && (
                <div className="errorMessage">
                  {columnsErrorMsg}
                </div>
              )}
            </div>
            <div className="col-xs-6 col-sm-6 col-md-3 submitBtn">
              <button
                className="btn-sm"
                disabled={
                  columnsValue == ''
                  || dataSizeValue == ''
                  || !validColumns
                  || !validDataSize
                }
                onClick={this.onSubmit}
              >
                Submit
              </button>
            </div>
          </div>

          {loader ? (
            <div className="loader">
              <Loader />
            </div>
          ) : (
            <div>
              {result && !emptyResult && (
                <div
                  className="col-xs-6 col-sm-6 col-md-12 margin"
                >
                  <span className="heading-3 fontSize">
                    Recommended optimized configuration
                  </span>

                  <div
                    className="table-fluid margin-1-b margin"
                  >
                    <table
                      id="project-card-table"
                      className="table border table-group table-striped pad-lr"
                    >
                      <table className="table border table-group pad-lr" />
                      <thead>
                        <tr className="tableHead">
                          <th
                            className="body-3 mainHeader"
                            colSpan="5"
                          >
                            EMR Configurations
                          </th>
                          <th
                            className="body-3 mainHeader"
                            colSpan="2"
                          >
                            Axgboost Configurations
                          </th>
                        </tr>
                        <tr className="tableHead">
                          <th
                            className="body-3 tableHeader"
                          >
                            Master instance type
                          </th>
                          <th
                            className="body-3 tableHeader"
                          >
                            Core instance type
                          </th>
                          <th
                            className="body-3 tableHeader"
                          >
                            No. of core instances
                          </th>
                          <th
                            className="body-3 tableHeader"
                          >
                            Master EBS volume(in GB)
                          </th>
                          <th
                            className="body-3 tableHeader"
                          >
                            Core EBS volume(in GB)
                          </th>
                          <th
                            className="body-3 tableHeader"
                          >
                            Worker Memory(in GB)
                          </th>
                          <th
                            className="body-3 tableHeader"
                          >
                            Worker Number
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        { result && result.map((item) => (
                          <tr className="body-1 table-row-link">
                            <td>{item.masterNodeType}</td>
                            <td>{item.coreNodeType}</td>
                            <td>{item.nodenum}</td>
                            <td>{item.masterEBSVol}</td>
                            <td>{item.coreEBSVol}</td>
                            <td>{item.xgbWorkerMemoryNum}</td>
                            <td>{item.xgbWorkerNum}</td>

                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              )}
            </div>
          )}

          {(displayErrMsg || emptyResult) && (
            <div className="noDataMessage">
              {emptyResult ? (
                <ErrorMsg msg="No Data to display" />
              ) : (
                <ErrorMsg msg={displayErrMsg} />
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}
