import React, { Component } from "react";
import { Link } from 'react-router-dom';


export default class NavHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: props.selectedTab
        }
    }

    componentWillReceiveProps(nextProp) {
        this.setState({selectedTab: nextProp.selectedTab});
    }

    render() {
        return (
            <ul class="nav-menu">
                <li class="nav-item" role="presentation"><a class="nav-link caret" role="navigation-section" aria-selected={this.state.selectedTab == "0" ?  "true" : "false"} href="/app/user/clusters">AUTUMN</a>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link caret" role="navigation-section" aria-selected={this.state.selectedTab == "1" ?  "true" : "false"} href="/app/user/datatransfer">Data Management</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link caret" role="navigation-section" aria-selected={this.state.selectedTab == "2" ?  "true" : "false"} href="/app/admin/scorecard">Score Card</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link caret" role="navigation-section" aria-selected={this.state.selectedTab == "3" ?  "true" : "false"} href="/app/admin/usecase">Use Case</a></li>
            </ul>
        );
    }
}
