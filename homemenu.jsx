import React, { Component } from "react";
import { Link } from 'react-router-dom';


export default class HomeMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tab: props.location.pathname.includes("clusters") ? "0" : "1"
        }
    }

    render() {
        return (
            <div className="nav-primary">
                <div className="nav nav-bordered nav-chevron" role="navigation" data-toggle="nav"
                    aria-current="horizontal">
                    <div className="nav-overlay"></div>
                    <ul className="nav-menu">
                        <li className="nav-item" role="presentation"  value = "0">
                        <Link to="/app/user/clusters" aria-selected={this.state.tab == "0"} className="nav-link caret" role="navigation-section">Instances</Link>
                        </li>
                        <li className="nav-item" role="presentation" value = "1" >
                        <Link to="/app/user/datatransfer" aria-selected={this.state.tab == "1"} className="nav-link caret" role="navigation-section">Data Transfer</Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}
