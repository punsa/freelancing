'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const aiLogger = require('./loggerService');
const ai = require('./aiService');
const logger = aiLogger.createLogger();


router.get('/', function (req, res) {
    try {
        // var token = req.userContext.tokens['access_token']
        let endPoint = "/api/v1/artifact/test1/best1/test";
        ai.getCall(endPoint, null, function (err, results) {
            if (!err) {
                logger.debug(results);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.post('/', function (req, res) {
    try {
        // var token = req.userContext.tokens['access_token']
        let endPoint = "/api/v1/artifact/test1/best1/test";
        ai.postCall(endPoint,null,req.body,function (err, results) {
            if (!err) {
                logger.debug(req.body);
                logger.debug(results);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})


module.exports = router
