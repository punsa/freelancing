import React, { Component } from "react";



export default class MlsiLoading extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {

        }

    }
    componentDidMount() {
        this.props.updateMlsiLoadingActive();
        var clusterName = localStorage.getItem("clusterName");
        document.getElementById("heading").innerHTML = "Please wait while your cluster " + clusterName + " is provisioned";
    }



    render() {

        return (
            <div class="form" style={{ marginTop: "85px", marginBottom: "200px" }}>
                <div class='progress-circle progress-indeterminate progress' style=
                    {{ position: "center", left: "50%", marginTop: "15%", marginLeft: "-4em" }}>
                    <h3 style={{
                        color: "#104c97", marginTop: "-66px",
                        marginLeft: "-230px", fontFamily: "franklin-gothic"
                    }} id="heading"></h3>
                </div>
            </div>
        )
    }
}
