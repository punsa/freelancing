import React from 'react';
import { shallow } from 'enzyme';
import Usecase from '../../src/containers/Usecase';
import Services from '../../src/services/Services';

Services.get = jest.fn((url, payload) => Promise.resolve({
  data: [ {}, {} ],
}));

const userInfo = { adsId: 'ncheriya', emailId: 'nikhil.cheriyala@aexp.com' };

describe('Usecase', () => {
  let props = {
    isHomeScreen: false,
    userinfo: { adsId: 'ncheriya', emailId: 'nikhil.cheriyala@aexp.com', hasAdminRole: true },
  };

  beforeEach(() => {
    Services.get = jest.fn((url, payload) => Promise.resolve({
      data: [ {}, {} ],
    }));
  });

  it('render snapshot', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const grid = wrapper.find('.cluster-grid');
    expect(grid).toMatchSnapshot();
  });

  it('render snapshot with isHomeScreen true', () => {
    const wrapper = shallow(<Usecase {...props} isHomeScreen={true} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('render snapshot with isHomeScreen false', () => {
    const wrapper = shallow(<Usecase {...props} isHomeScreen={false} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('calling loadingRendererMethod with params', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'loadingRendererMethod');
    instance.loadingRendererMethod({value: "abc"});
    expect(instance.loadingRendererMethod).toBeCalled();
  });

  it('calling loadingRendererMethod without params', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'loadingRendererMethod');
    instance.loadingRendererMethod({value: undefined});
    expect(instance.loadingRendererMethod).toBeCalled();
  });

  it('render snapshot with isHomeScreen null', () => {
    const wrapper = shallow(<Usecase {...props} isHomeScreen={null} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('calling loaddata with success response', async () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'loadData');
    instance.loadDataServiceCallback = jest.fn();
    instance.loadData(() => {});
    await new Promise((resolve, reject) => { setTimeout(() => { resolve(true) }, 4000) });
    expect(instance.loadData).toBeCalled();
  });

  it('calling loaddata with error response', async () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'loadData');
    Services.get = jest.fn((url, payload) => Promise.reject({
      error: [ {}, {} ],
    }));
    instance.loadDataServiceCallbackError = jest.fn();
    instance.loadData(() => {});
    await new Promise((resolve, reject) => { setTimeout(() => { resolve(true) }, 4000) });
    expect(instance.loadData).toBeCalled();
  });

  it('calling loadDataServiceCallback', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'loadDataServiceCallback');
    instance.loadDataServiceCallback({data: {Records: [] } }, {state: {startRow: 2}}, () => {});
    instance.loadDataServiceCallback({data: {Records: [{}, {}] } }, {state: {startRow: 2}}, () => {});
    instance.loadDataServiceCallback({data: {Records: [] } }, {state: {startRow: 1}}, () => {});
    instance.loadDataServiceCallback({data: {Records: [] } }, {state: {startRow: 2, awsUsecaseData: []}}, () => {});
    instance.loadDataServiceCallback({data: {Records: [{}, {}] } }, {state: {startRow: 2, awsUsecaseData: []}}, () => {});
    instance.loadDataServiceCallback({data: {Records: [{}, {}] } }, {state: {startRow: 1}}, () => {});
    expect(instance.loadDataServiceCallback).toBeCalled();
  });

  it('calling loadDataServiceCallbackError', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'loadDataServiceCallbackError');
    instance.loadDataServiceCallbackError({}, {updateNoDataTemplate: jest.fn(), gridApi: { showNoRowsOverlay: jest.fn() } });
    expect(instance.loadDataServiceCallbackError).toBeCalled();
  });

  it('calling totalCost', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'totalCost');
    instance.totalCost();
    expect(instance.totalCost).toBeCalled();
  });

  it('calling updateNoDataTemplate with msg', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'updateNoDataTemplate');
    instance.updateNoDataTemplate({ message: 'Hello' });
    expect(instance.updateNoDataTemplate).toBeCalled();
  });

  it('calling updateNoDataTemplate with no message', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'updateNoDataTemplate');
    instance.updateNoDataTemplate({ message: null });
    expect(instance.updateNoDataTemplate).toBeCalled();
  });

  it('calling onCloseModal with different status', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    
    jest.spyOn(instance, 'onCloseModal');
    instance.refreshPage = jest.fn();
    instance.onCloseModal(0, "Hello", true);
    instance.onCloseModal(0, null, true);
    instance.onCloseModal(1, "Hello", true);
    instance.onCloseModal(2, "Hello", true);
    instance.onCloseModal(3, "Hello", true);
    instance.onCloseModal(4, "Hello", true);
    instance.onCloseModal(5, "Hello", true);
    expect(instance.onCloseModal).toBeCalled();
  });

  it('calling onCloseModal with different status and refresh status as false', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onCloseModal');
    instance.onCloseModal(0, "Hello", false);
    instance.onCloseModal(1, "Hello", false);
    instance.onCloseModal(2, "Hello", false);
    instance.onCloseModal(3, "Hello", false);
    instance.onCloseModal(4, "Hello", false);
    instance.onCloseModal(5, "Hello", false);
    expect(instance.onCloseModal).toBeCalled();
  });


  it('calling onDeleteClick', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    instance.setState({ userInfo: props.userinfo, selectedUsecase: { seqId: 2 } });
    jest.spyOn(instance, 'onDeleteClick');
    instance.onDeleteClick();
    instance.setState({enableEditBtn: true});
    instance.onDeleteClick();
    expect(instance.onDeleteClick).toBeCalled();
  });

  it('calling onConfirmDelete', async() => {
    const wrapper = shallow(<Usecase {...props} />);
    const selectedUsecase = null;
    const instance = wrapper.instance();
    instance.setState({ userInfo: props.userinfo, selectedUsecase: { seqId: 2 }});
    jest.spyOn(instance, 'onConfirmDelete');
    instance.onConfirmDelete();
    instance.setState({ userInfo: props.userinfo, selectedUsecase: { seqId: 2 }, selectedRow: 0, awsUsecaseData: ["a"] });
    instance.onConfirmDelete();
    await new Promise((resolve, reject) => { setTimeout(() => { resolve(true) }, 4000) });
    expect(instance.onConfirmDelete).toBeCalled();
  });


  it('calling initialgridready', async() => {
    const wrapper = shallow(<Usecase {...props} />);
    const selectedUsecase = null;
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onGridReady');
    await new Promise((resolve, reject) => {
      setTimeout(() => { resolve(true) }, 4000);
    //instance.onConfirmDelete();
    //expect(instance.onGridReady).toBeCalled();
  });

  });
  
  it('calling onFilterChanged', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    instance.gridApi = {destroyFilter: jest.fn()};
    jest.spyOn(instance, 'onFilterChanged');
    let filterModel1 = { "0": {type: "inRange", filter: "0",filterTo: "2" } };
    let filterModel2 = { "0.2": {type: "equal", filter: "0",filterTo: "0", filterkey: "equal" } }
    let params = {api: { getFilterModel: jest.fn(() => { return filterModel1 }) } };
    instance.onFilterChanged(params);
    expect(instance.onFilterChanged).toBeCalled();
    params = {api: { getFilterModel: jest.fn(() => { return filterModel2 }) } };
    instance.onFilterChanged(params);
    params = {api: { getFilterModel: jest.fn(() => { return {...filterModel2, ...filterModel1} }) } };
    instance.onFilterChanged(params);
    expect(instance.onFilterChanged).toBeCalled();
    params = {api: { getFilterModel: jest.fn(() => { return {} }) } };
    instance.onFilterChanged(params);
    expect(instance.onFilterChanged).toBeCalled();
  });

  it('calling resetFilter', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    instance.gridApi = {destroyFilter: jest.fn()};
    jest.spyOn(instance, 'resetFilter');
    instance.resetFilter();
    expect(instance.resetFilter).toBeCalled();
  });

  it('calling onFilterModified', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onFilterModified');
    let filterModel1 = { "0": {type: "inRange", filter: "0",filterTo: "2" } };
    let filterModel2 = { "2": {type: "equal", filter: "0",filterTo: "0", filterKey: "equal" } }
    let params = {api: { getFilterModel: jest.fn(() => { return filterModel1 }), getFilterInstance: jest.fn((key) => { return {setModel: jest.fn()} }) } };
    // await new Promise((resolve, reject) => {
    //   instance.setState({filterKey: "2" }, () => {
    //     resolve(true);
    //   });
    // });
    instance.setState({filterKey: "2" });
    instance.onFilterModified(params);
    expect(instance.onFilterModified).toBeCalled();
    params = {api: { getFilterModel: jest.fn(() => { return {...filterModel2, ...filterModel1} }), getFilterInstance: jest.fn((key) => { return {setModel: jest.fn()} }) } };
    instance.onFilterModified(params);
    instance.setState({filterKey: null });
    params = {api: { getFilterModel: jest.fn(() => { return {...filterModel2 } }), getFilterInstance: jest.fn((key) => { return null; }) } };
    instance.onFilterModified(params);
    instance.setState({filterKey: "2" });
    params = {api: { getFilterModel: jest.fn(() => { return {...filterModel1 } }), getFilterInstance: jest.fn((key) => { return null; }) } };
    instance.onFilterModified(params);
    expect(instance.onFilterModified).toBeCalled();
  });

  it('calling setDefaultSelectedUsecase', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'setDefaultSelectedUsecase');
    instance.setDefaultSelectedUsecase();
    expect(instance.setDefaultSelectedUsecase).toBeCalled();
  });

  it('calling onRowSelectionChanged', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onRowSelectionChanged');
    instance.onRowSelectionChanged({api: { getSelectedRows: jest.fn(() => { return ["a", "b"] }) } });
    instance.onRowSelectionChanged({api: { getSelectedRows: jest.fn(() => { return [] }) } });
    expect(instance.onRowSelectionChanged).toBeCalled();
  });

  it('calling refreshPage', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'refreshPage');
    instance.refreshPage();
    expect(instance.refreshPage).toBeCalled();
  });

  it('calling onGridReadyWithParamsObj', async() => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onGridReady');
    const setDataSrc = (ds) => { ds.getRows(); }
    instance.onGridReady({api: {setDatasource: setDataSrc}});
    await new Promise((resolve, reject) => { setTimeout(() => { resolve(true) }, 400) });
    expect(instance.onGridReady).toBeCalled();
  });

  it('calling onPopupClose', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onPopupClose');
    instance.setState({ userInfo: props.userinfo, selectedUsecase: { seqId: 2 } });
    instance.onPopupClose(1);
    instance.onPopupClose(0);
    expect(instance.onPopupClose).toBeCalled();
  });

  it('calling onClkExportToExcel', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    instance.refs = {usecaseGridRef: {gridOptions: {api: {exportDataAsCsv: jest.fn()} } } }
    jest.spyOn(instance, 'onClkExportToExcel');
    instance.onClkExportToExcel({});
    expect(instance.onClkExportToExcel).toBeCalled();
  });

  it('calling onSelectionChanged',  () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onSelectionChanged');
    instance.setState({selectedRow: 1});
    document.querySelector = jest.fn(() => {return { classList: {remove: jest.fn()} }})
    // await new Promise((resolve, reject) => {
    //     instance.setState({selectedRow: 1}, () => {
    //         resolve(true);
    //     })
    // })
    instance.onSelectionChanged({api: { getSelectedNodes: jest.fn(() => { return [{setSelected: jest.fn()}, "b"] }), deselectAll: jest.fn() }, node: {rowIndex: 1 } });
    expect(instance.onSelectionChanged).toBeCalled();
    instance.onSelectionChanged({api: { getSelectedNodes: jest.fn(() => { return [{setSelected: jest.fn()}, "b"] }), deselectAll: jest.fn(), getSelectedRows: jest.fn(() => {return ["a"]}) }, node: {rowIndex: 0 } });
    expect(instance.onSelectionChanged).toBeCalled();
  });

  it('calling onSortChanged', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onSortChanged');
    instance.onSortChanged({api: { getSortModel: jest.fn(() => { return [{colId: "1.1", sort: "1.1"}, { colId: "2", sort: "1.1" }] }) } });
    instance.onSortChanged({api: { getSortModel: jest.fn(() => { return [] }) } });
    instance.onSortChanged({api: { getSortModel: jest.fn(() => { return [{colId: "1", sort: "1"}] }) } });
    expect(instance.onSortChanged).toBeCalled();
  });

  it('calling onEditClickCallback',  () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    //instance.setState({selectedUsecase: {Records: [{seqId: 1}] }});
    jest.spyOn(instance, 'onEditClickCallback');
    instance.onEditClickCallback({data: {Records: [{seqId: 1}] }} );
    
    expect(instance.onEditClickCallback).toBeCalled();
  });

  it('calling onEditClick',async () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    instance.setState({selectedUsecase: {Records: [{seqId: 1}] }});
    jest.spyOn(instance, 'onEditClick');
    instance.onEditClickCallback = jest.fn();
    instance.onEditClick();
    await new Promise((resolve, reject) => { setTimeout(() => { resolve(true) }, 4000) });
    expect(instance.onEditClick).toBeCalled();
  });

  it('calling onCreateClick', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onCreateClick');
    instance.setState({selectedUsecase: {seqId: 1}})
    instance.onCreateClick();
    expect(instance.onCreateClick).toBeCalled();
  });

  it('calling convertIntoFormData', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    instance.setState({selectedUsecase: {Records: [{seqId: 1}] }});
    jest.spyOn(instance, 'convertIntoFormData');
    instance.convertIntoFormData();
    expect(instance.convertIntoFormData).toBeCalled();
  });

  

  it('calling toolTipRef', () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'toolTipRef');
    instance.toolTipRef({});
    expect(instance.toolTipRef).toBeCalled();
  });

  it('props with homescreen true', () => {
    const wrapper = shallow(<Usecase {...props} isHomeScreen={true} />);
    const instance = wrapper.instance();
    expect(wrapper).toMatchSnapshot();
  });

  it('calling getRowsCallback', async () => {
    const wrapper = shallow(<Usecase {...props} />);
    const instance = wrapper.instance();
    instance.setState({selectedUsecase: {Records: [{seqId: 1}] }});
    jest.spyOn(instance, 'getRowsCallback');
    let params = {startRow: 1, endRow: 1};
    let parent = {gridApi: {showLoadingOverlay: jest.fn(), showNoRowsOverlay: jest.fn() }, state: { startRow: 0, endRow: 0 }, loadData: jest.fn((cb) => { cb(); }) };
    instance.getRowsCallback(params, parent);
    await new Promise((resolve, reject) => { setTimeout(() => { resolve(true) }, 1000) });
    expect(instance.getRowsCallback).toBeCalled();
  });


});



