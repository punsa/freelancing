import React, { Component } from 'react';
import Services from "../services/Services.js";
import moment from "moment";
import StatusPopup from './statuspopup.jsx';
import './commentbox.css';
class CommentBox extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.handleClose = this.handleClose.bind(this);
        this.setModalRef = this.setModalRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.onSubmitClick = this.onSubmitClick.bind(this);
        this.convertData = this.convertData.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeValue = this.onChangeValue.bind(this);
        this.getUseCase = this.getUseCase.bind(this);
        this.convertIntoFormData = this.convertIntoFormData.bind(this);
        this.setWorkstreamGroup = this.setWorkstreamGroup.bind(this);
        this.onChangeWorkstream = this.onChangeWorkstream.bind(this);
        this.validation = this.validation.bind(this);
        // this.closeStatusPopup = this.closeStatusPopup.bind(this);

        this.state = {
            useCaseForm: props.data,
            comment: '',
            workstreamName: '',
            emailId: props.emailId,
            workstreamGroup: [],
            workstreamTouched: false,
            valid: false
        };

        this.setWorkstreamGroup(true);
    }

    // componentWillReceiveProps(nextProps) {
    //     this.setState({ message: nextProps.message});
    // }

    onSubmitClick() {
        this.setState({ workstreamTouched: true });
        this.validation();
        if(this.state.valid) {
            // , timestamp: moment(new Date).format("YYYY-MM-DD_hh:mm:ss") 
            var tmpObj = this.state.useCaseForm;
            tmpObj.comments = tmpObj.comments.concat({ author: this.state.emailId, comment: this.state.comment, workStreamName: this.state.workstreamName });
            this.setState({ useCaseForm: tmpObj, comment: '', workstreamName: '' });
            this.onSubmit();
            //this.handleClose();
        }

    }

    setWorkstreamGroup(initial = false) {
        var workstreamMap = {};
        for (var i = 0; i < this.state.useCaseForm.comments.length; i++) {
            if (!workstreamMap[this.state.useCaseForm.comments[i].workStreamName]) {
                workstreamMap[this.state.useCaseForm.comments[i].workStreamName] = [];
            }
            workstreamMap[this.state.useCaseForm.comments[i].workStreamName].push({ author: this.state.useCaseForm.comments[i].author, comment: this.state.useCaseForm.comments[i].comment, timestamp: this.state.useCaseForm.comments[i].timestamp });
        }
        var workstreamKeys = Object.keys(workstreamMap);
        var workstreamArr = [];
        for (var i = 0; i < workstreamKeys.length; i++) {
            workstreamArr.push({ workstreamName: workstreamKeys[i], comments: workstreamMap[workstreamKeys[i]] });
        }
        if (initial) {
            this.state.workstreamGroup = workstreamArr;
        }
        else {
            this.setState({ workstreamGroup: workstreamArr });
        }

    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(event) {
        if (this.modalRef && !this.modalRef.contains(event.target)) {
            this.handleClose();
        }
    }

    getUseCase(callback) {
        Services.get("/api/usecases/" + this.state.useCaseForm.ucId).then(res => {
            if (callback) {
                callback(res);
            }
        })
    }

    convertIntoFormData(response) {

        return {
            ucId: response.ucId,
            useCaseName: response.useCaseName,
            useCaseDesc: response.useCaseDesc,
            useCaseType: response.useCaseType,
            applicationId: response.applicationId,
            onBoardStatus: response.onBoardStatus,
            ucOwnerEmailId: response.useCaseMetadata.ucOwnerEmailId,
            requesterEmailId: response.useCaseMetadata.requesterEmailId,
            cmptEnv: response.useCaseMetadata.cmptEnv,
            prcGroups: response.useCaseMetadata.prcGroups,
            comments: response.useCaseMetadata.comments
        };
    }

    convertData() {
        var tmpObj = {
            useCaseName: this.state.useCaseForm.useCaseName,
            useCaseDesc: this.state.useCaseForm.useCaseDesc,
            useCaseType: this.state.useCaseForm.useCaseType,
            applicationId: this.state.useCaseForm.applicationId,
            onBoardStatus: this.state.useCaseForm.onBoardStatus,
            useCaseMetadata: {
                ucOwnerEmailId: this.state.useCaseForm.ucOwnerEmailId,
                requesterEmailId: this.state.useCaseForm.requesterEmailId,
                cmptEnv: this.state.useCaseForm.cmptEnv
            }
        };
        tmpObj.useCaseMetadata["prcGroups"] = this.state.useCaseForm.prcGroups;
        tmpObj.useCaseMetadata["comments"] = this.state.useCaseForm.comments;
        return tmpObj;
    }

    onSubmit() {
        console.log(this.state.useCaseForm);
        //TODO :- write logic to put this data to api.
        parent = this;
        parent.props.onClose(5);
        Services.update("/api/usecases/" + this.state.useCaseForm.ucId, this.convertData()).then(res => {
            console.log(res);
            parent.getUseCase(function (response) {
                console.log(response);
                parent.setState({ useCaseForm: parent.convertIntoFormData(response["data"])});
                parent.setWorkstreamGroup();
                parent.props.onClose(4);
            })
        })
    }

    onChangeValue(event) {
        this.setState({ comment: event.target.value });
    }

    validation() {
        if(this.state.workstreamName.match(/^[a-zA-Z0-9\-]*$/)) {
            this.setState({ valid: true, workstreamTouched: true });
        }
        else {
            this.setState({ valid: false, workstreamTouched: true });
        }
    }

    onChangeWorkstream(event) {
        this.setState({ workstreamName: event.target.value });
        this.validation();
    }

    handleClose() {
        this.props.onClose();
    }

    setModalRef(node) {
        this.modalRef = node;
    }


    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen" ></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": 1000, "minWidth": "700px !important", "maxWidth": "700px !important", "overflowY": "auto !important" }}>
                    <div className="modal" style={{ top: "10%" }}>
                        <div className="container" style={{ width: "60%" }} ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">Comments</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.handleClose}
                                        data-toggle="tooltip-auto" title="Close"
                                        className="mlp-cursor-pointer dls-glyph-close"></image>
                                </div>
                            </div>
                            <div style={{
                                "paddingTop": "10px", "height": "400px", "overflowY": "scroll !important", "overflowX": "hidden"
                            }} className="usecase-form background-white">
                                {/* name */}
                                {/* {
                                    this.state.useCaseForm.comments.map(function (comment, index) {
                                        return (
                                            <div key={"commentdiv" + index} style={{ "margin": "10px", "borderRadius": "15px" }} className="comment-div" id={index + "comment"}>

                                                <div className="row" style={{ "marginRight": "12px", "marginLeft": "12px", "border": "1px #e9e9e9" }}>
                                                    <div className="col-md-12" style={{ "fontWeight": "bold", "marginTop": "5px" }}> <span style={{ "color": "#006FCF" }}>{comment.author}</span>
                                                        <span style={{ "float": "right", "fontWeight": "bold" }}>{comment.timestamp}</span>
                                                    </div>
                                                    <div className="col-md-12" style={{ "padding": "10px" }}>
                                                        {comment.comment}
                                                    </div>

                                                </div>
                                                <br />
                                            </div>
                                        )
                                    })
                                } */}

                                {
                                    this.state.workstreamGroup.map((workstream, indexer) => {
                                        return (
                                            <div class="border" style={{ "marginTop": "10px" }} key={'workstream' + indexer}>
                                                <div class="accordion border-b">
                                                    <div class="collapsible accordion-toggle flex-align-center dls-accent-white-01-bg" data-toggle="accordion" role="button" aria-haspopup="true" aria-expanded="false" tabindex="0">
                                                        <span class="collapsible-caret"></span>
                                                        <span>{workstream.workstreamName}</span>
                                                    </div>
                                                    <div class="accordion-content display-none" role="tabpanel">
                                                        <div class="flex-align-center border-dark-t pad dls-accent-gray-01-bg">
                                                            <div class="row stack-md-down">
                                                                <div class="col-xs-12 col-md-12 stack">
                                                                    {
                                                                        workstream.comments.map(function (comment, index) {
                                                                            return (
                                                                                <div key={"commentdiv" + index} style={{ "margin": "10px", "borderRadius": "15px" }} className="comment-div" id={index + "comment"}>

                                                                                    <div className="row" style={{ "marginRight": "12px", "marginLeft": "12px", "border": "1px #e9e9e9" }}>
                                                                                        <div className="col-md-12" style={{ "fontWeight": "bold", "marginTop": "5px" }}> <span style={{ "color": "#006FCF" }}>{comment.author}</span>
                                                                                            <span style={{ "float": "right", "fontWeight": "bold" }}>{comment.timestamp}</span>
                                                                                        </div>
                                                                                        <div className="col-md-12" style={{ "padding": "10px" }}>
                                                                                            {comment.comment}
                                                                                        </div>

                                                                                    </div>
                                                                                    <br />
                                                                                </div>
                                                                            )
                                                                        })
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }

                                <div className="row overview-rowmargin">
                                   <div className={"col-md-6"} htmlFor="name" style={{ "paddingTop": "15px" }}>Workstream Name : </div>
                                    <div className={"col-md-6"}>
                                    <input type="text" value={this.state.workstreamName} id="name" name="workstreamName" onChange={this.onChangeWorkstream} className="form-control" />
                                    {(this.state.workstreamTouched && !this.state.valid && <p style="color:red">Invalid workstream name!</p>)}
                                    </div>
                                </div>
                                <div className="row overview-rowmargin">
                                    <div className={"col-md-6"} htmlFor="comment" style={{ "paddingTop": "35px" }}>Comment : </div>
                                    <div className={"col-md-6"}> <textarea type="text" value={this.state.comment} placeholder="Enter your Comment here..!" id="comment" name="comment" rows="4" onChange={this.onChangeValue} className="form-control" style={{ "background": "transparent" }} ></textarea></div>
                                </div>
                                <div className="row" style={{ "marginTop": "70px" }}>

                                    <div className="col-md-4"></div>
                                    <div className="col-md-2">
                                        <button disabled={this.state.comment == '' || this.state.workstreamName == ''} className="btn btn-primary" style={{ "marginLeft": "-135px" }} onClick={this.onSubmitClick}>Submit</button>
                                    </div>
                                    <div className="col-md-2"></div>
                                    <div className="col-md-2">
                                        <button className="btn btn-primary" style={{ "marginLeft": "-65px" }} onClick={this.handleClose}>Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CommentBox;





