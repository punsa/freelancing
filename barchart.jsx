import React, { Component } from 'react';
import * as d3 from 'd3';
import './barChart.css';
class BarChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            frequency: props.frequency,
            visibility: props.visibility
        }
    }

    drawChart() {

        var tooltip = d3.select("body").append("div").attr("class", "tooltip tooltip-light")
         .attr("data-placement", "bottom");
        var margin = { top: 20, right: 20, bottom: 70, left: 40 },
            width = 700 - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;
        
        d3.select("#bar-chart svg").remove();
       
        var svg = d3.select("#bar-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");
        
        var x = d3.scaleBand()
            .range([0, width])
            .padding(0.1);
        var y = d3.scaleLinear()
            .range([height, 0]);
        x.domain(this.state.data.map(function (d) { return d.key; }));
        y.domain([0, d3.max(this.state.data, function (d) { return d.data })]);
        // append the rectangles for the bar chart
        svg.selectAll(".bar")
            .data(this.state.data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) { return x(d.key) + x.bandwidth() / 4; })
            .attr("width", x.bandwidth() / 2)
            .attr("y", function (d) { return y(d.data); })
            .attr("height", function (d) { return height - y(d.data); })
            .on("mouseover", function (d) {
                console.log(y(0));
                console.log(y(d.data));
                tooltip
                    .style("left", d3.event.pageX - 60 + "px")
                    .style("top", (600 - (y(0) - y(d.data))) + "px")
                    .style("display", "inline-block")
                    .html("$" + (d.data))
                    .style("visibility", "visible")
                    .style("opacity", "1");
            })
            .on("mouseout", function (d) {
                tooltip.style("visibility", "hidden");
            });


        // add the y Axis
        svg.append("g")
            .call(d3.axisLeft(y))
        svg.selectAll(".tick text").each(function (data) {
            var tick = d3.select(this);
            tick.text("$" + tick.text());
        });

        // add the x Axis
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

    }



    componentWillReceiveProps(nextProps) {
        this.setState({ data: nextProps.data, frequency: nextProps.frequency });
        this.drawChart();
    }

    componentDidMount() {
        this.drawChart();
    }

    render() {
        return (
            <div id="bar-chart">
            </div>

        );
    }
}
export default BarChart;
