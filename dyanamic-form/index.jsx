import React from 'react';
import './form.css';
import Services from "../../services/Services.js";

export default class DynamicForm extends React.Component {
    state = {};
    constructor(props) {
        super(props);
        this.onClose = this.onClose.bind(this);
        this.onChange = this.onChange.bind(this);
        let stateObj = {};
        let validationObj = {};
        for (var i = 0; i < this.props.model.length; i++) {
            stateObj[this.props.model[i].key] = this.props.defaultValues[this.props.model[i].key];
            if (this.props.model[i].validations) {
                validationObj[this.props.model[i].key] = { touched: false, error: false, errorMessage: '', rules: this.props.model[i].validations }
            }
        }
        this.state = { ...stateObj };
        this.state["validations"] = { ...validationObj };
        this.state["submitActivated"] = true;
        this.state["model"] = this.props.model;
        this.validate = this.validate.bind(this);
        this.updateSubmitActivateStatus = this.updateSubmitActivateStatus.bind(this);
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.model !== this.props.model) {
            let stateObj = {};
            for (var i = 0; i < this.props.model.length; i++) {
                stateObj[this.props.model[i].key] = this.props.defaultValues[this.props.model[i].key];
            }
            this.setState({ ...stateObj });
        }
    }

    componentDidMount() {
        document.body.style.overflow = 'hidden';
        this.getInstanceOptions();
        this.updateSubmitActivateStatus();
    }

    componentWillUnmount() {
        document.body.style.overflow = 'auto';
    }


    onSubmit = (e) => {
        e.preventDefault();
        if (this.props.onSubmit) this.props.onSubmit(this.convertdata(this.props.mapper));
    }

    convertdata = (mapper) => {
        var formdata = {};
        var keys = Object.keys(mapper);
        for (var i = 0; i < keys.length; i++) {
            if (this.props.submitFields && this.props.submitFields[keys[i]] && this.state[this.props.submitFields[keys[i]].field] == this.props.submitFields[keys[i]].value)
                continue;
            if (typeof (mapper[keys[i]]) == "object") {
                formdata[keys[i]] = this.convertdata(mapper[keys[i]]);
            }
            else {
                // TODO :- logic for ldap.
                switch (mapper[keys[i]]) {
                    case 'ldap1':
                        formdata[keys[i]] = '';
                        break;
                    case 'ldap2':
                        formdata[keys[i]] = '';
                        break;
                    default:
                        formdata[keys[i]] = this.state[mapper[keys[i]]];
                        break;
                }
            }
        }
        return formdata;
    }

    onClose = (e) => {
        e.preventDefault();
        this.props.onClose(e);
    }

    validate = () => {
        var validationObj = this.state.validations;
        var keys = Object.keys(this.state.validations);
        for (var i = 0; i < keys.length; i++) {
            if (this.state.validations[keys[i]].touched == false)
                continue;
            var error = false;
            var errorMessage = "";
            for (var j = 0; j < this.state.validations[keys[i]].rules.length; j++) {
                switch (this.state.validations[keys[i]].rules[j].type) {
                    case 1:
                        if (this.state[keys[i]].match(/[a-zA-Z0-9\-]*/)[0] != this.state[keys[i]]) {
                            error = true;
                            errorMessage = this.state.validations[keys[i]].rules[j].message;
                            break;
                        }
                        break;
                    case 2:
                        if (parseInt(this.state[keys[i]]) < 3) {
                            error = true;
                            errorMessage = this.state.validations[keys[i]].rules[j].message;
                            break;
                        }
                        break;
                    case 3:
                        if (parseInt(this.state[keys[i]]) > 100) {
                            error = true;
                            errorMessage = this.state.validations[keys[i]].rules[j].message;
                            break;
                        }
                        break;
                    case 4:
                        if (parseInt(this.state[keys[i]]) > 1) {
                            error = true;
                            errorMessage = this.state.validations[keys[i]].rules[j].message;
                            break;
                        }
                        break;
                    case 5:
                        if (parseInt(this.state[keys[i]]) == 0) {
                            error = true;
                            errorMessage = this.state.validations[keys[i]].rules[j].message;
                            break;
                        }
                        break;
                }
            }
            validationObj[keys[i]].error = error;
            validationObj[keys[i]].errorMessage = errorMessage;
        }
        this.setState({ validations: validationObj }, () => {
            this.updateSubmitActivateStatus();
        });
    }

    updateSubmitActivateStatus = () => {
        var flag = true;
        var keys = Object.keys(this.state.validations);
        for (var i = 0; i < keys.length; i++) {
            if (this.state.validations[keys[i]].error == true) {
                flag = false;
                break;
            }
        }
        this.setState({ submitActivated: flag });
    }


    onChange = (e, key, type = "single") => {
        console.log(`${key} changed ${e.target.value} type ${type}`);
        if (type === "single") {
            let val = e.target.value;

            this.setState({
                [key]: val
            }, () => {
                if (this.state.model.find(obj => obj.key == key).refreshInstance == true) {
                    this.getInstanceOptions();
                }

                // state is updated with the new input values by the user.
                let validationObj = this.state.validations;
                if (validationObj[key]) {
                    validationObj[key].touched = true;
                }
                this.setState({ validations: validationObj }, () => {
                    this.validate();
                });
            });
        } else {
            // Array of values (e.g. checkbox): TODO: Optimization needed.
            let found = this.state[key] ?
                this.state[key].find((d) => d === e.target.value) : false;

            if (found) {
                let data = this.state[key].filter((d) => {
                    return d !== found;
                });
                this.setState({
                    [key]: data
                });
            } else {
                this.setState({
                    [key]: [e.target.value, ...this.state[key]]
                });
            }
        }
    }

    getInstanceOptions = () => {
        Services.get("/api/clusters/instances?stacktype=" + this.state["stackType"] + "&provider=" + this.state["provider"]).then(res => {
            var instanceOptions = res.data.instances;
            var options = [];
            for (var i = 0; i < instanceOptions.length; i++) {
                options.push({ value: instanceOptions[i].instDisplayName, key: instanceOptions[i].instConfig });
            }
            var model = this.state.model;
            var index1 = model.findIndex((obj) => obj.key == 'controlFlavor');
            var index2 = model.findIndex((obj) => obj.key == 'computeFlavor');
            var index3 = model.findIndex((obj) => obj.key == 'flavor');
            model[index1].options = options;
            model[index2].options = options;
            model[index3].options = options;
            this.setState({ model: model });
        })
            .catch(function (error) {
                console.error('AJAX error occured :' + error);
            })
    }


    renderForm = () => {
        //let model = this.props.model;
        let model = this.state.model;
        let defaultValues = this.props.defaultValues;

        let formUI = model.map((m) => {
            let key = m.key;
            let type = m.type || "text";
            let props = m.props || {};
            let name = m.name;
            let value = m.value;
            let col = m.col;
            let id = m.id;
            let placeholder = m.placeholder;
            let possibleValues = m.show && m.show.type == 1 && m.show.value ? m.show.value.split(",") : [];

            let target = key;
            value = this.state[target];

            let input = <input {...props}
                className={"pl-20"}
                type={type}
                key={key}
                id={id}
                name={name}

                placeholder={placeholder}
                onChange={(e) => { this.onChange(e, target) }}
            />;

            if (type == 'label-info') {
                input = <label className="label-info"
                    key={"l" + key}
                    htmlFor={key}>
                    {m.label}
                </label>;
            }

            if (type == "radio") {

                input = m.options.map((o) => {
                    let checked = o.value == value;
                    console.log("radio: ", o.value, value);
                    return (
                        <div class="radio">
                            <input {...props} selected={checked} onChange={(e) => { this.onChange(e, m.key) }} key={o.key} id={o.key} class="form-control" type="radio" aria-invalid="false" name={m.key} value={o.value} />
                            <label for={o.key} >{ o.key }</label>
                        </div>
                    );
                });

                // input = <div className={"form-control"}>{input}</div>;
            }

            if (type == "select") {
                var displayValueFlag = false;
                if ((m.key == 'computeFlavor' || m.key == 'controlFlavor' || m.key == 'flavor')) {
                    displayValueFlag = true;
                }
                input = m.options.map((o) => {
                    // let checked = o.value == value;
                    console.log("select: ", o.value, value);
                    return (
                        <option {...props}
                            className="form-input"
                            key={o.key}
                            // selected={checked}
                            value={o.key}
                        >{displayValueFlag ? (o.key + ' (' + o.value + ')') : (o.value)}</option>
                    );
                });

                console.log("Select default: ", value);
                var input1 = <option className="form-input" key="select" value="" >Select</option>;
                input = [input1].concat(input);
                console.log(input);
                input = <div className={"form-control"}><select   {...props} id={id} onChange={(e) => { this.onChange(e, m.key) }}>{input}</select></div>;
            }

            if (type == "checkbox") {
                input = m.options.map((o) => {

                    //let checked = o.value == value;
                    let checked = false;
                    if (value && value.length > 0) {
                        checked = value.indexOf(o.value) > -1 ? true : false;
                    }
                    console.log("Checkbox: ", checked);
                    return (
                        <React.Fragment key={"cfr" + o.key}>
                            <input {...props}
                                className="form-input"
                                type={type}
                                key={o.key}
                                name={o.name}
                                checked={checked}
                                value={o.value}
                                onChange={(e) => { this.onChange(e, m.key, "multiple") }}
                            />
                            <label key={"ll" + o.key}>{o.label}</label>
                        </React.Fragment>
                    );
                });
                input = <div className="form-group-dropdown">{input}</div>;
            }

            return (
                (<div key={'g' + key} className={"col-md-" + col} >
                    {m.type != 'label-info' && (!m.show || (m.show && ((m.show.type == 1 && possibleValues.includes(this.state[m.show.field])) || (m.show.type == 0)))) && (<label className="form-label padding-10"
                        key={"l" + key}
                        htmlFor={key}>
                        {m.label}
                    </label>)}
                    {(!m.show || (m.show && ((m.show.type == 1 && possibleValues.includes(this.state[m.show.field])) || (m.show.type == 0)))) && input}
                    {this.state.validations[m.key] && this.state.validations[m.key].touched == true && this.state.validations[m.key].error == true && (<p className="error-msg">{this.state.validations[m.key].errorMessage}</p>)}
                </div>)
            );
        });
        return formUI;
    }

    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen" style={{ zIndex: 10 }}></div>
                <div className="hidden-sm-down position-relative">
                    <div className="modal" style={{ zindex: 10, top: "7%", left: "340px", width: "50%" }}>
                        <div className="container" ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">Create New Cluster</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.onClose}
                                        data-toggle="tooltip-auto" title="Close"
                                        className="mlp-cursor-pointer dls-glyph-close"></image>
                                </div>
                            </div>
                            <div className="usecase-form background-white" style={{ "width": "100%", "maxHeight": "500px", "height": "auto" }}>
                                <form className="dynamic-form row" onSubmit={(e) => { this.onSubmit(e) }}>
                                    {this.renderForm()}
                                    <div className="form-actions">
                                        <div className="button" style={{ "marginTop": "25px" }}>
                                            <button disabled={!this.state.submitActivated} type="submit" style={{
                                                "marginLeft": "320px",
                                                "paddindLeft": "31px"
                                            }}>Submit</button>
                                            <button type="Cancel" onClick={this.onClose} style={{
                                                "marginLeft": "-430px"
                                            }}>Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



