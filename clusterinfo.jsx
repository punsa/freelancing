import React, { Component } from 'react';
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import ClusterOverview from './ClusterOverview.jsx';
import ProcessGrid from "./ProcessGrid.jsx";
import UserSessionGrid from "./UserSessionGrid.jsx";
import Services from "../services/Services.js";

class ClusterInfo extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.handleClose = this.handleClose.bind(this);
        this.setModalRef = this.setModalRef.bind(this);
        this.setProcessRef = this.setProcessRef.bind(this);
        this.setOverviewRef = this.setOverviewRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.updateNoDataTemplate = this.updateNoDataTemplate.bind(this);
        this.state = {
            processData: null,
            userSessionData: null,
            pGridError: null,
            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate: "<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert'>" +
            "<span class='alert-icon dls-icon-info-filled'></span><span>No Record Found.</span></div>"
        };
    }


    componentDidMount() {
        var clstrInfo = this;
        var svcReq = {
            timezone: "LOCAL",
            timezoneOffset: new Date().getTimezoneOffset()
        }
        Services.get("/api/clusters/"+ clstrInfo.props.clusterId + "/processes", svcReq)
            .then(function (response) {
                clstrInfo.setState({ processData: response.data.processes  || [] });
            })
            .catch(function (error) {
                clstrInfo.setState({ pGridError: error });
                console.error('AJAX error occured : ' + error);
            })
        Services.get("/api/clusters/" + clstrInfo.props.clusterId + "/processes/sshd")
            .then(function (response) {
                clstrInfo.setState({ userSessionData: response.data.processes || [] });
            })
            .catch(function (error) {
                clstrInfo.setState({ pGridError: error });
                console.error('AJAX error occured : ' + error);
            })
        this.processRef.click();
        this.overviewRef.click();
        document.addEventListener('mousedown', this.handleClickOutside);
    }


    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(event) {
        if (this.modalRef && !this.modalRef.contains(event.target)) {
            this.handleClose();
        }
    }

    handleClose() {
        this.props.onClose();
    }

    setModalRef(node) {
        this.modalRef = node;
    }

    setProcessRef(node) {
        this.processRef = node;
    }

    setOverviewRef(node) {
        this.overviewRef = node;
    }

    updateNoDataTemplate(info) {
        let msg = info.message ? info.message : "NO DATA";
        this.setState({
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'>" +
            "<span class='alert-icon dls-icon-warning-filled'>" +
            "</span>" + "<span>" + msg + "</span>" + "</div>"
        });
    }
    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen"></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": 600 }}>
                    <div className="modal" style={{ top: "7%" }}>
                        <div className="container" ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">CLUSTER : {this.props.clusterId}</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.handleClose}
                                           data-toggle="tooltip-auto" title="Close"
                                           className="mlp-cursor-pointer dls-glyph-close"></image>
                                </div>
                            </div>
                            <div className="tabs" data-toggle="tabs" role="tablist">
                                <div className="tab-menu">
                                    <select className="form-control mobile">
                                        <option role="tab" data-value="1" aria-selected="true">Overview</option>
                                        <option role="tab" data-value="2" aria-selected="false">Process</option>
                                        <option role="tab" data-value="3" aria-selected="false">User Session</option>
                                    </select>
                                    <div className="form-control desktop">
                                         <div className="tab-link" role="tab" data-value="1" aria-selected="true" style={{ padding: 0 }} ref={this.setOverviewRef}>
                                            <span>Overview</span>
                                        </div>
                                        <div className="tab-link" role="tab" data-value="2" aria-selected="false" style={{ padding: 0 }} ref={this.setProcessRef}>
                                            <span>Process</span>
                                        </div>
                                        <div className="tab-link" role="tab" data-value="3" aria-selected="false" style={{ padding: 0 }}>
                                            <span>User Session</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="dls-accent-white-01-bg tab-content-container">
                                    <div className="tab-content" role="content" data-value="1" aria-selected="true">
                                        <ClusterOverview clusterId={this.props.clusterId} terminatedBy={this.props.terminatedBy}></ClusterOverview>
                                    </div>
                                    <div className="tab-content" role="content" data-value="2" aria-selected="false">
                                        <ProcessGrid rowData={this.state.processData} errorInfo={this.state.pGridError}
                                                     clusterId={this.props.clusterId}></ProcessGrid>
                                    </div>
                                    <div className="tab-content" role="content" data-value="3" aria-selected="false">
                                        <UserSessionGrid rowData={this.state.userSessionData} errorInfo={this.state.pGridError}
                                                         clusterId={this.props.clusterId}></UserSessionGrid>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ClusterInfo;
