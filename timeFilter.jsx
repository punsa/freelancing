import React, { Component } from "react";
import moment from 'moment';
import "./datefilter.css";

export default class TimeFilter extends Component {
constructor(props, context) {
    super(props, context);
   this.state = {
    type: null,
    value: ''

   }
}

isFilterActive() {
    return (this.state.type !== null && this.state.type !== undefined && this.state.type !== '') ||
      (this.state.value !== null && this.state.value !== undefined && this.state.value !== '');
  }

getModel() {
    let type = this.state.type;
    return { filterType: 'text', type: this.state.type, filterkey: this.props.filterKey, filter: this.hhmmsstoTotalSeconds(this.state.value) };
  }

  hhmmsstoTotalSeconds = (hhmmssStr) => {
    var res = 0;
    var arr = hhmmssStr.split(":");
    res = parseInt(arr[0])*3600 + parseInt(arr[1])*60 + parseInt(arr[2]) * 1;
    return res;
  }


applyFilter = () => {
    this.props.filterChangedCallback();
  }

resetFilter = () => {
    this.props.resetTimeFilter();
  }

  onChangeType = (e) => {
    this.setState({ type: e.target.value });
  }
   onChangeValue = (e) => {
    this.setState({ value: e.target.value });
  }
 render() {


 let style = {
      fontSize: "10px",
      width: "260px",
      height: "220px",
      overflowX: "hidden",
      overflowY:"hidden",
    borderRadius: ".25rem",
    lineHeight: "1.45667",
    minHeight: "3.125rem",
    padding: "0 .625rem",
    marginTop: "5px"
    /* margin-right: 9px; */

    };

 return (
<div style={style}>
             <div className="radio-filter">
              Choose Type of Filter:
              </div>
             <fieldset>
               <div className="row">
                <div className="radio" style={{ marginLeft: '16px' }}>
                  <input id="demo-1" className="form-control" type="radio" aria-invalid="false"
                    name="size" checked={this.state.type == "greaterThan"} value="greaterThan" onChange={this.onChangeType} />
                  <label htmlFor="demo-1" style={{ fontSize: '11px' }}>GreaterThan</label>
                </div>

                <div className="radio" style={{ marginLeft: '57px' }}>
                  <input id="demo-2" className="form-control" type="radio" aria-invalid="false"
                    name="size" checked={this.state.type == "lessThan"} value="lessThan" onChange={this.onChangeType} />
                  <label htmlFor="demo-2" style={{ fontSize: '11px' }}>LessThan</label>
                </div>
                </div>
              </fieldset>

  Enter Time:
  <input id="demo-2" className="form-control" type="text" placeholder="HH:MM:SS"
                    name="time" value={this.state.value} onChange={this.onChangeValue} />
             <div className="row" style={{ marginTop: '46px' }}>
              <button className="btn-icon btn-sm" style={{ marginLeft: '16px' }} onClick={this.applyFilter}>Apply</button>
              <button className="btn-icon btn-sm" style={{ marginLeft: '57px' }} onClick={this.resetFilter}>Reset </button>
            </div>

  </div>

 );
 }
}
