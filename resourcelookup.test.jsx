import React from 'react';
import { shallow } from 'enzyme';
import ResourceLookup from '../../src/containers/ResourceLookup';
import Services from '../../src/services/Services';

Services.get = jest.fn((url, payload) => Promise.resolve({
  data: [ {}, {} ],
}));

describe('Resource Lookup', () => {
  let props = {
    isHomeScreen: false,
  };

  it('render snapshot', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('render snapshot with isHomeScreen', () => {
    props = {
      isHomeScreen: null,
    };
    const wrapper = shallow(<ResourceLookup {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('calling dataSizeChange with value', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'dataSizeChange');
    const evt = { target: { value: 1 } };
    instance.dataSizeChange(evt);
    expect(instance.dataSizeChange).toBeCalled();
  });

  it('calling dataSizeChange with value greater than 100', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'dataSizeChange');
    const evt = { target: { value: 101 } };
    instance.dataSizeChange(evt);
    expect(instance.dataSizeChange).toBeCalled();
  });

  it('calling dataSizeChange without value', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'dataSizeChange');
    const evt = { target: { value: '' } };
    instance.dataSizeChange(evt);
    expect(instance.dataSizeChange).toBeCalled();
  });

  it('calling columnsChange with value', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'columnsChange');
    const evt = { target: { value: 1 } };
    instance.columnsChange(evt);
    expect(instance.columnsChange).toBeCalled();
  });

  it('calling columnsChange with value greater than 1000000000', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'columnsChange');
    const evt = { target: { value: 1000000001 } };
    instance.columnsChange(evt);
    expect(instance.columnsChange).toBeCalled();
  });

  it('calling columnsChange without value', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'columnsChange');
    const evt = { target: { value: '' } };
    instance.columnsChange(evt);
    expect(instance.columnsChange).toBeCalled();
  });

  it('calling onSubmit service resolve if', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onSubmit');
    wrapper.setState({ dataSizeValue: 10, columnsValue: 1000 });
    instance.onSubmit();
    expect(instance.onSubmit).toBeCalled();
  });

  it('calling onSubmit service resolve else', () => {
    Services.get = jest.fn((url, payload) => Promise.resolve({
      data: [],
    }));
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onSubmit');
    wrapper.setState({ dataSizeValue: 10, columnsValue: 1000 });
    instance.onSubmit();
    expect(instance.onSubmit).toBeCalled();
  });

  it('calling onSubmit service reject if', () => {
    Services.get = jest.fn((url, payload) => Promise.reject({
      response: { status: 501, data: [] },
    }));

    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onSubmit');
    wrapper.setState({ dataSizeValue: 10, columnsValue: 1000 });
    instance.onSubmit();
    expect(instance.onSubmit).toBeCalled();
  });

  it('calling onSubmit service if', () => {
    Services.get = jest.fn((url, payload) => Promise.reject());

    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'onSubmit');
    wrapper.setState({ dataSizeValue: 10, columnsValue: 1000 });
    instance.onSubmit();
    expect(instance.onSubmit).toBeCalled();
  });

  it('test submit button disbaled', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    wrapper.setState({
      dataSizeValue: 10, columnsValue: 1000, validColumns: true, validDataSize: true,
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('test selectedRow zero', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    wrapper.setState({
      selectedRow: 0, dataSizeValue: 10, columnsValue: 1000, validColumns: true, validDataSize: true,
    });
    wrapper.setState({
      result: [ {
        masterNodeType: true,
        coreNodeType: true,
        nodenum: true,
        coreEBSVol: true,
        masterEBSVol: true,
      } ],
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('test selectedRow non-zero', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    wrapper.setState({
      selectedRow: 1, dataSizeValue: 10, columnsValue: 1000, validColumns: true, validDataSize: true,
    });
    wrapper.setState({
      result: [ {
        masterNodeType: true,
        coreNodeType: true,
        nodenum: true,
        coreEBSVol: true,
        masterEBSVol: true,
      } ],
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('test HomeMenu', () => {
    const wrapper = shallow(<ResourceLookup {...props} />);
    const instance = wrapper.instance();
    instance.isHomeScreen = true;
    wrapper.setState({ isHomeScreen: true });
    expect(wrapper).toMatchSnapshot();
  });
});
