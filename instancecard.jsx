import React, { Component } from "react";
import ReactDOM from 'react-dom';
import Services from "../services/Services.js";
import DynamicForm from '../components/dynamicform/index.jsx';
import ProjectCheckout from "./ProjectCheckout.jsx";
import StatusPopup from '../containers/statuspopup.jsx';
import "../containers/ClusterForm.css";
import LoaderPopup from '../containers/loaderpopup.jsx';


export default class InstancesCard extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      cardSubMenu: false,
      userinfo: props.userinfo,
      cardNotifications: false,
      clusterFormModal: null,
      statusPopup: null,
      tableData: [],
      selectedCluster: null,
      showLoaderPopup: null,
      showRowTooltip: false,
      tooltipInfo: {},
      selectedRow: -1,
      showDataLoading: true,
      overlayLoadingTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span>" +
        "<span>Loading...</span>" +
        "</div>",
      overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span>" +
        "<span>No Cluster found</span>" +
        "</div>"
    };
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
    this.onClose = this.onPopupClose.bind(this);
    this.getClusterData = this.getClusterData.bind(this);

  }

  cardSubMenu = (event) => {
    event.preventDefault();

    this.setState({
      cardSubMenu: true
    }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  toolTipRef = (elem) => {
    this.tooltipDiv = elem;
}


  cardNotifications = (event) => {
    event.preventDefault();
    this.setState({
      cardNotifications: true
    }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  onTerminateClick() {
    this.onClusterdelete();
  }

  onSelectionChanged = (item, index) => {
    this.setState({ selectedCluster: item, selectedRow: index });
  }

  onDeleteClick = () => {
    var ele = <StatusPopup onClose={this.onPopupClose} message="Are you sure you want to terminate the cluster?"
      alertBox={true} />
    this.setState({ statusPopup: ele });
  }

  onClusterdelete() {
    this.setState({clusterFormModal: null, showLoaderPopup: <LoaderPopup />});
   Services.delete("/api/clusters/"+ this.state.selectedCluster.clusterId)
      .then(res => {
        this.setState({ selectedCluster: null, selectedRow: -1,showLoaderPopup: null });
        console.log(res);
        var ele = <StatusPopup onClose={this.onPopupClose} message="Cluster termination is initiated" />
        this.setState({ statusPopup: ele });
        this.getClusterData();
        // refresh cluster grid.
      })
      .catch(function (error) {
        console.error('AJAX error occured :' + error);
      })
  }

  onPopupClose = (status) => {
    this.setState({ statusPopup: null });
    if (status === 1) {
      this.onClusterdelete();
    }
  }

  setDefaultCluster = () => {
    return {
      clustername: "",
      slave_instancecount: "",
      spotprice: ""
    };
  }

  closeMenu = () => {
    this.setState({
      cardSubMenu: false,
      instancesOptions: false,
      dmuseOptions: false,
      cardNotifications: false
    }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }

  Instancesapp = () => {
    window.location.href='/app/user/clusters';
  }


  componentDidMount = () => {
  this.getClusterData();
  }

  getClusterData = () => {
    var parent = this;
    var url = "/api/clusters/user/" + this.state.userinfo.emailId + "?filterType=notEqual&filterKey=clusterStatus&filterValue=TERMINATED&timezone=LOCAL&timezoneOffset=" + new Date().getTimezoneOffset();
    Services.get(url)
      .then(function (response) {
        var resData = response.data.clusters;
        parent.setState({ tableData: resData.slice(0, Math.min(2,resData.length)) || [], showDataLoading:false});
let staticClusters =[
  {"clusterName":"hqidlmllwa02","clusterType": "EMR","clusterEnv":"aws","createdByEmail":"nikhil.cheriyala@aexp.com","clusterId":"hqidlmllwa02"},
  {"clusterName":"goldml","clusterType": "EMR","clusterEnv":"GOLDML","createdByEmail":"nikhil.cheriyala@aexp.com","clusterId":"hjkl87227yz"},
  {"clusterName":"ecp","clusterType": "EMR","clusterEnv":"ECPML","createdByEmail":"nikhil.cheriyala@aexp.com","clusterId":"zxvc87227yz"}
]
for (let i = 0; i< staticClusters.length; i++){
  resData.push( staticClusters[i]);
}
        parent.setState({ tableData: resData.slice(0, Math.min(resData.length)) || [], showDataLoading:false});

    })
      .catch(function (error) {
        if (error.status == undefined) {
          parent.updateNoDataTemplate(error);
        }
      })
  }

  updateNoDataTemplate = (info) => {
    let msg = info.message ? info.message : "No Record Found";
    this.setState({
      overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'>" +
        "<span class='alert-icon dls-icon-warning-filled'>" +
        "</span>" + "<span>" + msg + "</span>" + "</div>"
    });
  }

  onCloseModal = (status) => {
    this.setState({ clusterFormModal: null });
  }


  onModalClose = () => {
    this.setState({ clusterFormModal: null });
    this.setState({ projectOverview: null });
    this.setState({ clusterInfo: null });
    this.setState({ modal: null });
    this.setState({ projectCheckoutModal: null });
  }

  getStyle = (item,index) => {
    let setStyle = [{"status":"Request","color":"#00008B"},{"status":"Initializng","color":"#00BFFF"},{"status":"Ready","color":"#FF4500"},{"status":"Idle","color":"#daa520"},{"status":"Running","color":"#008000"},{"status":"Terminating","color":"#FF0000"}]
    let rowStyle = {fontSize : "12px"};
    for(let i=0; i < setStyle.length ; i++){
      if(setStyle[i].status == item.clusterStatus){
        rowStyle.color = setStyle[i].color;
      }
    }
    rowStyle.backgroundColor = this.state.selectedRow == index ? "#e4f2ef" : "";
    return rowStyle;
  }

  render() {
    return (
      <div className="col-md-6 pad-tb margin-b zoom">
        <div className="card height-auto fluid flex flex-column card-rounded card-block">
          <div>
            <h2 className="heading-4 margin-1-b dls-accent-blue-02" onClick={this.Instancesapp}>INSTANCES</h2>
            <hr />
            {/* <p className="body-1 margin-3-tb">This is body copy. We recommend keeping this copy to a four line maximum.</p> */}
          </div>
          <div>
            {this.state.clusterFormModal}
          </div>
          <div className="modal-container">
            <div style={{ overflowX: "auto", height: "180px", marginBottom: "10px", marginTop: "30px", overflowY: "auto" }}>

              <div className="table-fluid margin-1-b" style={{border:"#C2CED2"}}>
                <table className="table border table-group table-striped pad-lr">
                  <thead>
                    <tr>
                      <th className="body-3" >Name</th>
                      <th className="body-3" >Type</th>
                      <th className="body-3">Start Time</th>
                    </tr>
                  </thead>
                  <tbody >{
                    this.state.tableData.map((item, index) => {
                      return <tr className={"body-1 table-row-link" + (this.state.selectedRow == index ? "selected" : "")} onClick={() => this.onSelectionChanged(item, index)} style={this.getStyle(item, index)} data-toggle="tooltip"
                      data-placement="left"
                      title =   {"STATUS : " + item.clusterStatus} >
        
                    <td>{item.clusterName}</td>
                        <td>{item.clusterType}</td>
                        <td>{item.clusterStartTs}</td>
                      </tr>
                    })
                  }
                    {
                      (this.state.showDataLoading) && (<div className='progress-circle progress-indeterminate progress-md' style={{
                        "left": "147px",
                        "top": "44px"
                      }}>
                      </div>)
                    }
                   {
                      (!this.state.showDataLoading) && (!this.state.tableData || this.state.tableData.length == 0) &&
                      (<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert' style={{
                        "left": "72px",
                        "top": "9px"

                      }}><span class='alert-icon dls-icon-neutral-filled'></span>
                        <span>No Record Found.</span></div>)
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div>
            {/* <button className="btn-icon btn-sm margin-auto-md" style={{ paddingLeft: "0.55rem" }} onClick={this.onCreateClick.bind(this)}>Create</button> */}
            <button className="btn-icon btn-sm margin-auto-md" style={{ marginTop: "6px", paddingLeft: "0.55rem" }} disabled={this.state.selectedCluster == null || this.state.selectedCluster.clusterName == "gold" || this.state.selectedCluster.clusterName == "goldml" ||
              this.state.selectedCluster.clusterName == "ecp" || this.state.selectedCluster.clusterStatus == "Terminating"} onClick={this.onDeleteClick.bind(this)}>Terminate</button>
            {/* <button className="btn-icon btn-sm margin-auto-md" onClick={this.onSubmit}
              disabled={this.state.selectedCluster == null}
              style={{ marginRight: "25px" }}>
              Launch</button> */}
              </div>
        </div>

        <div>
          {this.state.statusPopup}
        </div>
        <div>
          {this.state.projectCheckoutModal}
        </div>
        <div>
                        { this.state.showLoaderPopup }
                    </div>
      </div>
    );
  }

}
