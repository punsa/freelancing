'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const logger = require('winston')
const mlp = require('./mlpService');
const appConfig = require('./../config/app-config');

const env = process.env.EPAAS_ENV || 'e0';


router.get('/role', function (req, res) {
    try {

        
      var email = req.headers.email;
      if(req.query.email) {
          email = req.query.email;
      }
        if(env == 'e0'){
          email = 'nikhil.cheriyala@aexp.com'
        }

        mlp.getCall("/api/v1/ldap/email/"+email, function (err, results) {
            if (!err) {
                logger.info(results);

                var tmpObj = JSON.parse(results);
                var userResponse = {};
                userResponse["adsId"] = tmpObj["adsId"];
                userResponse["firstName"] = tmpObj["firstName"];
                userResponse["lastName"] = tmpObj["lastName"];
                userResponse["emailId"] = tmpObj["emailId"];             
                var groups = tmpObj["groups"];
                var roles = [];
                if(groups && groups.length > 0) {
                    for(var i=0;i < groups.length;i++) {
                        var groupName = groups[i];
                        if(groupName == appConfig.MLP_ADMIN_PRC_GROUP) {
                            roles.push("Admin");
                        }
                         if(groupName == appConfig.MLP_USER_PRC_GROUP) {
                            roles.push("User");
                        }
                    }
                }
                userResponse["roles"] = roles;
                userResponse["groups"] = tmpObj["groups"];
                res.status('200').send(userResponse);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        });
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

module.exports = router

