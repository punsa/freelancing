'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const logger = require('winston')
const mlp = require('./mlpService');
const crypto = require('./cryptoService');
const appConfig = require('./../config/app-config');
var moment = require('moment');

const env = process.env.EPAAS_ENV || 'e0';

function replaceMapperKey(obj) {
    var mapper = {
        "key": "name",
        "data": "children"
    };
    if (obj instanceof Array) {
        var resultArr = []
        for (var i = 0; i < obj.length; i++) {
            if (typeof obj[i] == "object" || obj[i] instanceof Array) {
                resultArr.push(replaceMapperKey(obj[i]));
            }
            else {
                resultArr.push(obj[i]);
            }
        }
        return resultArr;
    }
    var result = {};
    for (var key in obj) {
        var destKey = mapper[key] || key;
        var destValue = obj[key];
        if (typeof destValue === "object") {
            destValue = replaceMapperKey(destValue);
        }
        result[destKey] = destValue;
        if(destKey == "name" && obj["displayText"]) {
            result[destKey] = obj["displayText"];
        }
    }
    return result;
}

function formatTerminatedByData(obj) {
    var result = {};
    result["name"] = "clusters";

    var tmpObj = findKey("agg", obj["clusters"])

    if (tmpObj != 0) {
        result["children"] = replaceMapperKey(tmpObj);
    }
    else {
        result["children"] = replaceMapperKey(obj["clusters"]);
    }
    return result;
}

function findKey(key, arr) {
    var dataValue = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i]["name"] == key || arr[i]["key"] == key) {
            return arr[i]["data"];
        }
    }
    return dataValue;
}

function formatInsightsData(frequency, clusterEndTs, dataValue) {

    var endDate =  moment(clusterEndTs).format("YYYY-MM-DD");
    var startDate = "";
    var responseData = { "clusters": [] };
    var tmpDate;
    var i;
    var tmpDateStr;

    if (frequency == "DAILY") {

        tmpDate = moment(clusterEndTs);
        startDate = tmpDate.subtract(5,'days').format("YYYY-MM-DD")
        for (i = 0; i < 6; i++) {
            tmpDateStr = moment(clusterEndTs).subtract(i,'days').format("YYYY-MM-DD");
            responseData["clusters"].push({ key: tmpDateStr, data: dataValue });
        }
    }
    else if (frequency == "MONTHLY") {

        tmpDate = moment(clusterEndTs);
        startDate = tmpDate.subtract(5,'month').startOf('month').format("YYYY-MM-DD");
        for (i = 0; i < 6; i++) {
            tmpDateStr = moment(clusterEndTs).subtract(i,'month').format("YYYY-MM");
            responseData["clusters"].push({ key: tmpDateStr, data: dataValue });
        }
    }
    else if (frequency == "AGG") {
        startDate = moment(clusterEndTs).subtract(365,'days').format("YYYY-MM-DD");
        responseData["clusters"].push({ key: "agg", data: dataValue });
    }
    else {

        tmpDate = moment(clusterEndTs);
        startDate = tmpDate.subtract(36,'days').startOf('isoWeek').format("YYYY-MM-DD");

        for (i = 0; i < 6; i++) {
            tmpDate = moment(clusterEndTs).subtract(i,'week');
            tmpDateStr = tmpDate.isoWeek()
            var displayText = tmpDate.startOf('isoWeek').format('MM/DD') +"-"+tmpDate.endOf('isoWeek').format('MM/DD')
            responseData["clusters"].push({ key: tmpDateStr, data: dataValue, displayText : displayText });
        }
    }

    let svcReq = {
        startDate: startDate,
        endDate: endDate,
        frequency: frequency
    };

    logger.info("Start Date : "+svcReq.startDate +" End Date : "+svcReq.endDate);

    return [svcReq, responseData];
}

router.get('/terminatedby', function (req, res) {
    try {
        var insightsObj = formatInsightsData(req.query.frequency, req.query.clusterEndTs, null);
        mlp.getTerminatedByData(insightsObj[0], function (err, results) {
            if (!err) {

                var formattedResult = { "name": "clusters", "children": [] };
                results = JSON.parse(results);
                if (results.clusters && results.clusters.length > 0) {
                    for (var i = 0; i < insightsObj[1]["clusters"].length; i++) {
                        insightsObj[1]["clusters"][i]["data"] = findKey(insightsObj[1]["clusters"][i]["key"], results.clusters)
                    }
                    formattedResult = formatTerminatedByData(insightsObj[1]);
                }

                res.status('200').send(formattedResult);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/costSaves', function (req, res) {
    try {
        var insightsObj = formatInsightsData(req.query.frequency, req.query.clusterEndTs, 0);
        mlp.getCostSavings(insightsObj[0], function (err, results) {
            if (!err) {

                results = JSON.parse(results);
                if (results.clusters) {
                    for (var i = 0; i < insightsObj[1]["clusters"].length; i++) {
                        insightsObj[1]["clusters"][i]["data"] = findKey(insightsObj[1]["clusters"][i]["key"], results.clusters);
                        if(insightsObj[1]["clusters"][i]["displayText"]) {
                            insightsObj[1]["clusters"][i]["key"] = insightsObj[1]["clusters"][i]["displayText"];
                        }
                    }
                }
                res.status('200').send(insightsObj[1]);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})




// router.get('/MyTransfers', function (req, res) {
//    try {
//         let endpoint = "/api/v1/dm/MyTransfers?startDate="+ req.query.startDate +"&endDate="+ req.query.endDate +"&frequency=" + req.query.frequency;
//         mlp.getCall(endpoint, function (err, results) {
//            if (!err) {
//                 results = JSON.parse(results);
//                 res.status('200').send(results);
//             } else {
//                 res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
//             }
//         })

//     } catch (e) {

//         res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
//     }
// })




module.exports = router
