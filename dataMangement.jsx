import React, { Component } from "react";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import 'ag-grid/dist/styles/ag-grid.css';
import '../public/css/mlp-ag-grid.scss';
import appConfig from "../config/web-config";
import Services from "../services/Services.js";
import AdminMenu from '../components/AdminMenu.jsx';
import StatusFilter from '../components/filters/statusfilter.jsx';
import FileSizeFilter from '../components/filters/filesizefilter.jsx';
import DataTransferInfo from "./DataTransferInfo.jsx";
import DmUserActions from "./dmuseractions.jsx";
import moment from "moment";
import DmApprovalButton from "./dmApprovalButton.jsx";
import { HomeMenu } from '../components/HomeMenu.jsx';


export default class DataManagement extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            userinfo: props.userinfo,
            isHomeScreen: props.isHomeScreen,
            rowData: null,
            selectedRow: -1,
            dataGrid: null,
            filterKey: null,
            filterType: null,
            isHomeScreen: props.isHomeScreen == null ? false : props.isHomeScreen,
            filterValue: null,
            sortType: null,
            sortBy: null,
            selectedDM: null,
            DMRetryCommentBox: null,
            startRow: 1,
            enableCommentBtn: false,
            endRow: appConfig.DM_GRID_BLOCK_SIZE,
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
                    }
                }
            },
            frameworkComponents: {
                statusFilter: StatusFilter,
                fileSizeFilter: FileSizeFilter
            },
            rowBuffer: 0,
            rowModelType: "infinite",
            paginationPageSize: appConfig.DM_GRID_PAGE_SIZE,
            cacheBlockSize: appConfig.DM_GRID_BLOCK_SIZE,
            maxConcurrentDatasourceRequests: 2,
            infiniteInitialRowCount: 0,
            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span><span>No Record Found.</span></div>",
            showOverlay: false
        };
        //this.valueFilterKey = dmTransferStatus;
        this.statusFilterOptions = [
            { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
            { value: 'SUBMITTED', label: 'SUBMITTED' },
            { value: 'CREATED', label: 'CREATED' },
            { value: 'IN_PROGRESS', label: 'IN PROGRESS' },
            { value: 'XFER_COMPLETED', label: 'TRANSFER COMPLETED' },
            { value: 'XFER_FAILED', label: 'TRANSFER FAILED' },
            { value: 'XFER_ON_HOLD', label: 'TRANSFER HOLD' },
            {value: 'XFER_INITIATION_FAILED', label: 'TRANSFER INITIATION FAILED'},
            { value: 'FAILED_LONG_PNDG_APRVL', label: 'TRANSFER AUTO REJECT'},
            { value: 'XFER_REJECTED', label : 'TRANSFER REJECTED'}
        ]
    }
    onGridReady = (params) => {

        var parent = this;
        this.gridApi = params.api;
        this.columnApi = params.columnApi;

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                parent.gridApi.showLoadingOverlay()
                parent.state.startRow = params.startRow + 1;
                parent.state.endRow = params.endRow;


                parent.loadData(function () {

                    setTimeout(function () {
                        var data = parent.state.dataGrid;
                        var rowsThisPage = data.slice(params.startRow, params.endRow);
                        var lastRow = -1;
                        if (data.length < params.endRow) {
                            lastRow = data.length;
                        }
                        if (rowsThisPage.length > 0) {

                            parent.gridApi.hideOverlay();

                        } else if (data.length > 0 && params.startRow == data.length) {

                            parent.gridApi.hideOverlay();

                        } else {
                            parent.gridApi.showNoRowsOverlay();
                        }
                        params.successCallback(rowsThisPage, lastRow);
                    }, 500);
                });
            }
        };

        params.api.setDatasource(dataSource);
    };

    columnRowGroupChanged = (params) => {
    }

    onFilterModified = (params) => {
        var filters = Object.keys(params.api.getFilterModel());
        var filterLength = filters.length;
        var oldFilterKey = this.state.filterKey;
        var newFilterKey = null;
        if (filterLength == 1) {
            if (oldFilterKey && filters[0] != oldFilterKey) {
                var filterInstance = params.api.getFilterInstance(oldFilterKey);
                filterInstance.setModel(null);
            }
            newFilterKey = filters[0];
        } else if (filterLength > 1) {

            for (var i = 0; i < filterLength; i++) {
                if (filters[i] == oldFilterKey) {
                    var filterInstance = params.api.getFilterInstance(oldFilterKey);
                    filterInstance.setModel(null);
                } else {
                    newFilterKey = filters[i];
                }
            }
        }
        this.state.filterKey = newFilterKey;
    }
    disableOtherFilters = (params) => {
        //loop colList
        //var colRef = params.api.columnController.getPrimaryColumn('transferId').colDef;
        //colRef.suppressFilter = true;
        //var colRef = params.api.columnController.getPrimaryColumn('dmTransferFileSize').colDef;
        //colRef.suppressFilter = true;
        //var colRef = params.api.columnController.getPrimaryColumn('dmTransferStatus').colDef;
        //colRef.suppressFilter = true;
        //var colRef = params.api.columnController.getPrimaryColumn('timeToTransfer').colDef;
        //colRef.suppressFilter = true;
        //this.refs.DMGridRef.gridOptions.api.refreshHeader();
    }
    onFilterChanged = (params) => {
        //this.disableOtherFilters(params);
        var filters = params.api.getFilterModel();
        //this.resetFilter();
        if (Object.keys(filters).length == 1) {
            var filterKey = Object.keys(filters)[0];
            this.state.filterKey = filterKey;
            if (filters[filterKey].type == "inRange") {
                this.state.filterType = "between";
                this.state.filterValue = filters[filterKey].filter + '~' + filters[filterKey].filterTo;
            }
            else {
                this.state.filterType = filters[filterKey].type;
                this.state.filterValue = filters[filterKey].filter;
            }

        } else if (Object.keys(filters).length == 0) {
            this.state.filterKey = null;
            this.state.filterType = null;
            this.state.filterValue = null;
        }
    }
    resetFilter = () => {
       this.gridApi.destroyFilter("dmTransferStatus");
    }
    resetFileSizeFilter = () => {
       this.gridApi.destroyFilter("dmTransferFileSize");
    }
    onSortChanged= (params) => {
        var sortBy = params.api.getSortModel();
        if (sortBy.length > 0) {
            this.state.sortBy = sortBy[0].colId;
            this.state.sortType = sortBy[0].sort;
        } else {
            this.state.sortBy = null;
            this.state.sortType = null;
        }
    }
    loadData = (callback) => {

        var parent = this;
        var svcReq = {
            startRow: parent.state.startRow,
            endRow: parent.state.endRow,
            filterKey: parent.state.filterKey,
            filterType: parent.state.filterType,
            filterValue: parent.state.filterValue,
            sortType: parent.state.sortType,
            sortBy: parent.state.sortBy,
            timezone: "LOCAL",
            timezoneOffset: new Date().getTimezoneOffset()
        }

        var url = "/api/dm/dataTransfer";

        if (this.state.isHomeScreen) {
            url += '/user/' + this.state.userinfo.emailId;
        }
        else {
            url += '/user/ALL';
        }
        Services.get(url, svcReq)
            .then(res => {
                var resData = res.data;
                if (res.data.constructor == Object) {
                    resData = [];
                }
                var data = parent.state.dataGrid;
                if (data && parent.state.startRow > 1) {
                    data = data.concat(resData);
                } else {
                    data = resData;
                }
                parent.state.dataGrid = data;
                callback()
            })
            .catch(function (error) {
                parent.updateNoDataTemplate(error);
                parent.gridApi.showNoRowsOverlay();
                console.error('AJAX error occured :' + error);
            })
    }

    convertIntoFormData = () => {
        return {
            transferId: this.state.lastSelectedRowData.transferId,
            comments: this.prevComments || []
        };
    }

    onRowSelectionChanged = (event) => {
        if (event.api.getSelectedRows().length && this.state.userInfo.hasAdminRole)
            this.setState({ enableEditBtn: true })
        // else this.setState({enableEditBtn: false})
    }

    onGridRowClick = (params) => {
        const rowData = params.data;
        let enableCommentBtn = (rowData.dmTransferStatus && rowData.dmTransferStatus == "PIICHECK FAILED") ? true : false;
        this.setState({ enableCommentBtn: enableCommentBtn, lastSelectedRowData: rowData });
    }
    onClickReload = () => {
        window.location.reload();
    }
    updateNoDataTemplate = (info) => {
        let msg = info.message ? info.message : "No Record Found";
        //let refreshNode = <Overlay/>
        this.setState({
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'>" +
            "<span class='alert-icon dls-icon-warning-filled'>" +
            "</span>" + "<span>" + msg + "</span>" + "</div>"
        });
    }
    showOverlay = (show) => {
        this.setState({showOverlay: show});
    }
    onClkExportToExcel = (event) => {
        this.refs.DMGridRef.gridOptions.api.exportDataAsCsv();
    }
    refreshPage= () => {
        var params = { type: "gridReady", api: this.gridApi, columnApi: this.columnApi };
        this.onGridReady(params);
    }
    timeTransferFormatter = (params) => {
        if (params && params.data && params.data.timeToTransfer !== undefined) {
            return this.hhmmss(params.data.timeToTransfer);
        } else return "";
    }
    bytesToSize = (bytes) => {
        //return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            if(bytes < 1024) return bytes + " Bytes";
            else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KB";
            else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MB";
            else return(bytes / 1073741824).toFixed(3) + " GB";
    }

    onRowDoubleClicked = (row) => {
        var dataTransferComponent = <DataTransferInfo onClose={this.onModalClose} transferId={row.data.transferId} userInfo={this.props.userInfo} />
        this.setState({ transferId: row.data.transferId, dataTransfer: dataTransferComponent});
    }

    onModalClose = () => {
        this.setState({ dataTransfer: null });
        this.setState({ DMRetry: null });
        this.setState({ DMRetryCommentBox: null });
    }

    dmTransferFileSizeFormatter = (params) => {
        if (params && params.data && params.data.dmTransferFileSize !== undefined) {
            return this.bytesToSize(params.data.dmTransferFileSize);
        } else return "";
    }
    hhmmss= (totalSeconds) => {
        var hours = 0;
        totalSeconds = Math.round(totalSeconds);

        if (totalSeconds > 3600) {
            hours = Math.floor(totalSeconds / 3600);
            totalSeconds = totalSeconds - (hours * 3600);
        }

        var minutes = 0;
        if (totalSeconds > 60) {
            minutes = Math.floor(totalSeconds / 60);
            totalSeconds = totalSeconds - (minutes * 60);
        }

        var seconds = Math.round(totalSeconds);

        // round seconds
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds < 10 ? "0" + seconds : seconds);
        return result;
    }
    statuscolumnrenderer = (params) => {
        return params.value;
    }
    statusCellRenderer = (params) =>  {
        var value = params.colDef.field.dmTransferStatus;
        return value;
    }

    render() {
        return (
            <div>
                {(this.isHomeScreen == true && <HomeMenu tab={"1"} />)}
                {(!this.state.isHomeScreen && (<AdminMenu datamanagement={true} scorecard={false} usecase={false}/>))}
                { this.state.userinfo && !this.state.userinfo.hasAdminRole && <NavHeader selectedTab={"1"} /> }
                <div className="main-container">
                    <span className="heading-3">DATA MANAGEMENT</span>
                    <span className="cluster-excel">
                        <image onClick={this.onClkExportToExcel}
                            data-toggle="tooltip-auto" title="Export to Excel"
                            className="mlp-cursor-pointer icon-md dls-icon-download"></image>
                    </span>
                    <span className="main-refresh">
                        <image className="mlp-cursor-pointer icon-md dls-icon-refresh"
                            data-toggle="tooltip-auto"
                            title="Reload" onClick={this.refreshPage}></image>
                    </span>
                </div>
                <div className="cluster-grid" height="500px">
                    <div id="myGrid" style={{ height: '89%', width: '100%' }} className="ag-mlp" >
                        <AgGridReact
                            ref="DMGridRef"
                            rowData={this.state.dataGrid}
                            components={this.state.components}
                            headerHeight='50' rowHeight='40' //rowSelection="single"
                            animateRows
                            enableSorting
                            enableServerSideSorting
                            enableServerSideFilter
                            enableColResize
                            onRowDoubleClicked={this.onRowDoubleClicked.bind(this)}
                            onFilterChanged={this.onFilterChanged}
                            onFilterModified={this.onFilterModified}
                            columnRowGroupChanged={this.columnRowGroupChanged}
                            columnEverythingChanged={this.columnRowGroupChanged}
                            onSortChanged={this.onSortChanged}
                            onGridReady={this.onGridReady}
                            rowBuffer={this.state.rowBuffer}
                            rowDeselection={true}
                            suppressRowClickSelection={true}
                            rowModelType={this.state.rowModelType}
                            cacheBlockSize={this.state.cacheBlockSize}
                            cacheOverflowSize={this.state.cacheOverflowSize}
                            maxConcurrentDatasourceRequests={this.state.maxConcurrentDatasourceRequests}
                            infiniteInitialRowCount={this.state.infiniteInitialRowCount}
                            maxBlocksInCache={this.state.maxBlocksInCache}
                            pagination={true}
                            paginationPageSize={this.state.paginationPageSize}
                            overlayLoadingTemplate={this.state.overlayLoadingTemplate}
                            overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                            frameworkComponents={this.state.frameworkComponents}
                            rowSelection={this.state.rowSelection}
                            onRowClicked={this.onGridRowClick.bind(this)}>
                            <AgGridColumn headerName="Transfer ID" field="transferId" width={195} suppressFilter={false}
                                filter="agNumberColumnFilter" filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true }}></AgGridColumn>
                            <AgGridColumn headerName="User ID" field="reqUserId" width={220} suppressFilter={false} filter="agTextColumnFilter"
                                          filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true, filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'] }}></AgGridColumn>
                            <AgGridColumn headerName="Status" field="dmTransferStatus" width={220} suppressFilter={false}
                                filter="statusFilter" filterParams={{ filterOptions: this.statusFilterOptions, clearButton: true, filterKey: 'dmTransferStatus', filterType: 'equals', filterResetCallback: this.resetFilter }}></AgGridColumn>
                            <AgGridColumn headerName="File Size (Bytes, KB, MB, GB)" valueFormatter={this.dmTransferFileSizeFormatter} field="dmTransferFileSize" width={280} suppressFilter={false}
                                          filter="fileSizeFilter" filterParams={{ filterOptions: this.fileSizeFilterOptions, clearButton: true, filterKey: 'dmTransferFileSize', filterType: 'equals', resetFileSizeFilter: this.resetFileSizeFilter }}
                                          //filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true, filterOptions: ['greaterThan', 'lessThan'] }}
                            ></AgGridColumn>
                            <AgGridColumn headerName="Source" field="dmTransferSource" width={285} filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true, filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'] }}></AgGridColumn>
                            <AgGridColumn headerName="Destination" field="dmTransferDestination" width={280} filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true, filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith'] }}></AgGridColumn>
                            <AgGridColumn headerName="Time to transfer" valueFormatter={this.timeTransferFormatter} field="timeToTransfer" width={200} suppressFilter={false}
                                filter="agNumberColumnFilter" filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true }}></AgGridColumn>
                            <AgGridColumn headerName="Start Time" field="dmTransferStartTime" width={195} suppressFilter={true}></AgGridColumn>
                            <AgGridColumn headerName="End Time" field="dmTransferEndTime" width={180} suppressFilter={true}></AgGridColumn>
                            </AgGridReact>
                    </div>
                </div >
                <div>
                    {this.state.dataTransfer}
                </div>
                <div>
                    {this.state.DMRetryCommentBox}
                </div>
            </div>

        );
    }
}
