'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const mlp = require('./mlpService');
const crypto = require('./cryptoService');
const appConfig = require('./../config/app-config');
var moment = require('moment');
const mlpLogger = require('./loggerService');
const logger = mlpLogger.createLogger();

const env = process.env.EPAAS_ENV || 'e0';

function getUseCases(token, ucId, userRequest, svcReq, callback) {
    let endPoint = "/api/v1/usecases";
    if (ucId && ucId != "") {
        if(userRequest) {
            endpoint += "/user"
        }
        endPoint += "/" + ucId;
    }
    if (svcReq) {
        endPoint = endPoint + "?"
    }

    for (var key in svcReq) {
        if (svcReq[key]) {
            endPoint = endPoint + key + "=" + svcReq[key] + "&"
        }
    }
    mlp.executeRequest(endPoint, 'GET', token, {}, callback);
}

function createUseCase(token, scvReq, callback) {
    let endPoint = "/api/v1/usecases";
    mlp.executeRequest(endPoint, 'POST', token, scvReq, callback);
}

function updateUseCase(token, ucId, scvReq, callback) {
    let endPoint = "/api/v1/usecases/" + ucId;
    mlp.executeRequest(endPoint, 'PUT', token, scvReq, callback);
}

function deleteUseCase(token, useCaseId, callback) {
    let endPoint = "/api/v1/usecases/" + useCaseId;
    mlp.executeRequest(endPoint, 'DELETE',token, {}, callback);
}

function setFilters(filterType, filterValue) {
    logger.debug("filtertype: :- " + filterType);
    logger.debug("filtervalue: :- " + filterValue);
    if (filterType == null || filterValue == null)
        return [filterType, filterValue];
    var pattern = null;
    var a = filterValue;
    switch (filterType) {
        case "equals": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "notEqual": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "startsWith": pattern = new RegExp("^" + a + ".*$", "i");
            break;
        case "endsWith": pattern = new RegExp("^.*" + a + "$", "i");
            break;
        case "contains": pattern = new RegExp("^.*" + a + ".*$", "i");
            break;
        default: logger.debug("Not Found Filter Type : " + filterType);
    }

    if (pattern == null)
        return [filterType, filterValue];
    for (var key in clusterStatusMapper) {
        if (pattern.test(key)) {
            if (filterType === "equals")
                return ["contains", clusterStatusMapper[key]];
            return [filterType, clusterStatusMapper[key]];
        }
    }
    logger.debug("filterVal :- " + filterValue + " does not match any key in clusterStatusMapper");
    return [filterType, filterValue];
}

router.get('/user/:id', function(req, res){
    getUsecaseHelper(req, res, true);
})

function getUsecaseHelper(req, res, userRequest) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        let endPoint = "/api/v1/usecases";
        if (!validateFilterInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }
        
        var filterType = null;
        if (req.query.filterType) {
            switch (req.query.filterType) {
                case 'lessThanOrEqual': filterType = "lessThanEqual"
                    break;
                case 'greaterThanOrEqual': filterType = "greaterThanEqual"
                    break;
                default: filterType = req.query.filterType;
                    break;
            }
        }

        let svcReq = {
            startRow: req.query.startRow, endRow: req.query.endRow,
            filterKey: req.query.filterKey, filterType: filterType, filterValue: req.query.filterValue,
            sortBy: req.query.sortBy, sortType: req.query.sortType,
        }
        getUseCases(token,userRequest ? req.params.id : req.query.ucId, userRequest, svcReq,function (err, results) {
          //mlp.getCall(endPoint,token,function (err, results) {
            var et = new Date().getTime();
            // var ucOwnerEmailId = req.body.useCaseMetadata["ucOwnerEmailId"];
            logger.info('get usecase loading time', et - st + " milliseconds");
            if (!err) {
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
}


router.get('/', function (req, res) {
    getUsecaseHelper(req, res, false);
})

router.post('/', function (req, res) {
    try {
        var st = new Date().getTime();
        var endPoint = "/api/v1/usecases";
        // var requesterEmail = req.headers.email;
        //var requesterEmail = userResponse["emailId"];
        //var useCaseMetadata = req.header.requesterEmailId;
        var token = req.userContext.tokens['access_token'];
        var requesterEmail = req.body.useCaseMetadata["requesterEmailId"];
        if (env == 'e0') {
            requesterEmail = "mlp_cloud@aexp.com"
        }
        req.body.useCaseMetadata["requesterEmailId"] = requesterEmail;

        if (req.body.onBoardStatus && req.body.onBoardStatus.length > 0 && req.body.useCaseDesc && req.body.useCaseDesc.length > 0 && req.body.useCaseName && req.body.useCaseName.match(/[a-zA-Z0-9]*/)[0] == req.body.useCaseName && req.body.useCaseType && req.body.useCaseType.length > 0 && req.body.useCaseMetadata.cmptEnv && req.body.useCaseMetadata.cmptEnv.length > 0) {
            if (req.body.useCaseType.toUpperCase() == "Application" && !isNaN(parseInt(req.body.applicationId)) || req.body.useCaseMetadata.ucOwnerEmailId && req.body.useCaseMetadata.ucOwnerEmailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)[0] == req.body.useCaseMetadata.ucOwnerEmailId) {

                mlp.postCall(endPoint, token, req.body, function (err, results) {
                    var et = new Date().getTime();
                    logger.info('create usecase loading time', et - st + " milliseconds");
                    if (!err) {
                        logger.debug(results);
                        var status = '200';
                        if (results && results.statusCode == 409) {
                            status = results.statusCode;
                        }
                        res.status(status).send('{"status":  "created"}');
                    } else {
                        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
                    }
                });
            }

            else {
                res.status('500').send('{"status" : "failed", "statusCode" :"Invalid appplicationId or ucOwnerEmailId" }');
            }
        }
        else {
            res.status('500').send('{"status" : "failed", "statusCode" :"Invalid api params" }');
        }
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP7a01" }');
    }
})

router.put('/:useCaseId', function (req, res) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        var endPoint = "/api/v1/usecases/" + req.params.useCaseId;

        if (req.body.onBoardStatus && req.body.onBoardStatus.length > 0 && req.body.useCaseDesc && req.body.useCaseDesc.length > 0 && req.body.useCaseName && req.body.useCaseName.match(/[a-zA-Z0-9]*/)[0] == req.body.useCaseName && req.body.useCaseType && req.body.useCaseType.length > 0) {

            if ((!req.body.comments || req.body.comments.length == 0 || req.body.useCaseMetadata.awsIamRole && ["DataScientistFraudModeler", "DataScientistPOC", "GRBCModeler", "PlatformModeler", "Admin", "VendorModeler1", "VendorModeler2", "DataScientistCreditModeler"].indexOf(req.body.useCaseMetadata.awsIamRole)) && req.body.useCaseMetadata.cmptEnv && req.body.useCaseMetadata.cmptEnv.length > 0) {

                if (req.body.useCaseType.toUpperCase() == "APPLICATION" && !isNaN(parseInt(req.body.applicationId)) || req.body.useCaseMetadata.ucOwnerEmailId && req.body.useCaseMetadata.ucOwnerEmailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)[0] == req.body.useCaseMetadata.ucOwnerEmailId) {

                    var commentsValid = true;
                    if (req.body.useCaseMetadata.comments && req.body.useCaseMetadata.comments.length > 0) {
                        var commentObj = req.body.useCaseMetadata.comments[req.body.useCaseMetadata.comments.length - 1];
                        if (commentObj.workStreamName && commentObj.workStreamName.match(/[a-zA-Z0-9/-]*/)[0] == commentObj.workStreamName && commentObj.comment && commentObj.comment.length > 0) {
                            commentsValid = true;
                        } else {
                            commentsValid = false;
                        }
                    }
                    if (commentsValid) {
                        mlp.putCall(endPoint, token, req.body, function (err, results) {
                            var et = new Date().getTime();
                            logger.info('update usecase loading time', et - st + " milliseconds");
                            if (!err) {
                                logger.debug(results);
                                res.status('200').send(results);
                            } else {
                                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
                            }
                        })
                    }
                }

                else {
                    res.status('500').send('{"status" : "failed", "statusCode" :"Invalid appplicationId or ucOwnerEmailID"}');
                }
            }
            else {
                res.status('500').send('{"status" : "failed", "statusCode" :"Invalid awsIamRole" }');
            }
        }
        else {
            res.status('500').send('{"status" : "failed", "statusCode" :"Invalid api params" }');
        }
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})


router.delete('/:useCaseId', function (req, res) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        var endPoint = "/api/v1/usecases/" + req.params.useCaseId;
        mlp.deleteCall(endPoint, token, {}, function (err, results) {
            var et = new Date().getTime();
            logger.info('Delete usecase loading time', et - st + " milliseconds");
            if (!err) {
                logger.debug(results);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/prcgroups_validity/:prcgrps', function (req, res) {
    try {
        var token = req.userContext.tokens['access_token']
        var endPoint = "/api/v1/ldap/prcgroups/users";
        var body = { "prcGroupList": req.params.prcgrps.split(",") }
        mlp.postCall(endPoint, token, body, function (err, results) {
            if (!err) {
                res.status('200').send({ valid: true });
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/prcgroups_users/:prcgrps', function (req, res) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        var endPoint = "/api/v1/ldap/prcgroups/users";
        var body = { "prcGroupList": req.params.prcgrps.split(",") }
        mlp.postCall(endPoint, token, body, function (err, results) {
            var et = new Date().getTime();
            logger.info('prcGroups loading time', et - st + " milliseconds");
            if (!err) {
                results = JSON.parse(results);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/:ucId', function (req, res) {
    try {
        var st = new Date().getTime();
        var token = req.userContext.tokens['access_token']
        getUseCases(token, req.params.ucId, false, null, function (err, results) {
            var et = new Date().getTime();
            logger.info('Indiviual usecase loading time', et - st + " milliseconds");
            if (!err) {
                results = JSON.parse(results);
                results["useCaseMetadata"]["comments"] = mlp.convertTimeFieldAsTimezone(results['useCaseMetadata']['comments'], ["timestamp"], req.query.timezone, req.query.timezoneOffset);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/insert', function (req, res) {
    try {
        var data = JSON.parse(req.query.data);
        mlp.postUseCase(data, function (err, results) {
            if (!err) {
                res.status('201').send('{"status":  "created"}');
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        });
    }
    catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/update', function (req, res) {
    try {
        var data = JSON.parse(req.query.data);
        mlp.putUseCase(req.query.ucId, data, function (err, results) {
            if (!err) {
                res.status('201').send('{"status":  "updated"}');
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        });
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

function validateFilterInput(req) {
    var data = req.query;
    var filterValueRegex = /^[a-zA-Z0-9\-\_~\.@:/,]+$/
     if ((!data.filterKey || data.filterKey && data.filterKey.length <= 25 && validator.isAlpha(data.filterKey)) &&
        (!data.filterValue || data.filterValue && data.filterValue.length <= 75 && filterValueRegex.test(data.filterValue)) &&
        (!data.filterType || data.filterType && data.filterType.length <= 25 && validator.isAlpha(data.filterType)) &&
        (!data.startRow || data.startRow && validator.isNumeric(data.startRow) && data.startRow <= 100000) &&
        (!data.endRow || data.endRow && validator.isNumeric(data.endRow) && data.endRow <= 100000) &&
        (!data.sortBy || data.sortBy && data.sortBy.length <= 25 && validator.isAlpha(data.sortBy)) &&
        (!data.sortType || data.sortType && data.sortType.length <= 5 && validator.isAlpha(data.sortType))) {
        return true;
    } else {
        logger.info("Validation Failed");
        return false;
    }
}

module.exports = router;