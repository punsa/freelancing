import React from 'react';
import "./UsecaseForm.css";
import "./usecaseOverview.css";
import "./commentbox.css";
import Services from "../services/Services.js";

class ClusterOverview extends React.Component {

    constructor(props, context) {
        super(props, context);
        // this.onClkExportToExcel = this.onClkExportToExcel.bind(this);
        // this.formatResponse = this.formatResponse.bind(this);
        this.state = {
            clusterId: this.props.clusterId,
            terminatedBy: this.props.terminatedBy,
            data: [],
            clusterName: '',
            masterNodeIP: ''
        };

    }

    camelcaseIntoWords = (input) => {
        return input.replace(/([a-z](?=[A-Z]))/g, '$1 ').split(" ").map(obj => { return obj.toUpperCase() }).join(" ")
    }

    formatData = (data) => {
        var res = [];
        var mid = Math.ceil(data.length / 2);
        //var mid = data.length/2;
        for (var i = 0; i < data.length; i++) {

            if (i < mid) {
                res[i] = data[i];
            }
            else {
                res[i - mid]["key1"] = data[i]["key"];
                res[i - mid]["value1"] = data[i]["value"];
            }
        }
        return res;
    }

    formatEC2Response = (res) => {
        var keys = Object.keys(res);
        var data = [];
        for (var i = 0; i < keys.length; i++) {
            if (keys[i] == "stackId" || keys[i] == "availabilityZone" || keys[i] == "clusterName" || keys[i] == "clusterType" || keys[i] == "masterNodeIP") {
                continue;
            }
            var obj = { key: this.camelcaseIntoWords(keys[i]), value: " " }
            if (typeof (res[keys[i]]) == "object") {

                var arr = this.formatEC2Response(res[keys[i]]);
                for (var j = 0; j < arr.length; j++) {
                    arr[j].key = this.camelcaseIntoWords(arr[j].key);
                }
                //data.push(obj);
                data = data.concat(arr);
            }
            else {
                obj.value = res[keys[i]];
                data.push(obj);
            }
        }
        return data;
    }


    formatResponse = (res) => {
        var data = [];
        if (res["clusterType"] == "EC2") {
            var tmpData = this.formatEC2Response(res);
            data = this.formatData(tmpData);
            return data;
        }
        if (res["instances"]) {
            data.push({ key: 'NODE TYPE', value: res["instances"][0]["role"], key1: 'NODE TYPE', value1: res['instances'][1] ? res["instances"][1]["role"] : '' });
            data.push({ key: 'INSTANCE TYPE', value: res["instances"][0]["type"], key1: 'INSTANCE TYPE', value1: res['instances'][1] ? res["instances"][1]["type"] : '' });
            data.push({ key: 'COUNT', value: res["instances"][0]["count"], key1: 'COUNT', value1:res['instances'][1] ? res["instances"][1]["count"] : '' });
            data.push({ key: 'PRICING TYPE', value: res["instances"][0]["pricingType"], key1: 'PRICING TYPE', value1: res['instances'][1] ? res["instances"][1]["pricingType"] : '' });
            if (this.state.terminatedBy != 'SYSTEM' && this.state.terminatedBy != 'USER')
                data.push({ key: 'FAILURE REASON', value: res["failureReason"], key1: 'FAILURE DETAILS', value1: res["failureDetail"] });
        }
        else {
            data.push({ key: 'ROLE', value: '', key1: 'ROLE', value1: '' });
            data.push({ key: 'TYPE', value: '', key1: 'TYPE', value1: '' });
            data.push({ key: 'COUNT', value: '', key1: 'COUNT', value1: '' });
            data.push({ key: 'PRICING TYPE', value: '', key1: 'PRICING TYPE', value1: '' });
            if (this.state.terminatedBy != 'SYSTEM' && this.state.terminatedBy != 'USER')
                data.push({ key: 'FAILURE REASON', value: '', key1: 'FAILURE DETAILS', value1: '' });
        }

        // if(res['instanceIps']) {
        //     data.push({key: 'MASTER IP', value: res['instanceIps'][0], key1: 'CORE IP', value1: res['instanceIps'][1]});
        // }
        // else {
        //     data.push({key: 'MASTER IP', value: '', key1: 'CORE IP', value1: ''});
        // }
        if (res['instances'] && res['instances'][0]['role'].toUpperCase() == 'CORE') {
            var tmp = Object.assign({}, data[data.length - 1]);
            data[data.length - 1].key = tmp.key1;
            data[data.length - 1].value = tmp.value1;
            data[data.length - 1].key1 = tmp.key;
            data[data.length - 1].value1 = tmp.value;
        }
        data.push({ key: 'EMR ID', value: res["emrId"], key1 : '', value1 : ''});
        return data;
    }

    componentDidMount = () => {
        Services.get("/api/clusters/" + this.state.clusterId + "/overview").then(res => {
            var data = this.formatResponse(res["data"]);
            this.setState({ data: data, clusterName: res["data"]["clusterName"] });
            this.setState({ data: data, masterNodeIP: res["data"]["masterNodeIP"] });
        })
    }



    //  onGridReady(params) {
    //     var parent = this;
    //     this.gridApi = params.api;
    //     this.columnApi = params.columnApi;

    // };

    //   onClkExportToExcel(event) {
    //         var params = {
    //         }
    //         this.refs.overviewRef.gridOptions.api.exportDataAsCsv();
    //     }

    render() {
        return (
            <div className="modal-grid">
                <div id="myProcessGrid" style={{ height: '100%', width: '100%' }} className="ag-mlp">
                    <div style={{ "overflowX": "auto", "overflowY": "auto", "height": "450px" }}>
                        <div>
                            {this.state.data && (<div className={"row"}>
                                <div className={"col-md-6"}><span style={{ "fontSize": "small", "fontWeight": "bold" }}>CLUSTER NAME : </span> <span style={{ "color": "#006FCF" }}> {this.state.clusterName} </span></div>
                                <div className={"col-md-6"}><span style={{ "fontSize": "small", "fontWeight": "bold" }}>IP ADDRESS : </span> <span style={{ "color": "#006FCF" }}> {this.state.masterNodeIP} </span> </div>
                            </div>)
                            }
                            {this.state.data && this.state.data.map((value, index) => {
                                return (<div className={"row"} key={index} href="#data1">
                                    <div className={"col-md-6"} style={{ "margin-top": "10px" }}><span style={{ "fontSize": "small", "fontWeight": "bold" }}>{value["key"]}</span> : <span style={{ "color": "#006FCF" }}> {value["value"]} </span></div>
                                    {value["key1"] != '' && value["key1"] && (<div className={"col-md-6"} style={{ "margin-top": "10px" }}><span style={{ "fontSize": "small", "fontWeight": "bold" }}>{value["key1"]}</span> : <span style={{ "color": "#006FCF" }}>{value["value1"]}</span></div>)}
                                </div>)
                            })}

                            {/*{((!this.state.data || this.state.data.length == 0) && (
                                <div class="alert alert-neutral alert-dismissible anim-fade in" style={{ "margin-top": "120px" }} role="alert">
                                    <span class="alert-icon dls-icon-info-filled"></span>
                                    <span>No overview is available </span>
                                </div>
                            ))}*/}
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default ClusterOverview;


// { "stackId":"arn:aws:cloudformation:us-east-1:405188027262:stack/platml-emr-921-tan/f50b8880-bdee-11e8-b425-500c219a98d2", "emrId":"j-2HDUEXXB0UNXT", "instances":[{ "role": "CORE", "type": "m4.large", "count": 1, "pricingType": "SPOT" }, { "role": "MASTER", "type": "m4.large", "count": 1, "pricingType": "ON_DEMAND" }], "failureReason":"BOOTSTRAP_FAILURE", "failureDetail":"On the master instance (i-07d655c7754c3537b), application provisioning failed", "clusterType":"EMR", "clusterName":"platml-emr-921-tan", "masterNodeIP":"172.25.144.243" }

//{"stackId":"arn:aws:cloudformation:us-east-1:405188027262:stack/mlplatEMRGC/fe245460-bdee-11e8-b5b1-500c217b1cd2","emrId":"j-1ZZ5JW9I4J6OK","instances":[{"role":"MASTER","type":"m4.large","count":1,"pricingType":"ON_DEMAND"}],"failureReason":"BOOTSTRAP_FAILURE","failureDetail":"On the master instance (i-0599e707d6c35e218), application provisioning failed","clusterType":"EMR","clusterName":"mlplatEMRGC","masterNodeIP":"172.25.144.209"}
