import React, {Component} from "react";
import ReactDOM from 'react-dom';
import Services from "../services/Services.js";
import "./Dashboard.css";
import Dropdown from 'react-dropdown';
import ProjectOverview from './projectOverview.jsx';
import ProjectsCard from './../components/ProjectsCard.jsx';
import ProjectCheckout from "./../components/ProjectCheckout.jsx";
import InstancesCard from './../components/InstancesCard.jsx';
import ModelRepoCard from './../components/ModelRepoCard.jsx';
import DataManagementCard from './../components/DataManagementCard.jsx';
import UseCaseCard from './../components/UseCaseCard.jsx';
import AcceleratorCard from './../components/AcceleratorCard.jsx';
import Notifications from './../components/Notifications.jsx';


export default class LandingPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      userinfo: props.userinfo,
    };
  }

  oncClusterCreate = () => {
    this.childInstance.getClusterData();
  }

  render() {

    return (<div>
      <div className="dashboard-container">
        <Notifications/>
        <div class="card card-img-banner dls-accent-white-01 hero" style={{maxHeight: "110%"}}>
          <div class="card-img-bg" style={{backgroundColor:"#EEF7FA"}}>
            <div class="card-img-tint card-img-hero-tint"></div>
          </div>
          <div className="tile-box row pad-responsive-lr pad-responsive-t stack-responsive-a">
          
          <ProjectsCard onClusterCreate={() => { this.onClusterCreate() } } userinfo={this.state.userinfo}/>
          
          <InstancesCard ref={instance => this.childInstance = instance} userinfo={this.state.userinfo} />

          {/* <AcceleratorCard /> */}

          <DataManagementCard userinfo={this.state.userinfo} />

          <UseCaseCard userinfo={this.state.userinfo} />

         {/* <ModelRepoCard /> */}

        </div>

      </div>
    </div>
    
  </div>
    );
  }
}
