import React, { Component } from "react";
import ProjectCheckout from "./ProjectCheckout.jsx";
import StatusPopup from "../containers/statuspopup.jsx"
import Services from "../services/Services.js";
import LoaderPopup from '../containers/loaderpopup.jsx';


export default class ProjectsCard extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedOption: "",
      selectedRow: -1,
      userinfo: props.userinfo,
      cardSubMenu: false,
      cardNotifications: false,
      showLoaderPopup: null,
      tableData: [],
      newProjectName: '',
      newGitUrl: '',
      statusPopupModal: null,
      overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span>" +
        "<span>No Record Found.</span>" +
        "</div>",
      showDataLoading: true,
      overlayLoadingTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span>" +
        "<span>Loading...</span>" +
        "</div>",
      clusterFormModalloading: null
    };
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
    this.getProjectData = this.getProjectData.bind(this);
  }

  cardSubMenu = (event) => {
    event.preventDefault();
    this.setState({
      cardSubMenu: true
    }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  cardNotifications = (event) => {
    event.preventDefault();
    this.setState({
      cardNotifications: true
    }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  closeMenu = () => {
    this.setState({
      cardSubMenu: false,
      instancesOptions: false,
      dmuseOptions: false,
      cardNotifications: false
    }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }

  componentDidMount = () => {
    this.getProjectData();
    var that = this;
    document.addEventListener("click", function (evt) {
      var flyoutElement = document.getElementById('project-card-table'),
        targetElement = evt.target;  // clicked element

      do {
        if (targetElement && flyoutElement && (targetElement.getAttribute("id") == flyoutElement.getAttribute("id"))) {
          return;
        }
        // Go up the DOM
        targetElement = targetElement.parentElement;
      } while (targetElement);
      that.setState({ selectedRow: -1 });
    });
  }

  getProjectData = () => {
    var parent = this;
    var url = "/api/project/" + this.state.userinfo.username;
    Services.get(url)
      .then(function (response) {
        var resData = response.data.projects;
        parent.setState({ tableData: resData || [], showDataLoading: false });
      })
      .catch(function (error) {
        //parent.setState({updateNoDataTemplate: error});
        if (error.status == undefined) {
          parent.updateNoDataTemplate(error);
        }
      })
  }

  updateNoDataTemplate = (info) => {
    let msg = info.message ? info.message : "No Record Found";
    this.setState({
      showDataLoading: false,
      overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'>" +
        "<span class='alert-icon dls-icon-warning-filled'>" +
        "</span>" + "<span>" + msg + "</span>" + "</div>"
    });
  }

  onSubmit = (e) => {
    var modalEle = null;
    if (this.state.selectedRow != 0) {
      //window.location.href ="https://jalahb";
      modalEle = <ProjectCheckout onClose={this.onProjectCheckoutModalClose} userinfo={this.props.userinfo}
        projectName={this.state.selectedOption} showProjectRow={false} />

    }
    else {
      modalEle = <ProjectCheckout onClose={this.onProjectCheckoutModalClose} userinfo={this.props.userinfo}
        projectName={this.state.selectedOption} showProjectRow={true} />
    }
    this.setState({ projectCheckoutModal: modalEle });
    console.log('You have selected:', this.state.selectedOption);
  }




  onSubmitProjectRow = (callback, payload) => {
    let parent = this;
    Services.create("/api/project", payload).then(res => {
      if (res.status == 409) {
        callback(false);
      } else if (res.status == 200) {
        callback(true);
      }

    }).catch(function (err) {
      if (err && err.response.status === 500) {
        callback(false);

      } else if (err && err.response.status === 409) {
        callback(false);
      } else if(err.response.status === 521) {
        callback(false, "Invalid repository URL!!");
      }

    })

  }



  onProjectCheckoutModalClose = (type, stateObj, newprojectState) => {
    var tmpProjectName = null;
    var tmpClusterName = null;
    this.setState({ showLoaderPopup: <LoaderPopup /> });    

    switch (type) {
      case 0: this.setState({ projectCheckoutModal: null });
        break;

        //create cluster with existing project
      case 1:
        this.setState({ projectCheckoutModal: null });
        var that = this;

        this.setState({ clusterFormModalloading: <LoaderPopup/> });
        Services.create("/api/clusters", { ...stateObj })
          .then(res => {
            console.log(res);

            var ele = <StatusPopup onClose={that.onPopupClose} message="Cluster creation initiated!!" />
            that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
            //this.props.onClose();
            // refresh cluster grid.
            that.props.onClusterCreate();
            tmpProjectName = newprojectState;
            tmpClusterName = stateObj['stackName']
            that.redirectToLoadingPage(res["data"]["stackId"]["stackId"], tmpProjectName);
          })
          .catch(function (error) {
            console.error('AJAX error occured :' + error);
            var ele = <StatusPopup onClose={that.onPopupClose} message="Sorry, cluster creation failed.Please try it again" />

            that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
            that.onCloseForm(1);
            //this.props.onClose();
          });
        break;

        //create cluster and project
      case 2:
        this.setState({ projectCheckoutModal: null });
        var that = this;

        this.setState({ clusterFormModalloading: <LoaderPopup/> });
        this.onSubmitProjectRow((status, message = null) => {
          if (status == true) {
            tmpProjectName = newprojectState['projectName'];
            tmpClusterName = stateObj["stackName"]
            Services.create("/api/clusters", { ...stateObj })
              .then(res => {
                console.log(res);
                var ele = <StatusPopup onClose={that.onPopupClose} message="Cluster creation initiated!!" />
                that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
                //this.props.onClose();
                // refresh cluster grid.
                that.props.onClusterCreate();
                that.getProjectData();
                that.redirectToLoadingPage(res["data"]["stackId"]["stackId"], tmpProjectName);
              })
              .catch(function (error) {
                console.error('AJAX error occured :' + error);
                var ele = <StatusPopup onClose={that.onPopupClose} message="Sorry, cluster creation failed.Please try it again" />
                that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
                that.onCloseForm(1);
                //this.props.onClose();
              });
          }
          else {
            if(!message) {
              message = "Sorry project creation failed please try again"
            }
            var ele = <StatusPopup onClose={that.onPopupClose} message={message} />
            that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
            console.log('error in project creation');
          }
        }, newprojectState);
        break;
    //create project with existing cluster
      case 3:
        this.setState({ projectCheckoutModal: null });
        var that = this;

        this.setState({ clusterFormModalloading: <LoaderPopup/> });
        this.onSubmitProjectRow((status, message = null) => {
          if (status == true) {
            tmpProjectName = newprojectState['projectName'];
            tmpClusterName = stateObj;
            that.getProjectData();
            that.redirectToLoadingPage(tmpClusterName, tmpProjectName);
            var ele = <StatusPopup onClose={that.onPopupClose} message="Project Created Successfully" />
            that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
          }
          else {
            if(!message) {
              message = "Sorry project creation failed please try again";
            }
            var ele = <StatusPopup onClose={that.onPopupClose} message={message} />
            that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
            console.log('error in project creation');
          }
          that.setState({ clusterFormModalloading: null });
        }, newprojectState);
        break;
        //create only project
      case 4:
        this.setState({ projectCheckoutModal: null });
        var that = this;

        this.setState({ clusterFormModalloading: <LoaderPopup/> });
        this.onSubmitProjectRow((status,  message = null) => {
          if (status == true) {
            that.getProjectData();
            var ele = <StatusPopup onClose={that.onPopupClose} message="Project Created Successfully" />
            that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
          }
          else {
            if(!message) {
              message = "Sorry project creation failed please try again";
            }
            var ele = <StatusPopup onClose={that.onPopupClose} message={message} />
            that.setState({ clusterFormModalloading: null, statusPopupModal: ele });
            console.log('error in project creation');
          }
          that.setState({ clusterFormModalloading: null });
        }, newprojectState);
        break;
    }
  }

  redirectToLoadingPage = (tmpClusterName, tmpProjectName) => {

    localStorage.setItem("clusterName", tmpClusterName);
    if (tmpClusterName === "GOLD") {
      window.open("/app/mlphub/gold/" +tmpProjectName);
    } else if (tmpClusterName === "GOLDML") {
      window.open("/app/mlphub/goldml/" +tmpProjectName);
    } else if (tmpClusterName === "ECPML") {
      window.open("/app/mlphub/ecp/" +tmpProjectName);
    } else {
      window.open("/app/mlphub/" + tmpClusterName + "/" + tmpProjectName);
      //window.location.href ="https://jalahb";
    }
  }

  onPopupClose = () => {
    this.setState({ statusPopupModal: null });
  }


  onModalClose = () => {
    this.setState({ projectOverview: null });
    this.setState({ projectFormModal: null });
    this.setState({ projectCheckoutModal: null });
    this.getProjectData();
  }

  onSelectionChanged = (item, index) => {
    this.setState({ selectedOption: item.projectName, selectedRow: index });
  }

  
  getModel() {
    this.state.selectedOption.value;
  }


  render() {
    return (
      <div className="col-md-6 pad-tb margin-b zoom">
        <div className="card height-auto fluid flex flex-column card-rounded card-block">
          <div>
            {/* <div>
                    <span className="cluster-excel">
                      <div className="badge badge-icon-default">2</div>
                      <image data-toggle="tooltip-auto" title="Alerts" className="mlp-cursor-pointer icon-md dls-icon-alert"></image>
                    </span>
                    {/* <div>
                      <span className="main-more">
                        <div className="mlp-cursor-pointer icon-md dls-icon-more" data-toggle="tooltip-auto" title="View More" onClick={this.cardSubMenu}></div>
                      </span>
                    </div> 
                  </div> */}
            {/* {
                    this.state.cardSubMenu ? (<div className="menu body-1">
                        <ul>
                          <li>Menu item 2</li>
                          <li>Menu item 2</li>
                          <li>Menu item 3</li>
                        </ul>
                      </div>)
                      : (null)
                  } */}
            <h2 className="heading-4 margin-1-b dls-accent-blue-02">PROJECTS</h2>
            <hr />
          </div>
          <div className="modal-container" style={{ marginBottom: "10px", marginTop: "30px" }}>
            <div style={{ overflowX: "auto", height: "180px", overflowY: "auto" }}>
              <div className="table-fluid margin-1-b">
                <table id="project-card-table" className="table border table-group table-striped pad-lr">
                  <table className="table border table-group pad-lr"></table>
                  <thead>
                    <tr>
                      <th className="body-3">NAME</th>
                      <th className="body-3">GIT URL</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className={"body-1 table-row-link" + (this.state.selectedRow == 0 ? "selected" : "")} style={{ "fontSize": "12px", "background": (this.state.selectedRow == 0 ? "#e4f2ef" : "") }} onClick={() => this.onSelectionChanged({ projectName: this.state.newprojectName }, 0)} >
                      <td>Enter Project Name</td>
                      <td></td>
                    </tr>
                    {
                      this.state.tableData.map((item, index) => {
                        return <tr className={"body-1 table-row-link" + (this.state.selectedRow == index + 1 ? "selected" : "")}
                          onClick={() => this.onSelectionChanged(item, index + 1)}
                          style={{ "fontSize": "12px", "background": (this.state.selectedRow == index + 1 ? "#e4f2ef" : "") }} >
                          <td>{item.projectName}</td>
                          <td>{item.repositoryURL}</td>
                        </tr>
                      })
                    }
                    {
                      (this.state.showDataLoading) && (<div className='progress-circle progress-indeterminate progress-md' style={{
                        "left": "147px",
                        "top": "44px"
                      }}>
                      </div>)
                    }
                    {
                      (!this.state.showDataLoading) && (!this.state.tableData || this.state.tableData.length == 0) &&
                      (<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert' style={{
                        "left": "72px",
                        "top": "9px"
                      }}><span class='alert-icon dls-icon-neutral-filled'></span>
                        <span>No Record Found.</span></div>)
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div>
            {/* <button className="btn-icon btn-sm margin-auto-md" onClick={this.onCreateClick} >Create</button> */}
            { this.props.userinfo.hasAdminRole && <button className="btn-icon btn-sm margin-auto-md" onClick={this.onSubmit}
              disabled={this.state.selectedRow == -1}
              style={{ "marginTop": "6px", marginRight: "25px" }}>
            Launch</button> }
          </div>
        </div>
        <div>
          {this.state.projectFormModal}</div>
        <div>
          {this.state.projectCheckoutModal}
        </div>
        <div>
          {this.state.clusterFormModalloading}
        </div>
        <div>
          {this.state.statusPopupModal}
        </div>
      </div>
    );
  }

}
