import React, { Component } from "react";
import Services from "../services/Services.js";
import UsecaseForm from "../containers/UsecaseForm.jsx";
import StatusPopup from "../containers/statuspopup.jsx";
import ReactDOM from 'react-dom';


export default class UseCaseCard extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      userInfo: this.props.userinfo,
      cardSubMenu: false,
      cardNotifications: false,
      data: "",
      tableData: [],
      useCaseFormModal: null,
      statusPopup: null,
      selectedUsecase: null,
      showDataLoading:true,
      selectedRow: -1
    };
    this.onSelectionChanged = this.onSelectionChanged.bind(this);
    this.getCostCol = this.getCostCol.bind(this);
    this.splunkAccess = this.splunkAccess.bind(this);
    this.getPrcgroupsData = this.getPrcgroupsData.bind(this);
    this.getUsecaseData = this.getUsecaseData.bind(this);
  }

  componentDidMount() {
    this.getUsecaseData();
  }

  getUsecaseData = () => {
    var parent = this;
    var url = "/api/usecases?startRow=1&endRow=5";
    Services.get(url)
      .then(function (response) {
        var resData = response["data"]["usecases"]
        parent.setState({ tableData: resData.slice(0, Math.min(5, resData.length)) || [],showDataLoading:false });
      })
      .catch(function (error) {
        parent.setState({ updateNoDataTemplate: error });
      })
  }

  onCreateClick = () => {
    var defaultUseCase = this.setDefaultSelectedUsecase();
    var modalEle = <UsecaseForm onClose={this.onCloseModal} data={defaultUseCase} isNew="true" />
    this.setState({ useCaseFormModal: modalEle });
  }



  onConfirmDelete = () => {
    parent = this;
    Services.delete("/api/usecases/" + this.state.selectedUsecase.ucId).then(res => {
      parent.setState({ selectedUsecase: null, selectedRow: -1 });
      this.onCloseModal(3);
    })
  }

  onCloseModal = (status) => {
    this.setState({ useCaseFormModal: null });
    this.setState({ overviewPopup: null });
    this.setState({ commentModal: null });
    this.getUsecaseData();
    switch (status) {
      case 1:
        var ele = <StatusPopup onClose={this.onPopupClose} message="UseCase is created!!" />
        this.setState({ statusPopup: ele });
        break;
      case 2:
        ele = <StatusPopup onClose={this.onPopupClose} message="UseCase is edited!!" />
        this.setState({ statusPopup: ele });
        break;
      case 3:
        ele = <StatusPopup onClose={this.onPopupClose} message="UseCase is deleted!!" />
        this.setState({ statusPopup: ele });
        break;
      case 4:
        ele = <StatusPopup onClose={this.onPopupClose} message="Workstream comment successfully added" />
        this.setState({ statusPopup: ele });
        break;
      default:
        this.setState({ statusPopup: null });
        break;
    }
  }

  onPopupClose = (status) => {
    this.setState({ statusPopup: null });
    if (status === 1) {
      this.onConfirmDelete();
    }
  }

  getCostCol = (param) => {
    return (param && param.useCaseMetadata.cmptEnv && param.useCaseMetadata.cmptEnv.length === 1 &&
      param.useCaseMetadata.cmptEnv[0].toString() === "ECP") ? "N/A" : "<a target='_blank'>Splunk Cost</a>";
  }

  useCaseapp = () => {
    window.location.href='/app/user/usecase';
  }

  onDeleteClick = () => {
    var ele = <StatusPopup onClose={this.onPopupClose} message="Are you sure you want to delete?"
      alertBox={true} />
    this.setState({ statusPopup: ele });
  }

  setDefaultSelectedUsecase = () => {
    return {
      useCaseName: "",
      useCaseDesc: "",
      useCaseType: "application",
      applicationId: "",
      onBoardStatus: "REQUEST",
      ucOwnerEmailId: "",
      requesterEmailId: "",
      cmptEnv: [],
      prcGroups: [],
      comments: []
    };
  }

  onSelectionChanged = (item, index) => {
    this.setState({ selectedUsecase: item, selectedRow: index });
  }


  cardSubMenu = (event) => {
    event.preventDefault();
    this.setState({
      cardSubMenu: true
    }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  cardNotifications = (event) => {
    event.preventDefault();
    this.setState({
      cardNotifications: true
    }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  getUserrole = (email, callback) => {
    var endpoint = "/api/users/info/?email=" + email;
    Services.get(endpoint, null).then(res => {
      callback(true, res["data"]["username"]);
    })
      .catch(err => {
        callback(false, "");
      })
  }

  getSplunkAccess = () => {
    if (this.state.userInfo["emailId"]) {
      Services.get("api/usecases/prcgroups_users/IMDC-SPLK-500000723-CDAT-READONLY-E1").then(res => {
      });
    }
  }

  splunkAccess = (params) => {
    var splunkAccess = false;
    if (!(params.useCaseMetadata.cmptEnv.length === 1 && params.useCaseMetadata.cmptEnv[0] === "ECP")) {
      this.getPrcgroupsData((res) => {
        var prefixurl = "https://splunk-isl001-dev.aexp.com/en-US/app/" +
          "splunk_app_aws/monthly_billing?form.billingAccountId=*&form.accountId=*&form.region=*&form.currency=USD&form.tags=Role%3D"
        for (var i = 0; i < res.length; i++) {
          if (res[i].emailId.toLowerCase() == this.state.userInfo.emailId.toLowerCase()) {
            splunkAccess = true;
            this.getUserrole(this.state.userInfo.emailId, (valid, adsId) => {
              if (valid) {
                window.open(prefixurl + (params.useCaseMetadata.awsIamRole == ' ' ? 'undefined' : params.useCaseMetadata.awsIamRole), "_blank");
                return true;
              }
              else {
                var ele = <StatusPopup onClose={this.onPopupClose}
                  message="This is an Invalid Owner's Email or not found in splunk PRC group" />
                this.setState({ statusPopup: ele });
              }
            });
            break;
          }
        }

        if (!splunkAccess) {
          var ele = <StatusPopup onClose={this.onPopupClose} message="You do not have access to splunk!!" />
          this.setState({ statusPopup: ele });
        }
      })
    }
    return false;
  }

  getPrcgroupsData = (callback) => {
    var endpoint = "/api/usecases/prcgroups_users/IMDC-SPLK-500000723-CDAT-READONLY-E1";
    Services.get(endpoint, null).then(res => {
      callback(res["data"])
    })
      .catch(err => {
      })
  }


  closeMenu = () => {
    this.setState({
      cardSubMenu: false,
      instancesOptions: false,
      dmuseOptions: false,
      cardNotifications: false
    }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }

  onModalClose = () => {
    this.setState({ projectOverview: null });
  }

  render() {

    return (
      <div className="col-md-6 pad-tb margin-b zoom">
        <div className="card height-auto fluid flex flex-column card-rounded card-block">
          <div>
            {/* <div>
              <span className="cluster-excel">
                <div className="badge badge-icon-default">2</div>
                <image data-toggle="tooltip-auto" title="Alerts" className="mlp-cursor-pointer icon-md dls-icon-alert"></image>
              </span>
              <div>
                <span className="main-more">
                  <div className="mlp-cursor-pointer icon-md dls-icon-more" data-toggle="tooltip-auto" title="View More" onClick={this.cardSubMenu}></div>
                </span>
              </div>
            </div>
            {
              this.state.cardSubMenu
                ? (<div className="menu body-1">
                  <ul>
                    <li>
                      Menu item 2
                          </li>
                    <li>
                      Menu item 2
                          </li>
                    <li>
                      Menu item 3
                          </li>
                  </ul>
                </div>)
                : (null)
            } */}
            <h2 className="heading-4 margin-1-b dls-accent-blue-02" onClick={this.useCaseapp}>USE CASES</h2>
            <hr />
            {/* <p className="body-1 margin-3-tb">This is body copy. We recommend keeping this copy to a four line maximum.</p> */}
          </div>

          <div className="modal-container">
            <div style={{ overflowX: "auto", height: "180px", marginBottom: "10px", marginTop: "30px", overflowY: "auto" }}>
              <div className="table-fluid margin-1-b">
                <table className="table border table-group table-striped pad-lr">
                  <thead>
                    <tr>
                      <th className="body-3">Name</th>
                      <th className="body-3">Environment</th>
                      <th className="body-3">Cost</th>
                    </tr>
                  </thead>
                  <tbody>{
                    this.state.tableData.map((item, index) => {
                      return <tr className={"body-1 table-row-link" + (this.state.selectedRow == index ? "selected" : "") } onClick={() => this.onSelectionChanged(item, index)} style={{ "fontSize": "12px",  "background": (this.state.selectedRow == index ? "#e4f2ef" : "") }} >
                        <td>{item.useCaseName}</td>
                        <td>{item.useCaseMetadata.cmptEnv && item.useCaseMetadata.cmptEnv.length > 0 ? item.useCaseMetadata.cmptEnv.join(",") : ""}</td>
                        <td>{(item && item.useCaseMetadata.cmptEnv && item.useCaseMetadata.cmptEnv.length === 1 &&
                          item.useCaseMetadata.cmptEnv[0].toString() === "ECP") ? "N/A" : <a target='_blank' onClick={() => this.splunkAccess(item)}>Splunk Cost</a>}</td>
                      </tr>
                    })
                  }
                   {
                            (this.state.showDataLoading) && (<div className='progress-circle progress-indeterminate progress-md' style={{
                              "left": "147px",
                              "top": "44px"
                            }}>
                            </div>)
                          }
                         {
                      (!this.state.showDataLoading &&  this.state.tableData.length == 0) &&
                      (<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert' style={{
                        "left": "72px",
                        "top": "9px"

                      }}><span class='alert-icon dls-icon-neutral-filled'></span>
                        <span>No Record Found.</span></div>)
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {/* <div style={{ marginTop: "-43px"}}>
            <button className="btn-icon btn-sm margin-auto-md" onClick={this.onCreateClick.bind(this)}>Create</button>
            <button className="btn-icon btn-sm margin-auto-md" onClick={this.onDeleteClick.bind(this)} disabled={this.state.selectedUsecase == null}>Delete</button>
          </div> */}
        </div>
        <div>
          {this.state.useCaseFormModal}
        </div>
        <div>
          {this.state.statusPopup}
        </div>
      </div>
    );
  }

}
