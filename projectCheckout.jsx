import React from 'react';
import Services from "../services/Services.js";
import "./ProjectForm.css";

class ProjectCheckout extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            newGitUrl: "",
            newProjectName: "",
            projectName: props.projectName,
            selectedOption: "",
            tableData: [],
            userinfo: props.userinfo,
            masterInstancetype: "1x1",
            slaveInstanceType: "1x1",
            flavor: "dg1.32x250",
            volumeSize: "",
            slaveCount: "",
            slavePrice: "",
            emrOptions: [],
            ec2Options: [],
            clusterName: "",
            selectedOptionType: "",
            clusterNameError: false,
            clusterNameErrorMsg: "",
            slaveCountError: false,
            slaveCountErrorMsg: "",
            projectNameError: false,
            projectNameErrorMsg: "",
            gitUrlError: false,
            gitUrlErrorMsg: "",
            checkoutBtnStatus: false,
            controlMarketType: "ON_DEMAND",
            computeMarketType: "ON_DEMAND",
            vmMarketType: "ON_DEMAND",
            loading: '',
            envLoading: '',
            env: "",
            awsRoleList: "",
            envStatedevOrQa: false
        };
        this.getInstanceOptions = this.getInstanceOptions.bind(this);
        

    }

    validate = () => {
        var flag = null;
        if (this.state.selectedOption == "" || (this.state.selectedOption == 'AWS' && this.state.clusterName == "")
            || (this.state.selectedOption == 'AWS' && this.state.selectedOptionType == "") || (this.state.selectedOption == 'No AWS environment listed')
            || (this.state.selectedOption == 'AWS' && this.state.selectedOptionType == 'EMR' && this.state.slaveCount == "")
            || (this.state.selectedOption == 'AWS' && this.state.selectedOptionType == 'EMR' && this.state.clusterName == "")
            || (this.state.selectedOption == 'AWS' && this.state.selectedOptionType == 'EC2' && this.state.clusterName == "")
            || this.state.clusterNameError || this.state.slaveCountError || (this.props.showProjectRow && this.state.newProjectName == "") || (this.props.showProjectRow && this.state.newGitUrl == "")) {
            flag = false;
        }
        else {
            flag = true;
        }

        if ((!this.state.projectNameError && !this.state.gitUrlError) && this.props.showProjectRow && this.state.newProjectName != "" && this.state.newGitUrl != "" && !this.state.projectNameError && !this.state.gitUrlError && this.state.selectedOption == "") {
            flag = true;
        }

        if (this.state.gitUrlError) {
            flag = false;
        }

        this.setState({ checkoutBtnStatus: flag });
    }

    componentWillUnmount() {
        document.body.style.overflow = "auto";
    }

    componentDidMount = () => {
        document.body.style.overflow = "hidden";
        var parent = this;
        var url = "/api/clusters/user/" + this.state.userinfo.emailId;
        var svcReq = {
            startRow: 1,
            endRow: 20,
            filterKey: 'clusterStatus',
            filterType: 'notEqual',
            filterValue: 'TERMINATED'
        }
        parent.setState({ loading: true })
        Services.get(url, svcReq)
            .then(function (response) {
                var resData = response.data.clusters;
                parent.setState({ tableData: resData.slice(0, Math.min(resData.length)) || [], loading: false });
            })
            .catch(function (error) {
                parent.setState({ updateNoDataTemplate: error });
            }),
            this.getEnvAndRoles();
        if(window.location.href.indexOf("dev") != -1 || window.location.href.indexOf("qa") != -1) {
            this.setState({envStatedevOrQa: true});
        }
    }


    executeData = () => {
        let payload = this.getPostPayload(event);
        this.setState({ outputresponse: 'Running' });
        let parent = this;
        Services.create("/api/apps/execute", payload).then(res => {
            if (res) {
                this.setState({ outputresponse: JSON.stringify(res.data.message, undefined, 2) });
            }
        }).catch((err) => {
            this.setState({ outputresponse: err });
        })
    }

    handleClickOutside = (event) => {
        if (this.modalRef && !this.modalRef.contains(event.target)) {
            this.handleClose();
        }
    }

    getInstanceOptions = () => {
        Services.get("/api/clusters/instances?stacktype=emr&provider=aws").then(res => {
            var instanceOptions = res.data.instances;
            var options = [];
            for (var i = 0; i < instanceOptions.length; i++) {
                options.push({ value: instanceOptions[i].instDisplayName, key: instanceOptions[i].instConfig });
            }
            this.setState({ emrOptions: options });
        })
        Services.get("/api/clusters/instances?stacktype=ec2&provider=aws").then(res => {
            var instanceOptions = res.data.instances;
            var options = [];
            for (var i = 0; i < instanceOptions.length; i++) {
                options.push({ value: instanceOptions[i].instDisplayName, key: instanceOptions[i].instConfig });
            }
            this.setState({ ec2Options: options });
        }).catch(function (error) {
            console.error('AJAX error occured :' + error);
        })
    }

    getEnvAndRoles = () => {
        this.setState({ envLoading: true })
        Services.get("/api/users/ldap/user/" + this.state.userinfo.emailId).then(res => {
            this.setState({ env: res.data.envs, awsRoleList: res.data.awsRoleList, envLoading: false });
        })
    }

    getModel() {
        this.state.selectedOption.value;
    }

    handleClose = () => {
        this.props.onClose(0, null);
    }

    setModalRef = (node) => {
        this.modalRef = node;
    }


    onChangeValue = (event) => {
        this.setState({ newProjectName: event.target.value }, () => {
            this.validation(2);
        });

    }

    onChange = (event) => {
        this.setState({ newGitUrl: event.target.value }, () => {
            this.validation(3);
        });
    }


    validation = (type) => {
        switch (type) {
            case 0:
                if (this.state.clusterName && this.state.clusterName.length > 0 && this.state.clusterName == '' || this.state.clusterName.match(/^[a-zA-Z0-9\-]*$/) == null) {
                    this.setState({ clusterNameError: true, clusterNameErrorMsg: "Special characters are not allowed" }, () => { this.validate() });
                }
                else {
                    this.setState({ clusterNameError: false }, () => { this.validate() });
                }
                break;
            case 1:
                if (this.state.slaveCount && this.state.slaveCount < 3 || this.state.slaveCount > 100) {
                    if (this.state.slaveCount < 3)
                        this.setState({ slaveCountError: true, slaveCountErrorMsg: "Minimum count should be 3" }, () => { this.validate() });
                    else
                        this.setState({ slaveCountError: true, slaveCountErrorMsg: "Maximum count is 100" }, () => { this.validate() });
                }
                else {
                    this.setState({ slaveCountError: false }, () => { this.validate() });
                }
                break;
            case 2:
                if (this.state.newProjectName && this.state.newProjectName.length > 0 && this.state.newProjectName == '' || this.state.newProjectName.match(/^[a-zA-Z0-9\-&]*$/) == null) {
                    this.setState({ projectNameError: true, projectNameErrorMsg: "special characters are not allowed" }, () => { this.validate() });
                }
                else {
                    this.setState({ projectNameError: false }, () => { this.validate() });
                }
                break;
            case 3:
                if ((this.state.newGitUrl && this.state.newGitUrl.length > 0 && this.state.newGitUrl.startsWith("https")) || this.state.newGitUrl == "") {
                    this.setState({ gitUrlError: false }, () => { this.validate() });
                }
                else {
                    this.setState({ gitUrlError: true, gitUrlErrorMsg: "Invalid git url" }, () => { this.validate() });
                }
                break;
        }
    }


    getPostPayload = () => {
        let reqPayload = {
            projectName: this.state.newProjectName,
            repositoryURL: this.state.newGitUrl,
            userId: this.state.userinfo.username
        };
        return reqPayload;
    }


    onSubmitCallback = () => {
        if (this.state.selectedOptionType == "EMR" || this.state.selectedOptionType == "EC2") {
            // create cluster
            var stateObj = {};
            stateObj["createdBy"] = this.state.userinfo.adsId.replace("@ads.aexp.com", "");
            stateObj["createdByEmail"] = this.state.userinfo.emailId;
            if (this.state.selectedOptionType == "EMR") {
                stateObj["parameters"] = { controlNum: 1, computeFlavor: this.state.masterInstancetype, controlFlavor: this.state.slaveInstanceType, computeNum: this.state.slaveCount, controlMarketType: this.state.controlMarketType, computeMarketType: this.state.computeMarketType }
            }
            else {
                stateObj["parameters"] = { flavor: this.state.flavor, vmMarketType: this.state.vmMarketType }
            }
            stateObj["provider"] = "aws";
            stateObj["stackName"] = this.state.clusterName;
            stateObj["stackType"] = this.state.selectedOptionType.toLowerCase();
            if (this.props.showProjectRow) {
                this.props.onClose(2, stateObj, this.getPostPayload());
            }
            else {
                this.props.onClose(1, stateObj, this.state.projectName);
            }
        }
        else {
            if (this.props.showProjectRow) {
                if (this.state.selectedOption == "") {
                    this.props.onClose(4, this.state.selectedOption, this.getPostPayload());
                }
                else {
                    this.props.onClose(3, this.state.selectedOption, this.getPostPayload());
                }
            }
            else {

                localStorage.setItem("projectName", this.state.projectName);
                if (this.state.selectedOption === "GOLD") {
                    localStorage.setItem("clusterId", 'gold');
                    // window.open("/app/mlphub/gold/" + this.state.projectName);
                    window.open("https://lppbd3018.gso.aexp.com:9000");
                } else if (this.state.selectedOption === "GOLDML") {
                    localStorage.setItem("clusterId", 'goldml');
                    window.open("/app/mlphub/goldml/" + this.state.projectName);
                } else if (this.state.selectedOption === "ECPML") {
                    localStorage.setItem("clusterId", 'ecpml');
                    window.open("/app/mlphub/ecp/" + this.state.projectName);
                } else if (this.state.selectedOption === this.state.selectedOption) {
                    localStorage.setItem("clusterId", this.state.selectedOption);
                    window.open("/app/mlphub/" + this.state.selectedOption + "/" + this.state.projectName);
                }
                this.handleClose();
                console.log('You have selected:', this.state.selectedOption);
            }
        }
    }

    errorMsg = () => {
        if (this.state.selectedOption == 'AWS' && this.state.awsRoleList) {
            return <div>
                <label>Roles</label>
                <div class="select form-control" data-toggle="select">
                    <select id="selectDropdown"
                        className="form-input"
                        value={this.state.selectedRole}
                        onChange={(e) => {
                            this.setState({ selectedRole: e.target.value })
                        }}>
                        <option value="">SELECT ROLES</option>
                        {this.state.awsRoleList && this.state.awsRoleList.map((item) => { return <option value={item}>{item}</option> })}
                    </select>
                </div>
            </div>
        }
        else if (this.state.selectedOption == 'AWS' && !this.state.awsRoleList) {
            return <div>
                <label>Roles</label>
                <p style={{ color: "red" }}>No roles are currently assigned yet.Please refer to <a href="https://central752.intra.aexp.com/iam/newUIPOC.jsf" target="_blank">this link</a> and raise the required access in IIQ.</p>
            </div>
        }
    }

    onSelectionChanged = (e) => {
        this.setState({ selectedOption: e.target.clusterId });
    }

    handleOptionChange = (e) => {
        this.setState({ selectedOption: e.target.value });
    }

    render() {
        var data = [{ value: "GOLD" },
        { value: "GOLDML" },
        { value: "ECPML" }]
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen" style={{ zIndex: 10 }}></div>
                <div className="hidden-sm-down position-relative" style={{ "overflowY": "auto" }}>
                    <div className="modal" style={{ zIndex: 10, top: "5%" }}>
                        <div className="projectcheckout-container" style={{ "overflowY": "auto" }} ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">Launch Project</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <span onClick={this.handleClose}
                                        data-toggle="tooltip-auto" title="Close"
                                        className="mlp-cursor-pointer dls-glyph-close"></span>
                                </div>
                            </div>

                            <div className="usecase-form background-white" style={{ "paddingTop": "10px", "height": "auto", "maxHeight": "470px", paddingLeft: "10px", overflowY: "auto" }} >

                                {this.props.showProjectRow && <div className="row">
                                    <div className="col-md-6">
                                        <label htmlFor="egselect">PROJECT NAME</label>
                                        <textarea value={this.state.newProjectName} onChange={this.onChangeValue}
                                            type="text" placeholder="Enter the Name of the Project"
                                            rows="1" className="form-control" style={{ "background": "transparent" }} >
                                        </textarea>
                                        {this.state.projectNameError && (<p class="error-message" style={{ "color": "red" }}>{this.state.projectNameErrorMsg}</p>)}
                                    </div>
                                    <div className="col-md-6">
                                        <label htmlFor="egselect">GIT REPO URL</label>
                                        <textarea value={this.state.newGitUrl} onChange={this.onChange}
                                            type="text" placeholder="Enter the GIT REPO URL"
                                            rows="1" className="form-control" style={{ "background": "transparent" }} >
                                        </textarea>
                                        {this.state.gitUrlError && (<p class="error-message" style={{ "color": "red" }}>{this.state.gitUrlErrorMsg}</p>)}
                                    </div>

                                    {this.state.showInvalidMsg && <div style={{ paddingLeft: "276px", color: "#AD3117" }}> {this.state.invalidUrlMsg}</div>}
                                </div>
                                }


                                <div className="row">
                                    <div className="col-md-6 pad-4-lr">
                                        <label>Environment</label>
                                        <div>
                                            <div class="select form-control" data-toggle="select">
                                                <select id="selectDropdown"
                                                    className="form-input"
                                                    value={this.state.selectedOption}
                                                    onChange={(e) => {
                                                        this.setState({ selectedOption: e.target.value, clusterName: "" },
                                                            () => {
                                                                if (this.state.selectedOption != 'AWS') {
                                                                    this.setState({ selectedOptionType: '' });
                                                                };
                                                                this.validate();
                                                            });
                                                        if (e.target.value == 'emr') {
                                                            this.getInstanceOptions();
                                                        }
                                                    }
                                                    }>
                                                    <option value="">SELECT ENVIRONMENT</option>
                                                    {this.state.tableData.map((item, index) => {
                                                        if (item.clusterStatus == 'Idle' || item.clusterStatus == 'Ready' || item.clusterStatus == 'Running') {
                                                            return <option value={item.clusterId}>{item.clusterName}</option>
                                                        }
                                                    })}
                                                    {this.state.loading && <option value="loading" disabled>Loading dynamic clusters...</option>}

                                                    {this.state.env && this.state.env.map((item) => {
                                                        if (item.includes('AWS')) return <option value={item}>{item}</option>
                                                        else <option value={item}> No AWS environment listed</option>
                                                    })}
                                                    {this.state.envLoading && this.state.envStatedevOrQa && <option value="loading" disabled>Loading environments</option>}
                                                { this.state.envStatedevOrQa && <option value="GOLD">GOLD</option> }

                                                    {/* <option value="AWS">New AWS Environment</option> */}
                                                    {/* 
                                                    <option value="GOLDML">GOLDML</option>
                                                    <option value="ECPML">ECPML</option>  */}

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 pad-4-lr">
                                        {this.errorMsg()}
                                    </div>
                                </div>

                                {((this.state.selectedOption == 'AWS' || this.state.selectedOptionType == 'EMR' || this.state.selectedOptionType == "EC2") && (this.state.selectedRole)) && (
                                    <div className="row" style={{ "marginTop": "-10px" }}>
                                        <div className="col-md-6 pad-4-lr">
                                            <label>Name</label>
                                            <input
                                                placeholder="Cluster Name"
                                                className="pl-20"
                                                name="clusterName"
                                                value={this.state.clusterName}
                                                onChange={(e) => { this.setState({ clusterName: e.target.value }, () => { this.validation(0); }); }}
                                            />
                                            {this.state.clusterNameError && (<p class="error-message" style={{ "color": "red" }}>{this.state.clusterNameErrorMsg}</p>)}
                                        </div>
                                        <div className="col-md-6 pad-4-lr">
                                            <label>Type</label>
                                            <div>
                                                <div class="select form-control" data-toggle="select">
                                                    <select id="selectDropdown" className="form-input" value={this.state.selectedOptionType} onChange={(e) => { this.setState({ selectedOptionType: e.target.value }, () => { this.validate(); }); this.getInstanceOptions(); }}>
                                                        <option value="">Select Type</option>
                                                        <option value="EMR">EMR</option>
                                                        <option value="EC2">EC2</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}

                                {this.state.selectedOptionType == 'EMR' && (
                                    <div className="row" style={{ "marginTop": "-10px" }}>
                                        <div class="col-md-6">
                                            <label>Master Type</label>
                                            <div class="select form-control" data-toggle="select">
                                                <select id="selectDropdown"
                                                    className="form-input"
                                                    name="master_node_instance"
                                                    value={this.state.masterInstancetype}
                                                    onChange={(e) => { this.setState({ masterInstancetype: e.target.value }, () => { this.validate(); }); }}
                                                >
                                                    {this.state.emrOptions.map((item, index1) => <option value={item.key}>{item.key} ({item.value})</option>)}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Slave Type</label>
                                            <div class="select form-control" data-toggle="select">
                                                <select id="selectDropdown"
                                                    className="form-input"
                                                    name="slave_node_instance"
                                                    value={this.state.slaveInstanceType}
                                                    onChange={(e) => { this.setState({ slaveInstanceType: e.target.value }, () => { this.validate(); }); }}
                                                >
                                                    {this.state.emrOptions.map((item, index1) => <option value={item.key}>{item.key} ({item.value})</option>)}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style={{ "marginTop": "10px" }}>
                                            <label>Slave Count</label>
                                            <input
                                                type="number"
                                                placeholder="count"
                                                className="pl-20"
                                                name="slave_node_count"
                                                value={this.state.slaveCount}
                                                onChange={(e) => { this.setState({ slaveCount: e.target.value }, () => { this.validation(1); }); }}
                                            />
                                            {this.state.slaveCountError && (<p class="error-message" style={{ "color": "red" }}>{this.state.slaveCountErrorMsg}</p>)}
                                        </div>
                                        <div class="col-md-6" style={{ "marginTop": "10px" }}>
                                            <label>EBS size(GB)</label>
                                            <input
                                                placeholder="32"
                                                className="pl-20"
                                                name="ebsSize"
                                                value={this.state.volumeSize}
                                                onChange={(e) => { this.setState({ volumeSize: e.target.value }, () => { this.validate(); }); }}
                                                disabled />
                                        </div>
                                        <div class="col-md-6">
                                            <div className="checkbox">
                                                <input id="demo-checkbox-1" onChange={(e) => { this.setState({ computeMarketType: e.target.value == "1" ? "SPOT" : "ON_DEMAND" }) }} class="form-control" type="checkbox" name="spotInstance" value="1" />
                                                <label for="demo-checkbox-1">Spot Instance</label>
                                            </div>
                                        </div>
                                    </div>
                                )}

                                {this.state.selectedOptionType == 'EC2' && (
                                    <div className="row" style={{ "marginTop": "-10px" }}>
                                        <div class="col-md-6">
                                            <label>Machine Type</label>
                                            <div class="select form-control" data-toggle="select">
                                                <select id="selectDropdown"
                                                    className="form-input"
                                                    name="slave_node_instance"
                                                    value={this.state.flavor}
                                                    onChange={(e) => { this.setState({ flavor: e.target.value }, () => { this.validate(); }); }}
                                                >
                                                    {this.state.ec2Options.map((item, index1) => <option value={item.key}>{item.key} ({item.value})</option>)}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>EBS Size(GB)</label>
                                            <input
                                                placeholder="32"
                                                className="pl-20"
                                                name="slave_node_count"
                                                value={this.state.volumeSize}
                                                onChange={(e) => { this.setState({ volumeSize: e.target.value }, () => { this.validate(); }); }}
                                                disabled />
                                        </div>
                                        <div class="col-md-6" style={{ "marginTop": "10px" }}>
                                            <label>Machine Count</label>
                                            <input
                                                type="number"
                                                placeholder="1"
                                                className="pl-20"
                                                name="slave_node_count"
                                                value={this.state.machineCount}
                                                onChange={(e) => { this.setState({ slaveCount: e.target.value }); }}
                                                disabled
                                            />
                                            {this.state.slaveCountError && (<p class="error-message" style={{ "color": "red" }}>{this.state.slaveCountErrorMsg}</p>)}
                                        </div>
                                        <div class="col-md-6" style={{ "marginTop": "25px" }}>
                                            <div className="checkbox">
                                                <input id="demo-checkbox-1" onChange={(e) => { this.setState({ vmMarketType: e.target.value == "1" ? "SPOT" : "ON_DEMAND" }) }} class="form-control" type="checkbox" name="spotInstance" value="1" />
                                                <label for="demo-checkbox-1">Spot Instance</label>
                                            </div>
                                        </div>
                                    </div>
                                )}
                                <div className="row" style={{ "marginTop": "5px" }}>
                                    <button disabled={!this.state.checkoutBtnStatus} className="btn-md" style={{ marginLeft: "36%" }} onClick={this.onSubmitCallback}>Checkout</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default ProjectCheckout;
