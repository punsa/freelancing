'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const logger = require('winston')
const mlp = require('./mlpService');
const crypto = require('./cryptoService');
const appConfig = require('./../config/app-config');
const env = process.env.EPAAS_ENV || 'e0';
var moment = require('moment');

function setFilters(filterType, filterValue) {

    logger.debug("filtertype: :- " + filterType);
    logger.debug("filtervalue: :- " + filterValue);
    if (filterType == null || filterValue == null)
        return [filterType, filterValue];
    var pattern = null;
    var a = filterValue;
    switch (filterType) {
        case "equals": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "notEqual": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "startsWith": pattern = new RegExp("^" + a + ".*$", "i");
            break;
        case "endsWith": pattern = new RegExp("^.*" + a + "$", "i");
            break;
        case "contains": pattern = new RegExp("^.*" + a + ".*$", "i");
            break;
        default: logger.debug("Not Found Filter Type : " + filterType);
    }
    if (pattern == null)
        return [filterType, filterValue];


    logger.debug("filterVal :- " + filterValue + " does not match any key in dmTransferStatusMapper");
    return [filterType, filterValue];
}

function createProjectDetails(token, username, scvReq, callback) {
    let endPoint = "/api/v1/project";
    mlp.executeRequest(endPoint, 'POST', token, scvReq, callback);
}

function getProjectDetails(token, username, svcReq, callback) {
    let endpoint = "/api/v1/project/" + username;
    if (svcReq) {
        endpoint = endpoint + "?"
    }
    for (var key in svcReq) {
        if (svcReq[key]) {
            endpoint = endpoint + key + "=" + svcReq[key] + "&"
        }
    }
    mlp.executeRequest(endpoint, 'GET', token, {}, callback)

}

router.use(function validate(req, res, next) {
    logger.info('Request : ' + req.url);
    next()
})


router.get('/project/:username', function (req, res) {
    logger.info("IN project" + '/project/username');
    try {
        var token = req.userContext.tokens['access_token']

        var username = req.userContext.userinfo['preferred_username']


        if (!validateFilterInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }

        var filterType = null;

        if (req.query.filterType && req.query.filterType === 'lessThanOrEqual') {
            filterType = "lessThanEqual"
        } else if (req.query.filterType && req.query.filterType === 'greaterThanOrEqual') {
            filterType = "greaterThanEqual"
        } else {
            filterType = req.query.filterType;
        }

        var obj = setFilters(filterType, req.query.filterValue);

        var startTimestamp = null;
        var endTimestamp = null;

        let svcReq = {
            startRow: req.query.startRow, endRow: req.query.endRow,
            filterKey: req.query.filterKey, filterType: obj[0], filterValue: obj[1],
            sortBy: req.query.sortBy, sortType: req.query.sortType,
            startTimestamp: startTimestamp, endTimestamp: endTimestamp
        }

        getProjectDetails(token, username, svcReq, function (err, results) {
            if (!err) {
                results = JSON.parse(results);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});

router.post('/project', function (req, res) {
    try {
        var token = req.userContext.tokens['access_token'];
        var repo = req.body.repositoryURL;
        var endPoint = "/api/v1/project";
        logger.info("in project" + req.params.username);
       if(req.body.projectName && req.body.projectName.length > 0 && req.body.projectName && req.body.projectName.match(/[a-zA-Z0-9/-]*/)[0] == req.body.projectName) {
       if(req.body.repositoryURL && req.body.repositoryURL.length > 0 &&  req.body.repositoryURL && req.body.repositoryURL.startsWith("https")) {
     
        
        createProjectDetails(token, endPoint, req.body, function (err, results) {
          if (!err) {
            logger.debug(results);
            var status = '200';
            if (results && results.statusCode == 409) {
                status = results.statusCode;
            }
            res.status(status).send('{"status": "created"}');
        } else {
                var body = JSON.parse(results.body);
                if(body.code == 521) {
                    res.status('521').send('{"status" : "failed", "statusCode" :'+ body.message +' }')
                }
                else {
                    res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
                }
                
            }
        })
    }
    else{
        console.log("****************", repo);
        res.status('500').send('{"status" : "failed", "statusCode" :"Invalid repo url" }');
    }
}
else{
    res.status('500').send('{"status" : "failed", "statusCode" :"Invalid project Name"  }');
}
}
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/update', function (req, res) {
    try {
        var token = req.userContext.tokens['access_token'];

        var data = JSON.parse(req.query.data);
        createProjectDetails(req.query.username, token, data, function (err, results) {
            if (!err) {
                res.status('201').send('{"status":  "updated"}');
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        });
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})




function validateFilterInput(req) {
    var data = req.query;
    var filterValueRegex = /^[a-zA-Z0-9\-\_~\.@:/]+$/
    if ((!data.filterKey || data.filterKey && data.filterKey.length <= 25 && validator.isAlpha(data.filterKey)) &&
        (!data.filterValue || data.filterValue && data.filterValue.length <= 50 && filterValueRegex.test(data.filterValue)) &&
        (!data.filterType || data.filterType && data.filterType.length <= 25 && validator.isAlpha(data.filterType)) &&
        (!data.startRow || data.startRow && validator.isNumeric(data.startRow) && data.startRow <= 100000) &&
        (!data.endRow || data.endRow && validator.isNumeric(data.endRow) && data.endRow <= 100000) &&
        (!data.sortBy || data.sortBy && data.sortBy.length <= 25 && validator.isAlpha(data.sortBy)) &&
        (!data.sortType || data.sortType && data.sortType.length <= 5 && validator.isAlpha(data.sortType))) {
        return true;
    } else {
        logger.info("Validation Failed");
        return false;
    }
}


module.exports = router
