import React, { Component } from "react";
import ReactDOM from "react-dom";
import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
//import DatePicker from "react-datepicker";
import moment from 'moment';
import "./datefilter.css";

export default class DateFilter extends Component {
constructor(props, context) {
    super(props, context);
   this.state = {
    type: null,
    value: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
    //start_date: moment().subtract(1, "days"),
    date: moment(),
    //dateVal: ''

   }
}

isFilterActive() {
    return (this.state.type !== null && this.state.type !== undefined && this.state.type !== '') ||
      (this.state.value !== null && this.state.value !== undefined && this.state.value !== '');
  }

getModel() {
    let type = this.state.type;
    return { filterType: 'text', type: this.state.type, filterkey: this.props.filterKey, filter: this.state.value };
  }


applyFilter = () => {
    this.props.filterChangedCallback();
  }
onDateChange = (date) => {
  //console.log("date value" +dateVal);
  //this.setState({value: dateVal.format("YYYY-MM-DD hh:mm:ss")})
   this.setState({ date: date, value: moment(date).format('YYYY-MM-DD HH:mm:ss') });
        //this.setState({ value: date.format('YYYY-MM-DD') });

}
resetFilter = () => {
    this.props.resetDateFilter();
  }

  onChangeType = (e) => {
    this.setState({ type: e.target.value });
  }
   onChangeValue = (e) => {
    this.setState({ value: e.target.value });
  }
  onChangeDate = (e) => {
    this.setState({ date : e.target.value})
  }
 render() {


 let style = {
      fontSize: "10px",
      width: "260px",
      height: "400px",
      overflowX: "hidden",
      overflowY:"hidden",
    borderRadius: ".25rem",
    lineHeight: "1.45667",
    minHeight: "3.125rem",
    padding: "0 .625rem",
    marginTop: "5px"
    /* margin-right: 9px; */

    };

 return (
<div style={style}>
             <div className="radio-filter">
              Choose Type of Filter:
              </div>
             <fieldset>
               <div className="row">
                <div className="radio" style={{ marginLeft: '16px' }}>
                  <input id="demo-1" className="form-control" type="radio" aria-invalid="false"
                    name="size" checked={this.state.type == "greaterThan"} value="greaterThan" onChange={this.onChangeType} />
                  <label htmlFor="demo-1" style={{ fontSize: '11px' }}>GreaterThan</label>
                </div>

                <div className="radio" style={{ marginLeft: '57px' }}>
                  <input id="demo-2" className="form-control" type="radio" aria-invalid="false"
                    name="size" checked={this.state.type == "lessThan"} value="lessThan" onChange={this.onChangeType} />
                  <label htmlFor="demo-2" style={{ fontSize: '11px' }}>LessThan</label>
                </div>
                </div>
              </fieldset>

  Choose Date:
  <Datetime input={true}
            dateFormat="YYYY-MM-DD"
            timeFormat="HH:mm:ss"
            selected={this.state.date}
            maxDate={moment()}
           inputProps={{ placeholder: 'YYYY-MM-DD HH:MM:SS', disabled: false }}
            onChange={this.onDateChange} />
             <div className="row" style={{ marginTop: '46px' }}>
              <button className="btn-icon btn-sm" style={{ marginLeft: '16px' }} onClick={this.applyFilter}>Apply</button>
              <button className="btn-icon btn-sm" style={{ marginLeft: '57px' }} onClick={this.resetFilter}>Reset </button>
            </div>

  </div>

 );
 }
};
