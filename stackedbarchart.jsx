import React, { Component } from "react";
import * as d3 from "d3";



export default class StackedBarChart extends Component {
    constructor(props) {
        super(props);
        this.sbholderDiv = null;
        this.updateChart = this.updateChart.bind(this);
        this.sbholder = this.sbholder.bind(this);
        this.findObjectFromData = this.findObjectFromData.bind(this);
        // var data = [
        //     { date: '01-Aug', totalTransfer: 20, successTransfer: 5, failedTransfer: 10 },
        //     { date: '02-Aug', totalTransfer: 30, successTransfer: 10, failedTransfer: 10 },
        //     {date: '03-Aug', totalTransfer: 30, successTransfer: 17, failedTransfer: 13},
        //     {date: '04-Aug', totalTransfer: 30, successTransfer: 19, failedTransfer: 11},
        //     {date: '05-Aug', totalTransfer: 30, successTransfer: 10, failedTransfer: 20}
        // ];
        this.state = {
            data: props.data ? props.data : data
        }
    }



    sbholder(element) {
        this.sbholderDiv = element;
    }

    findObjectFromData(date) {
        var index = this.state.data.findIndex(obj => { return obj.date == date });
        return this.state.data[index];
    }


    updateChart() {
        var xData = ["totalSuccess", "totalFailures","totalInProgress","totalPIIcheckHold","others"];
        var legendsColor = {       
            "totalSuccess": "#7BCCC4",
            "totalFailures": "#009BBB",
            "totalInProgress":"blue",
            "totalPIIcheckHold":"orange",
            "others":"yellow"
        };

        var margin = { top: 20, right: 50, bottom: 30, left: 50 },
            width = 100 + (this.state.data.length * 100) - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;


        var x = d3.scaleBand()
            .range([0, width])
            .padding(0.1);

        var y = d3.scaleLinear()
            .range([height, 0]);

        var svg = d3.select(this.sbholderDiv).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        

        var data = this.state.data;
        var dataIntermediate = xData.map(function (c) {
            return data.map(function (d) {
                return { x: d.date, y: d[c] };
            });
        });

        //var dataStackLayout = d3.layout.stack()(dataIntermediate);
        var dataStackLayout = [];
        var yvector = [];
        for (var i = 0; i < data.length; i++) {
            yvector[i] = 0;
        }
        for (var i = 0; i < xData.length; i++) {
            var tmpArr = [];
            for (var j = 0; j < data.length; j++) {
                tmpArr.push({ x: data[j].date, y0: yvector[j], y: data[j][xData[i]] });
                yvector[j] += data[j][xData[i]];
            }

            dataStackLayout.push(tmpArr)
        }

        x.domain(dataStackLayout[0].map(function (d) {
            return d.x;
        }));

        y.domain([0,
            d3.max(dataStackLayout[dataStackLayout.length - 1],
                function (d) { return d.y0 + d.y; })
        ])
            .nice();

       

        // svg.selectAll(".tick text").each(function (data) {
        //     var tick = d3.select(this);
        //     tick.text(data.date);
        // });

        var tooltip = d3.select("body").append("div").attr("class", "tooltip tooltip-light")
         .attr("data-placement", "bottom");

        var layer = svg.selectAll(".stack")
            .data(dataStackLayout)
            .enter().append("g")
            .attr("class", "stack")
            .style("fill", function (d, i) {
                return legendsColor[xData[i % 3]];
            });

        layer.selectAll("rect")
            .data(function (d) {
                return d;
            })
            .enter().append("rect")
            .attr("x", function (d) {
                return (x(d.x) + x.bandwidth()/4);
            })
            .attr("y", function (d) {
                return y(d.y + d.y0);
            })
            .attr("height", function (d) {
                return y(d.y0) - y(d.y + d.y0);
            })
            .attr("width", x.bandwidth() / 2)
            .on("mouseover", function (d) {
                console.log(y(0));
                console.log(y(d.data));
                tooltip
                    .style("left", d3.event.pageX - 60 + "px")
                    .style("top", d3.event.pageY + "px")
                    .style("display", "inline-block")
                    .html((d.y0 == 0  ) ? ("Total Success : " + d.y) : ("Total Failures : " +( d.y)))
                    .style("visibility", "visible")
                    .style("opacity", "1");
            })
            .on("mouseout", function (d) {
                tooltip.style("visibility", "hidden");
            });

             // add the y Axis
        svg.append("g")
            .call(d3.axisLeft(y))
        svg.selectAll(".tick text").each(function (data) {
            var tick = d3.select(this);
            tick.text(tick.text());
        });

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        drawLegend();

        function drawLegend() {

            // Dimensions of legend item: width, height, spacing, radius of rounded rect.
            var li = {
                w: 125, h: 30, s: 3, r: 3
            };

            var legend = d3.select("#legend").append("svg:svg")
                .attr("id", "legendSvg")
                .attr("width", d3.keys(legendsColor).length * (li.w + li.s))
                .attr("height", li.h);

            var g = legend.selectAll("g")
                .data(d3.entries(legendsColor))
                .enter().append("svg:g")
                .attr("transform", function (d, i) {
                    return "translate(" + i * (li.w + li.s) + ",10)";
                });

            g.append("svg:rect")
                .attr("rx", li.r)
                .attr("ry", li.r)
                .attr("width", li.w)
                .attr("height", li.h)
                .style("fill", function (d) { return d.value; });

            g.append("svg:text")
                .attr("x", li.w / 2)
                .attr("y", li.h / 2)
                .attr("dy", "0.35em")
                .attr("text-anchor", "middle")
                .text(function (d) { return d.key; });
        }
    }


    componentDidMount() {
        this.updateChart();
    }

    render() {
        return (
            <div>
                <div className="row border">
                    <div id="main">
                        <div>
                            {/* <div id="sequence"></div> */}
                            <div className="legend-text"><span>Legends : </span></div>
                            <div id="legend"></div>
                        </div>
                        <div ref={this.sbholder} className="stackedbarchart"></div>
                    </div>
                </div>
            </div>
        );
    }
}


// [
//     {
//         "totalTransfer": 15,
//         "totalSuccess": 0,
//         "totalFailures": 3,
//         "totalInProgress": 1,
//         "totalPIIcheckHold": 0,
//         "others": 11,
//         "date": "2018-05"
//     },
//     {
//         "totalTransfer": 100,
//         "totalSuccess": 37,
//         "totalFailures": 44,
//         "totalInProgress": 2,
//         "totalPIIcheckHold": 1,
//         "others": 16,
//         "date": "2018-06"
//     },
//     {
//         "totalTransfer": 43,
//         "totalSuccess": 0,
//         "totalFailures": 6,
//         "totalInProgress": 13,
//         "totalPIIcheckHold": 2,
//         "others": 22,
//         "date": "2018-07"
//     },
//     {
//         "totalTransfer": 2,
//         "totalSuccess": 0,
//         "totalFailures": 0,
//         "totalInProgress": 0,
//         "totalPIIcheckHold": 0,
//         "others": 2,
//         "date": "2018-08"
//     }
// ]
