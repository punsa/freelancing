import React, { Component } from 'react';
import Services from "../services/Services.js";
import "./ClusterForm.css";

class ClusterForm extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.validation = this.validation.bind(this);
        this.getValidationObj = this.getValidationObj.bind(this);
        //this.convertData = this.convertData.bind.

        this.state = {
            clusterForm: props.data,
            isNew: props.isNew,
            isFormOk: (props.isNew == "false"),
            // validationObj: {
            //     "useCaseName": {
            //         "touched": false,
            //         "error": false,
            //         "errorMessage": ""
            //     },
            //     "userEmailId": {
            //         "touched": false,
            //         "error": false,
            //         "errorMessage": ""
            //     }
            // }
            validationObj: this.getValidationObj(props.data, props.isNew)
        };
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({ clusterForm: nextProps.data, validationObj: this.getValidationObj(nextProps.data) });
    }

    getValidationObj = (obj, isNew = true) => {
        var keys = ["clustername", "slave_instancecount", "spotprice", "instancecount", "slave_spotprice"]
        var validationObj = {}
        //var keys = ['useCaseName', 'useCaseDesc', 'ownerEmail'];
        var touched = (isNew == "false");
        for (var i = 0; i < keys.length; i++) {
            validationObj[keys[i]] = {
                "touched": touched,
                "error": false,
                "errorMessage": ""
            }
        }
        return validationObj;
    }
    submitActivate = () => {
        var tmpObj = this.state.validationObj;
        var keys = ["clustername", "slave_instancecount", "slave_spotprice", "instancecount", "slave_spotprice"]
        var flag = true;
        for (var i = 0; i < keys.length; i++) {
            if (!(tmpObj[keys[i]].touched == true && tmpObj[keys[i]].error == false)) {
                var flag = false;
                break;
            }
        }
        this.setState({ isFormOk: flag });
    }

    validation = (event) => {
        var tmpValidationObj = this.state.validationObj;
        if (!tmpValidationObj[event.target.name]) {
            tmpValidationObj[event.target.name] = {};
        }
        tmpValidationObj[event.target.name]["touched"] = true;
        switch (event.target.name) {
            case 'clustername':
                var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
                if (event.target.value.length > 40) {
                    tmpValidationObj[event.target.name]["error"] = true;
                    tmpValidationObj[event.target.name]["errorMessage"] = "Length should be less than 40 characters";
                }
                else {
                    tmpValidationObj[event.target.name]["error"] = false;
                    tmpValidationObj[event.target.name]["errorMessage"] = "";
                }
                break;
            case 'slave_instancecount':
                var no = parseFloat(event.target.value);
                var MAX_NO = 99;
                if (no > MAX_NO) {
                    tmpValidationObj[event.target.name]["error"] = true;
                    tmpValidationObj[event.target.name]["errorMessage"] = "It should be less than " + MAX_NO;
                }
                else {
                    tmpValidationObj[event.target.name]["error"] = false;
                    tmpValidationObj[event.target.name]["errorMessage"] = "";
                }
                break;
            case 'slave_spotprice':
                var no = parseFloat(event.target.value);
                var MAX_NO = 99;
                if (no > MAX_NO) {
                    tmpValidationObj[event.target.name]["error"] = true;
                    tmpValidationObj[event.target.name]["errorMessage"] = "It should be less than " + MAX_NO;
                }
                else {
                    tmpValidationObj[event.target.name]["error"] = false;
                    tmpValidationObj[event.target.name]["errorMessage"] = "";
                }
                break;
            case 'instancecount':
                var no = parseFloat(event.target.value);
                var MAX_NO = 99;
                if (no > MAX_NO) {
                    tmpValidationObj[event.target.name]["error"] = true;
                    tmpValidationObj[event.target.name]["errorMessage"] = "It should be less than " + MAX_NO;
                }
                else {
                    tmpValidationObj[event.target.name]["error"] = false;
                    tmpValidationObj[event.target.name]["errorMessage"] = "";
                }
                break;
            case 'spotprice':
                var no = parseFloat(event.target.value);
                var MAX_NO = 99;
                if (no > MAX_NO) {
                    tmpValidationObj[event.target.name]["error"] = true;
                    tmpValidationObj[event.target.name]["errorMessage"] = "It should be less than " + MAX_NO;
                }
                else {
                    tmpValidationObj[event.target.name]["error"] = false;
                    tmpValidationObj[event.target.name]["errorMessage"] = "";
                }
                break;
            default:
                break;
        }
        this.setState({ validationObj: tmpValidationObj });
    }


    onSubmitClick = () => {
        console.log(this.state.clusterForm);
        this.handleClose(1);
        //TODO :- write logic to put this data to api.
        // if(this.state.useCaseForm.ucId && this.state.useCaseForm.ucId != "") {
        //     Services.putUseCaseData(this.convertData()).then(res => {
        //         console.log(res);
        //         this.handleClose(2);
        //     })
        // }
        // else {
        //     Services.postUseCaseData(this.convertData()).then(res => {
        //         console.log(res);
        //         this.handleClose(1);
        //     })
        // }

    }

    onChangeValue = (event) => {
        var numberTypes = ["slave_instancecount", "slave_spotprice", "spotprice", "instancecount"];
        if (numberTypes.includes(event.target.name)) {
            event.target.value = event.target.value.replace(/[^0-9]/g, '');
        }

        if (event.target.name == "clustername") {
            event.target.value = event.target.value.replace(/[^a-zA-Z0-9]/g, '');
        }

        this.validation(event);

        // console.log(event);
        var tmpObj = this.state.clusterForm;
        tmpObj[event.target.name] = event.target.value;
        // if(event.target.name == "applicationId"){
        //   var val = event.target.value.replace(/\D/g,'').substr(0,10);
        //   tmpObj.applicationId = val;
        // }
        // if(event.target.name == "useCaseType"){
        //     var tmpValidationObj = this.state.validationObj;
        //     if(event.target.value == "poc"){
        //         tmpValidationObj["applicationId"] = {touched : true, error : false}
        //     } else {
        //         tmpValidationObj["ucOwnerEmailId"] = {touched : true, error : false}
        //     }
        //     this.setState({validationObj : tmpValidationObj})
        // }
        this.setState({ clusterForm: tmpObj });
        console.log(this.state.clusterForm);
        //this.submitActivate();
    }

    componentDidMount() {
        document.body.style.overflow = 'hidden';
    }

    componentWillUnmount() {

    }


    handleClose = (status) => {
        this.props.onClose(status);
    }

    setModalRef = (node) => {
        this.modalRef = node;
    }


    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen style={{ width: 50px}}"></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": 600 }}>
                    <div className="modal" style={{ top: "5%", left: "420px", width: "50%" }}>
                        {/*<div className="modal-screen style={{ width: 50px}}"></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": 400 }}>
                    <div className="modal" style={{ top: "5%", left: "250px", width: "50%" }}>*/}
                        <div className="container" ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">{this.state.isNew == "true" ? 'Create New' : 'Edit'} Cluster</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.handleClose}
                                        data-toggle="tooltip-auto" title="Close"
                                        className="mlp-cursor-pointer dls-glyph-close"></image>
                                </div>
                            </div>
                            <div className="usecase-form background-white">
                                {/* name */}

                                <div className="row">
                                    <div className={"col-md-6 " + (this.state.validationObj['clustername']['error'] ? 'has-warning' : '')}>
                                        <label htmlFor="name">Cluster Name</label>
                                        <input type="text" id="clustername" name="clustername" value={this.state.clusterForm.clustername} className={"form-control " + (this.state.validationObj['clustername']['error'] ? 'form-control-warning' : '')} onChange={this.onChangeValue} />
                                        <span className={this.state.validationObj["clustername"]["error"] ? 'block' : 'hide'}>{this.state.validationObj["clustername"]["errorMessage"]}</span>
                                    </div>

                                    <div className="col-md-6">
                                        <label htmlFor="egselect">Use-Case</label>
                                        <div className="select form-control" data-toggle="select">
                                            <select id="usecase" value={this.state.clusterForm.subnet} name="usecase" onChange={this.onChangeValue}>
                                                <option value="uc1">Use-case1</option>
                                                <option value="uc2">use-case2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div className="row">
                                    <div className="col-md-6">
                                        <label htmlFor="egselect">Environment</label>
                                        <div className="select form-control" data-toggle="select">
                                            <select id="environment" value={this.state.clusterForm.keyname} name="environment" onChange={this.onChangeValue}>
                                                <option value="aws">AWS</option>
                                                <option value="ecp">ECP</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <label htmlFor="egselect">Type</label>
                                        <div className="select form-control" data-toggle="select">
                                            <select id="type" value={this.state.clusterForm.type} name="type" onChange={this.onChangeValue}>
                                                <option value="aws">EMR</option>
                                                <option value="ec2">EC2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="row" select id="heading">
                                    <label>{this.state.clusterForm.type == "ec2" ? 'Node' : 'Master Node'} Attributes</label>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <label htmlFor="egselect">Instance Type</label>
                                        <div className="select form-control" data-toggle="select">
                                            <select id="instancetype" value={this.state.clusterForm.instancetype} name="instancetype" onChange={this.onChangeValue}>
                                                <option value="instancetype">128 vCores x 256GB</option>
                                                <option value="instancetype2">16 vCores x 122GB</option>
                                                <option value="instancetype3">4 vCores x 8GB</option>
                                                <option value="instancetype4">64 vCores x 488GB</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="col-md-3">
                                        <label htmlFor="instancecount">No.of Instances</label>
                                        <input type="text" id="instancecount" name="instancecount" value={this.state.clusterForm.instancecount} className={"form-control " + (this.state.validationObj['instancecount']['error'] ? 'form-control-warning' : '')} onChange={this.onChangeValue} />
                                        <span className={this.state.validationObj["instancecount"]["error"] ? 'block' : 'hide'}>{this.state.validationObj["instancecount"]["errorMessage"]}</span>
                                    </div>
                                    <div className="col-md-3">
                                        <label htmlFor="egselect">Volume Size(GB)</label>
                                        <div className="select form-control" data-toggle="select">
                                            <select id="volumesize" value={this.state.clusterForm.markettype} name="volumesize" onChange={this.onChangeValue}>
                                                <option value="vs1">10</option>
                                                <option value="vs2">20</option>
                                                <option value="vs3">30</option>
                                                <option value="vs4">40</option>
                                                <option value="vs5">50</option>
                                                <option value="vs6">60</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6">
                                        <label htmlFor="egselect">Pricing Type</label>
                                        <div className="select form-control" data-toggle="select">
                                            <select id="pricingtype" value={this.state.clusterForm.pricingtype} name="pricetype" onChange={this.onChangeValue}>
                                                <option value="on_demand">ON_DEMAND</option>
                                                <option value="spot">SPOT</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className={"col-md-6 " + (this.state.validationObj['spotprice']['error'] ? 'has-warning' : '')}>
                                        <label htmlFor="spotprice">Spot Price</label>
                                        <input type="text" id="spotprice" name="spotprice" data-toggle="inputfield" disabled value={this.state.clusterForm.spotprice} className={"form-control " + (this.state.validationObj['spotprice']['error'] ? 'form-control-warning' : '')} onChange={this.onChangeValue} />
                                        <span className={this.state.validationObj["spotprice"]["error"] ? 'block' : 'hide'}>{this.state.validationObj["spotprice"]["errorMessage"]}</span>
                                    </div>
                                </div>

                                {this.state.clusterForm.type != "ec2" && (
                                    
                                        <div className="row" select id="heading">
                                            <label>Slave  Node Attributes</label>
                                        </div>
                                )}
                                {this.state.clusterForm.type != "ec2" && (

                                        <div className="row">
                                            <div className="col-md-6">
                                                <label htmlFor="egselect">Instance Type</label>
                                                <div className="select form-control" data-toggle="select">
                                                    <select id="slave_instancetype" value={this.state.clusterForm.slave_instancetype} name="slave_instancetype" onChange={this.onChangeValue}>
                                                        <option value="slave_instancetype">128 vCores x 256GB</option>
                                                        <option value="slave_instancetype2">16 vCores x 122GB</option>
                                                        <option value="slave_instancetype3">4 vCores x 8GB</option>
                                                        <option value="slave_instancetype4">64 vCores x 488GB</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div className="col-md-3">
                                                <label htmlFor="slave_instancecount">No.of Instances</label>
                                                <input type="text" id="slave_instancecount" name="slave_instancecount" value={this.state.clusterForm.slave_instancecount} className={"form-control " + (this.state.validationObj['slave_instancecount']['error'] ? 'form-control-warning' : '')} onChange={this.onChangeValue} />
                                                <span className={this.state.validationObj["slave_instancecount"]["error"] ? 'block' : 'hide'}>{this.state.validationObj["slave_instancecount"]["errorMessage"]}</span>
                                            </div>
                                            <div className="col-md-3">
                                                <label htmlFor="egselect">Volume Size(GB)</label>
                                                <div className="select form-control" data-toggle="select">
                                                    <select id="slave_volumesize" value={this.state.clusterForm.slave_volumesize} name="slave_volumesize" onChange={this.onChangeValue}>
                                                        <option value="vs1">10</option>
                                                        <option value="vs2">20</option>
                                                        <option value="vs3">30</option>
                                                        <option value="vs4">40</option>
                                                        <option value="vs5">50</option>
                                                        <option value="vs6">60</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                )}

                                {this.state.clusterForm.type != "ec2" && (

                                        <div className="row">
                                            <div className="col-md-6">
                                                <label htmlFor="egselect">Pricing Type</label>
                                                <div className="select form-control" data-toggle="select">
                                                    <select id="slave_pricingtype" value={this.state.clusterForm.slave_pricingtype} name="slave_pricingtype" onChange={this.onChangeValue}>
                                                        <option value="on_demand">ON_DEMAND</option>
                                                        <option value="spot">SPOT</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className={"col-md-6 " + (this.state.validationObj['slave_spotprice']['error'] ? 'has-warning' : '')}>
                                                <label htmlFor="slave_spotprice">Spot Price</label>
                                                <input type="text" id="slave_spotprice" name="slave_spotprice" data-toggle="inputfield" disabled value={this.state.clusterForm.slave_spotprice} className={"form-control " + (this.state.validationObj['slave_spotprice']['error'] ? 'form-control-warning' : '')} onChange={this.onChangeValue} />
                                                <span className={this.state.validationObj["slave_spotprice"]["error"] ? 'block' : 'hide'}>{this.state.validationObj["slave_spotprice"]["errorMessage"]}</span>
                                            </div>

                                        </div>
                                )}
                                



                                <div className="row">
                                    <div className="col-md-3"></div>
                                    <div className="col-md-4">
                                        <button className="btn btn-primary" onClick={this.handleClose}>Cancel</button>
                                    </div>
                                    <div className="col-md-3">
                                        <button className="btn btn-primary" disabled={!this.state.isFormOk} onClick={this.onSubmitClick}>Submit</button>
                                    </div>
                                    <div className="col-md-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default ClusterForm;




