'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const request = require('request');
const appConfig = require('./../config/app-config');
var mlp = require('./mlpservice');
const mlpLogger = require('./loggerService');
const logger = mlpLogger.createLogger();

router.get('/initializing/:notebookEnv/:clusterId', function (req, res) {

  logger.info('Launching Gold Jupyter Notebook Single User Server ......');
  var token = req.userContext.tokens['access_token']
  var endpoint = appConfig.MLP_API_URL + "/api/v1/clusters/user/" + req.userContext.userinfo['email'] + "?filterType=equals&filterKey=clusterStatus";
  mlp.executeRequest(endpoint, 'GET', token, {}, function (err, results) {
    if (!err) {
      
      results = JSON.parse(results);
      console.log("********",results);
      var found = false;
      if (results && results['clusters']) {
        var index = results['clusters'].findIndex(x => x.clusterId == req.params.clusterId);
        if (index != -1) {
          found = true;
        }
      }
      if (found) {
        var username = req.userContext.userinfo['preferred_username']

        var notebookIP = req.params.clusterId
        var notebookDir = "/adshome/" + username
        var servername = req.params.notebookEnv

        if (req.params.notebookEnv == 'gold') {
          servername = "goldserver"
        } else if (req.params.notebookEnv == 'goldml') {
          servername = "goldmlserver"
        } else if (req.params.notebookEnv == 'ecp') {
          servername = "ecpserver"
        }

        var userOptions = {
          "notebook_ip": notebookIP,
          "notebook_dir": notebookDir,
        }

        if (req.query.project) {
          usetOptions["project_repo"] = "https://prathor@stash.aexp.com/stash/scm/~prathor/mlp-local-agent.git"
        }

        launchJupyterServer(token, username, servername, userOptions, res)
      }
      else {
        res.redirect("/initializing/" + req.params.clusterId);
      }

    } else {
      console.log("$$$$$$$$$$", err);
      res.redirect("/initializing/" + req.params.clusterId);
    }
  })
})

function launchJupyterServer(token, username, servername, userOptions, res) {

  var tokenData = { "auth": token, "expires_in": appConfig.JPYHUB_TOKEN_EXPIRY }
  executeRequest(appConfig.MLP_GOLD_MLSI_API + '/hub/api/users/' + username + '/tokens',
    'POST', null, tokenData, function (error, tokenRes) {

      if (!error) {

        logger.info('MLP JupyterHub API Result :' + tokenRes);
        var tokenResObj = JSON.parse(tokenRes);
        var apitoken = tokenResObj.token;
        logger.info('MLP JupyterHub API Token :' + apitoken);
        executeRequest(appConfig.MLP_GOLD_MLSI_API + '/hub/api/users/' + username + '/servers/' + servername,
          'POST', apitoken, userOptions, function (error, spawnerRes) {
            if (!error) {
              logger.error('MLP JupyterHub API Result :' + spawnerRes);

              var maxAge = 1 * 60 * 1000 // 2 minutes
              res.cookie('MLP-XSRF-TOKEN', token, { 'maxAge': maxAge, 'domain': 'aexp.com' })

              //http://localhost:8000/user/myuser/tree?#notebooks
              //http://localhost:8000/user/myuser/notebooks/Untitled.ipynb?kernel_name=python3

              res.redirect(appConfig.MLP_GOLD_MLSI + '/user/' + username + '/' + servername)

            } else {
              error = new Error('MLP JupyterHub Request Failed ' + error.toString());
              error.name = 'MLP823'
              res.status(500).send(error)
            }
          })
      } else {
        error = new Error('MLP JupyterHub Request Failed ' + error.toString());
        error.name = 'MLP822'
        res.status(500).send(error)
      }
    })

}
function executeRequest(endpoint, method, token, data, callback) {

  let headers = {
    'Content-Type': 'text/json',
    'Authorization': 'Bearer ' + token
  }

  var options = {
    url: endpoint,
    headers: headers,
    body: JSON.stringify(data),
    method: method,
    // key: privateKey,
    // cert: certificate,
    // passphrase: pass
  };

  logger.debug('MLP JupyterHub API Request URL:' + options.url + ' Body : ' + JSON.stringify(data) + ' Header :' + JSON.stringify(headers));

  request(options, function (error, response, body) {

    if (response) {
      logger.debug('MLP JupyterHub Response Status :' + response.statusCode + ' Body :' + body);
    }

    if (!error && response && (response.statusCode >= 200 && response.statusCode < 400)) {
      if (response.statusCode == 204) {
        callback(error, "{}");
      }
      else {
        callback(error, body);
      }
    }
    else if (response && response.statusCode == 400) {
      callback(error, body);
    }
    else {
      if (!error) {
        logger.error('MLP JupyterHub API Result :' + body);
        error = new Error('MLP JupyterHub Request Failed. Status Code: ' + response.statusCode);
        error.name = 'MLP821'
      } else {
        error = new Error('MLP JupyterHub Request Failed ' + error.toString());
        error.name = 'MLP822'
      }
      logger.error('MLP Error :' + error.toString());
      callback(error, response);
    }
  })
}

module.exports = router
