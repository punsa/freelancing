import React from 'react';
import DataTransfer from "./DataTransferDetails.jsx";
import DMMetaData from "./DMMetaData.jsx";
import Services from "../services/Services.js";

class DataTransferInfo extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            rowData: null,
            dmMetaData: null,
            pGridError: null,
            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate: "<div class='alert alert-neutral alert-dismissible anim-fade in' role='alert'>" +
            "<span class='alert-icon dls-icon-info-filled'></span><span>No Record Found.</span></div>"
        };
    }


    componentDidMount = () => {
        var parent = this;
        var offset = new Date().getTimezoneOffset();
        Services.get("/api/dm/dataTransfer/"+ parent.props.transferId +"?extended=true&timezone=LOCAL&timezoneOffset=" + offset )
            .then(function (response) {
                let rowData = response.data.extendedTransResult ? response.data.extendedTransResult.moduleStats : []
                return  parent.setState({ rowData:rowData });

            })
            .catch(function (error) {
                parent.setState({ pGridError: error });
                console.error('AJAX error occured : ' + error);
            })

        Services.get("/api/dm/dataTransfer/"+ parent.props.transferId +"?extended=true")
            .then(function (response) {
                //parent.setState({ dmMetaData: response.data.dataSetFormat.columns  || [] });
                let dmMetaData = response.data.dataSetFormat ? response.data.dataSetFormat.columns : []
                return  parent.setState({ dmMetaData:dmMetaData });
            })
            .catch(function (error) {
                parent.setState({ pGridError: error });
                console.error('AJAX error occured : ' + error);
            })

        this.dataTransferRef.click();
        document.addEventListener('mousedown', this.handleClickOutside);
    }


    componentWillUnmount = () => {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.modalRef && !this.modalRef.contains(event.target)) {
            this.handleClose();
        }
    }

    handleClose = () => {
        this.props.onClose();
    }

    setModalRef = (node) =>{
        this.modalRef = node;
    }

    setDataTransferRef = (node) => {
        this.dataTransferRef = node;
    }

    updateNoDataTemplate = (info) => {
        let msg = info.message ? info.message : "NO DATA";
        this.setState({
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'>" +
            "<span class='alert-icon dls-icon-warning-filled'>" +
            "</span>" + "<span>" + msg + "</span>" + "</div>"
        });
    }
    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen"></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": 600 }}>
                    <div className="modal" style={{ top: "7%" }}>
                        <div className="container" ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">TRANSFER ID : {this.props.transferId}</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.handleClose}
                                           data-toggle="tooltip-auto" title="Close"
                                           className="mlp-cursor-pointer dls-glyph-close"></image>
                                </div>
                            </div>
                            <div className="tabs" data-toggle="tabs" role="tablist">
                                <div className="tab-menu">
                                    <select className="form-control mobile">
                                        <option role="tab" data-value="1" aria-selected="true">Overview</option>
                                        <option role="tab" data-value="2" aria-selected="false">MetaData</option>
                                    </select>
                                    <div className="form-control desktop">
                                        <div className="tab-link" role="tab" data-value="1" aria-selected="true" style={{ padding: 0 }} ref={this.setDataTransferRef}>
                                            <span>Overview</span>
                                        </div>
                                        <div className="tab-link" role="tab" data-value="2" aria-selected="false" style={{ padding: 0 }}>
                                            <span>MetaData</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="dls-accent-white-01-bg tab-content-container">
                                    <div className="tab-content" role="content" data-value="1" aria-selected="true">
                                        <DataTransfer rowData={this.state.rowData} errorInfo={this.state.pGridError}
                                                      transferId={this.props.transferId} userInfo={this.props.userInfo}></DataTransfer>
                                    </div>
                                    <div className="tab-content" role="content" data-value="2" aria-selected="false">
                                        <DMMetaData rowData={this.state.dmMetaData} errorInfo={this.state.pGridError}
                                                    transferId={this.props.transferId}></DMMetaData>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DataTransferInfo;