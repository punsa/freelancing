class LoaderPopup extends Component {
    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen" style={{ zIndex: 10 }}></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": "100px", "maxWidth": "300px !important" }}>
                    <div className="modal" style={{ zindex: 10, top: "20%" }}>
                        <div className="container" style={{ width: "35%" }}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">Loading...</h2>
                                </div>
                            </div>
                            <div className="usecase-form background-white" style={{ "overflow": "hidden", "height": "150px" }}> <div class="form"><div class='progress-circle progress-indeterminate progress' style={{
                                "display": "block", "position": "center",
                                "left": "50%", "marginTop": "40px", "marginLeft": "-3em"
                            }}></div></div> </div></div></div></div></div>
        )
    }
}

export default LoaderPopup;