import React, { Component } from "react";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import 'ag-grid/dist/styles/ag-grid.css';
import '../public/css/mlp-ag-grid.scss';
import appConfig from "../config/web-config";
import ClusterInfo from "./ClusterInfo.jsx";
import Services from "../services/Services.js";
import AdminMenu from '../components/AdminMenu.jsx';
import DateFilter from '../components/filters/datefilter.jsx';
import TimeFilter from '../components/filters/timefilter.jsx';
import StatusFilter from '../components/filters/statusfilter.jsx';
import moment from "moment";
import DynamicForm from '../components/dynamicform/index.jsx';
import SuppressForm from './SuppressForm.jsx';
import StatusPopup from './statuspopup.jsx';
import LoaderPopup from './loaderpopup.jsx';
import NavHeader from './navheader.jsx';

export class SnoozeHeader extends Component {
    render() {
        return (
            <div class="text-align-center">
                <div class="">
                    <a
                        class="icon dls-icon-alert"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Snooze Notification"
                    ></a>
                </div>
            </div>
        )
    }
}

export default class ClusterGrid extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            selectedRow: -1,
            selectedCluster: null,
            showRowTooltip: false,
            clusterData: null,
            clusterInfo: null,
            clusterMouseOver: null,
            filterKey: null,
            filterType: null,
            filterValue: null,
            sortType: null,
            isHomeScreen: props.isHomeScreen == null ? false : props.isHomeScreen,
            userinfo: props.userinfo,
            sortBy: null,
            statuspopup: null,
            startRow: 1,
            current: {},
            showDataLoading: true,
            endRow: appConfig.CLUSTER_GRID_BLOCK_SIZE,
            showLoaderPopup: null,
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
                    }
                }
            },
            frameworkComponents: {
                statusFilter: StatusFilter,
                dateFilter: DateFilter,
                timeFilter: TimeFilter
            },
            rowBuffer: 0,
            rowModelType: "infinite",
            paginationPageSize: appConfig.CLUSTER_GRID_PAGE_SIZE,
            cacheBlockSize: appConfig.CLUSTER_GRID_BLOCK_SIZE,
            // maxBlocksInCache: 0,
            maxConcurrentDatasourceRequests: 2,
            infiniteInitialRowCount: 0,
            overlayLoadingTemplate: "<div class='progress-circle progress-indeterminate progress'></div>",
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'><span class='alert-icon dls-icon-warning-filled'></span>" +
                "<span>No Record Found.</span>" +
                "</div>",

            getRowNodeId: function (item) {
                return item.clusterId;
            },
            tooltipInfo: {}
        };
        this.statusFilterOptions = [
            { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
            { value: 'REQUEST', label: 'REQUEST' },
            { value: 'INITIALIZING', label: 'INITIALIZING' },
            { value: 'READY', label: 'READY' },
            { value: 'IDLE', label: 'IDLE' },
            { value: 'RUNNING', label: 'RUNNING' },
            { value: 'TERMINATING', label: 'TERMINATING' },
            { value: 'TERMINATED', label: 'TERMINATED' }
        ];
        this.typeFilterOptions = [
            { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
            { value: 'EMR', label: 'EMR' },
            { value: 'EC2', label: 'EC2' },
            { value: 'GPU', label: 'GPU' },

        ];
        this.terminatedFilterOptions = [
            { value: 'CLEAR FILTER', label: 'CLEAR FILTER' },
            { value: 'USER', label: 'USER' },
            { value: 'SYSTEM', label: 'SYSTEM' },
            { value: 'AWS', label: 'AWS' }
        ];
        this.valueFilterOption = [];
        this.onCreateClick = this.onCreateClick.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
    }

    toolTipRef = (elem) => {
        this.tooltipDiv = elem;
    }
    setDefaultCluster = () => {
        return {
            clustername: "",
            slave_instancecount: "",
            spotprice: ""
        };
    }

    onSelectionChanged = (params) => {
        this.gridApi = params.api;
        // if (params.node.rowIndex === this.state.selectedRow) {
        //     this.setState({ selectedRow: -1 });
        //     this.gridApi.getSelectedNodes()[0];
        //     this.gridApi.deselectAll();
        //     this.setState({ selectedCluster: null });
        //     var selector = "div[row-index='" + params.node.rowIndex + "']";
        //     document.querySelector(selector).classList.remove("ag-row-focus");
        // }
        // else {
        this.setState({ selectedRow: params.node.rowIndex });
        // var selectedRows = this.gridApi.getSelectedRows();
        this.setState({ selectedCluster: this.state.clusterData[params.node.rowIndex] });
        //}
    }


    onGridReady = (params) => {
        var parent = this;
        this.gridApi = params.api;
        this.columnApi = params.columnApi;

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                parent.gridApi.showLoadingOverlay()
                parent.state.startRow = params.startRow + 1;
                parent.state.endRow = params.endRow;

                parent.loadData(function () {

                    setTimeout(function () {
                        var data = parent.state.clusterData;

                        var rowsThisPage = data.slice(params.startRow, params.endRow);
                        var lastRow = -1;
                        if (data.length < params.endRow) {
                            lastRow = data.length;
                        }
                        if (rowsThisPage.length > 0) {
                            parent.gridApi.hideOverlay();

                        } else if (data.length > 0 && params.startRow === data.length) {
                            parent.gridApi.hideOverlay();

                        } else {
                            parent.gridApi.showNoRowsOverlay();
                        }
                        params.successCallback(rowsThisPage, lastRow);
                    }, 500);
                });
            }
        };
        params.api.setDatasource(dataSource);
    };

    onClkExportToExcel = (event) => {
        this.refs.clusterGridRef.gridOptions.api.exportDataAsCsv();
    }

    onCloseModal = (status) => {
        this.setState({ clusterFormModal: null });
        if (status == 1) {
            var ele = <StatusPopup onClose={this.onPopupClose} message="Cluster termination initiated..!!" />
            this.setState({ statusPopup: ele });
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ userinfo: nextProps.userinfo });
    }

    onSnoozeClose = (status) => {
        this.setState({ suppressForm: null });
        var modalEle = null;
        switch (status) {
            case 0:
                modalEle = <StatusPopup onClose={this.onPopupClose} message="Notification snoozed successfully!!" />
                break;
            case 1:
                modalEle = null;
                break;
            default:
                modalEle = null;
        }
        this.refreshPage();
        this.setState({ statusPopup: modalEle, showLoaderPopup: null });
    }

    onRowDoubleClicked = (row) => {
        var clusterInfoComponent = <ClusterInfo onClose={() => { this.onModalClose(false) }} clusterId={row.data.clusterId} terminatedBy={row.data.terminatedBy} />
        this.setState({ clusterId: row.data.clusterId, clusterInfo: clusterInfoComponent });
    }

    onModalClose = (refreshPageStatus = true) => {
        if (refreshPageStatus) {
            this.refreshPage();
        }
        this.setState({ clusterFormModal: null });
        this.setState({ clusterInfo: null });
        this.setState({ suppressForm: null });
        this.setState({ modal: null });
    }

    turnOffSnooze = (row) => {
                this.setState({ showLoaderPopup: <LoaderPopup /> });
                Services.update('/api/utility/notify/' + row.data.clusterId, { snoozeDuration: 0, requestedBy: this.state.userinfo.emailId }).then(res => {
                    this.refreshPage();
                    if (res.status == 200) {
                        //this.hhmmsstoTotalSeconds(this.state.newSuppressTime)
                        var ele = <StatusPopup onClose={this.onPopupClose} message="Notification snooze is turned off"
                        />
                        this.setState({ statusPopup: ele, showLoaderPopup: null });
                    }
                })
                    .catch(function (err) {
                        if (err && err.response.status === 500) {
                            parent.updateNoDataTemplate(err);

                        } else if (err && err.response.status === 409) {
                            parent.duplicateMsg(err);
                        }
                    })
    }

    onCellClicked = (row) => {
        if (row.colDef.field === "snoozeFlag" && (row && row.data && row.data.clusterRunTime > 24 * 3600 && row.data.clusterStatus == "Running")) {
            if (row.data.snoozeFlag) {
                var ele = <StatusPopup onClose={(val) => { this.setState({statusPopup: null});if(val == 1) { this.turnOffSnooze(row); } } } alertBox={true} headingMessage = "Seeking confirmation" message="Confirm to turn off snooze"
                />
                this.setState({ statusPopup: ele, showLoaderPopup: null });
            }
            else {
                this.onSuppressClick(row.data.clusterId);
            }
        }
    }

    onCellMouseOver = (row) => {
        //if(row.data.clusterStatus == "TRMTD")
        if (row.colDef.field === "clusterStatus" && row.data.clusterStatus === "Idle") {
            let x = row.event.clientX, y = row.event.clientY;
            this.tooltipDiv.style.top = (y + 2) + 'px';
            this.tooltipDiv.style.left = (x + 2) + 'px';
            let tStamp = row.data.statusUpdateTs, newDate, newDate30;
            if (tStamp) {
                newDate = new Date(tStamp);
                newDate30 = moment(newDate).add(30, 'minutes').format('llll');
                //format('MM/DD/YYYY HH:mm:ss ')+"MST"
            } else newDate30 = "";

            this.setState({
                tooltipInfo:
                {
                    clusterStatus: "To Be Terminated By ", statusUpdateTs: newDate30.toString()
                },
                showRowTooltip: true
            });
        }
    }

    onCellMouseOut = () => {
        this.setState({ showRowTooltip: false });
    }

    refreshPage = () => {
        var params = { type: "gridReady", api: this.gridApi, columnApi: this.columnApi };
        this.onGridReady(params);
    }

    onFilterModified = (params) => {
        var filters = Object.keys(params.api.getFilterModel());
        var filterLength = filters.length;
        var oldFilterKey = this.state.filterKey;
        var newFilterKey = null;
        if (filterLength == 1) {
            if (oldFilterKey && filters[0] !== oldFilterKey) {
                var filterInstance = params.api.getFilterInstance(oldFilterKey);
                filterInstance.setModel(null);
            }
            newFilterKey = filters[0];
        } else if (filterLength > 1) {

            for (var i = 0; i < filterLength; i++) {
                if (filters[i] === oldFilterKey) {
                    filterInstance = params.api.getFilterInstance(oldFilterKey);
                    filterInstance.setModel(null);
                } else {
                    newFilterKey = filters[i];
                }
            }
        }
        this.state.filterKey = newFilterKey;
    }

    onFilterChanged = (params) => {
        var filters = params.api.getFilterModel();
        if (Object.keys(filters).length == 1) {
            var filterKey = Object.keys(filters)[0];
            this.state.filterKey = filterKey;
            if (filters[filterKey].type === "inRange") {
                this.state.filterType = "between";
                this.state.filterValue = filters[filterKey].filter + '~' + filters[filterKey].filterTo;
            }
            else {
                this.state.filterType = filters[filterKey].type;
                this.state.filterValue = filters[filterKey].filter;
            }

        } else if (Object.keys(filters).length == 0) {
            this.state.filterKey = null;
            this.state.filterType = null;
            this.state.filterValue = null;
        }
    }

    resetFilter = () => {
        this.gridApi.destroyFilter("clusterType");
        this.gridApi.destroyFilter("clusterStatus");
        this.gridApi.destroyFilter("terminatedBy");
    }

    resetDateFilter = () => {
        this.gridApi.destroyFilter("clusterStartTs");
        this.gridApi.destroyFilter("clusterEndTs");
    }

    resetTimeFilter = () => {
        this.gridApi.destroyFilter("clusterRunTime");
    }

    onSortChanged = (params) => {
        var sortBy = params.api.getSortModel();
        if (sortBy.length > 0) {
            this.state.sortBy = sortBy[0].colId;
            this.state.sortType = sortBy[0].sort;
        } else {
            this.state.sortBy = null;
            this.state.sortType = null;
        }
    }

    onCreateClick() {
        Services.get("/api/clusters/getCluster")
            .then(res => {
                var defaultCluster = this.setDefaultCluster();
                // var modalEle = <ClusterForm onClose={this.onCloseModal} data={defaultCluster}/>
                var modalEle = <DynamicForm className="form"
                    title="Cluster Form"
                    defaultValues={res.data.defaultValues}
                    model={res.data.model}
                    mapper={res.data.mapper}
                    meta={res.data.meta}
                    submitFields={res.data.submitFields}
                    onClose={(e) => this.onCloseModal()}
                    onSubmit={(model) => { this.onClusterSubmit(model) }}
                />
                this.setState({ clusterFormModal: modalEle });
            })
            .catch(function (error) {
                console.error('AJAX error occured :' + error);
            })
    }

    onSuppressClick = (clusterId) => {
        var ele = <SuppressForm onClose={this.onSnoozeClose} userinfo={this.state.userinfo} clusterId={clusterId} />
        this.setState({ suppressForm: ele });;

    }

    onDeleteClick = () => {
        var ele = <StatusPopup onClose={this.onPopupClose} message="Are you sure you want to terminate the cluster?"
            alertBox={true} />
        this.setState({ statusPopup: ele });

    }

    onPopupClose = (status) => {
        this.setState({ statusPopup: null });
        if (status === 1) {
            this.onClusterdelete();
        }
    }

    onClusterSubmit(model) {
        var that = this;
        this.setState({clusterFormModal: null, showLoaderPopup: <LoaderPopup />});
        var stateObj = { ...model };
        stateObj["createdBy"] = this.state.userinfo.adsId.replace("@ads.aexp.com", "");
        stateObj["createdByEmail"] = this.state.userinfo.emailId;
        Services.create("/api/clusters", { ...stateObj })
            .then(res => {
                console.log(res);
                this.onCloseForm();
                this.refreshPage();
                // refresh cluster grid.

            })
            .catch(function (error) {
                //console.error('AJAX error occured :' + error);
                that.onCloseForm(1);
            })
    }

    onCloseForm = (status) => {
        this.setState({ clusterFormModal: null, showLoaderPopup: null });
        if (status == 1) {
            var ele = <StatusPopup onClose={this.onPopupClose} message="Sorry, cluster creation failed.Please try it again" />
            this.setState({ statusPopup: ele });
        }
        else {
            ele = <StatusPopup onClose={this.onPopupClose} message="Cluster creation initiated!!" />
            this.setState({ statusPopup: ele });
        }
    }


    onClusterdelete() {
        this.setState({clusterFormModal: null, showLoaderPopup: <LoaderPopup />});
        // Services.delete("/api/clusters", { data: { stackType: this.state.selectedCluster.clusterType.toLowerCase(), provider: this.state.selectedCluster.clusterEnv.toLowerCase(), stackName: this.state.selectedCluster.clusterId } })

        Services.delete("/api/clusters/"+ this.state.selectedCluster.clusterId)
            .then(res => {
                this.setState({ selectedCluster: null, selectedRow: -1, showLoaderPopup: null });
                console.log(res);
                this.onCloseModal(1);
                this.refreshPage();
                // var ele = <StatusPopup onClose={this.onPopupClose} message="Cluster is terminated!" />
                // this.setState({ statusPopup: ele });
                // refresh cluster grid.
            })
            .catch(function (error) {
                this.setState({ showLoaderPopup: null });
                console.error('AJAX error occured :' + error);
            })
    }


    loadData = (callback) => {
        var parent = this;
        var filterVal = (parent.state.filterKey == 'clusterStartTs' || parent.state.filterKey == 'clusterEndTs') ? moment.utc(new Date(parent.state.filterValue)).format("YYYY-MM-DD hh:mm:ss") : parent.state.filterValue;
        var svcReq = {
            startRow: parent.state.startRow,
            endRow: parent.state.endRow,
            filterKey: parent.state.filterKey,
            filterType: parent.state.filterType,
            filterValue: filterVal,
            sortType: parent.state.sortType,
            sortBy: parent.state.sortBy,
            timezone: "LOCAL",
            timezoneOffset: new Date().getTimezoneOffset()
        }

        var url = "/api/clusters";
        if (this.state.isHomeScreen) {
            url += "/user/" + this.state.userinfo.emailId;
        }
        else {
            url += "/user/ALL";
        }
        Services.get(url, svcReq)
            .then(res => {
                var data = parent.state.clusterData;
                if (data && parent.state.startRow > 1) {
                    data = data.concat(res.data.clusters);
                } else {
                    data = res.data.clusters;
                }
                parent.state.clusterData = data;
                callback()
            })
            .catch(function (error) {
                parent.updateNoDataTemplate(error);
                parent.gridApi.showNoRowsOverlay();
            })
    }

    idleTimeFormatter = (params) => {
        if (params.data.idleTime) {
            return this.hhmmss(params.data.idleTime);
        } else return "";
    }

    clusterRuntimeFormatter = (params) => {
        if (params && params.data && params.data.clusterRunTime) {
            return this.hhmmss(params.data.clusterRunTime);
        } else return "";
    }

    minUsrFormatter = (params) => {
        if (params && params.data && params.data.minUsrSession) {
            return this.hhmmss(params.data.minUsrSession);
        } else return "";
    }

    maxUsrFormatter = (params) => {
        if (params && params.data && params.data.maxUsrSession) {
            return this.hhmmss(params.data.maxUsrSession);
        } else return "";
    }

    avgUsrFormatter = (params) => {
        if (params && params.data && params.data.avgUsrSession) {
            return this.hhmmss(params.data.avgUsrSession);
        } else return "";
    }


    snoozeRenderer = (params) => {
        var html = "";
        var toDisable = (params && params.data && params.data.clusterRunTime > 24 * 3600 && params.data.clusterStatus == "Running") ? false : true;
        var clusterId = (params && params.data && params.data.clusterId) ? params.data.clusterId : "";
        if (params && params.data && params.data.snoozeFlag) {
            html = '<div class="stack"><div class="switch switch-on"  style = "width: 42%;height: 15px;margin-left: 13px;" tabindex="1" data-name="switch-1" data-on="" role="checkbox" aria-checked="true" aria-disabled="false"><div class="switch-handle" style = "width:40%"></div><input id="el62" name="switch-1" type="checkbox" value="true" style="display: none;"></div>';
            console.log(`snoozeFlag = ${params.data.snoozeFlag}`);
        }
        else {
            if (toDisable)
                html = '<div class="stack disabled"><div class="switch disabled"  style = "width: 42%;height: 15px;margin-left: 13px;" tabindex="1" data-name="switch-1" data-on="true" role="checkbox" aria-checked="false" aria-disabled="false"><div class="switch-handle" style = "width: 40%"></div><input id="el3" name="switch-1" type="checkbox" value="false" style="display: none;"></div></div>';
            else
                html = '<div class="stack"><div class="switch"  style = "width: 42%;height: 15px;margin-left: 13px;" tabindex="1" data-name="switch-2" role="checkbox" aria-checked="false" aria-disabled="false"><div class="switch-handle" style = "width: 40%"></div><input id="el63" name="switch-2" type="checkbox" value="false" style="display: none;"></div>';
        }
        return html;
    }

    convertToLocaleTimezone = (tmpTime) => {
        var offset = new Date(tmpTime).getTimezoneOffset();
        var momentTm = moment(tmpTime).subtract(offset, 'minutes');
        momentTm = momentTm.format("YYYY-MM-DD hh:mm:ss");
        console.log(momentTm);
        return momentTm;
    }
    updateClusterStartTS = (params) => {
        if (params && params.data && params.data.clusterStartTs) {
            return this.convertToLocaleTimezone(params.data.clusterStartTs);
        } else return "";
    }
    updateClusterEndTS = (params) => {
        if (params && params.data && params.data.clusterEndTs) {
            return this.convertToLocaleTimezone(params.data.clusterEndTs);
        } else return "";
    }
    updateNoDataTemplate = (info) => {
        let msg = info.message ? info.message : "No Record Found";
        this.setState({
            overlayNoRowsTemplate: "<div class='alert alert-warn alert-dismissible anim-fade in' role='alert'>" +
                "<span class='alert-icon dls-icon-warning-filled'>" +
                "</span>" + "<span>" + msg + "</span>" + "</div>"
        });
    }

    hhmmss = (totalSeconds) => {

        var hours = 0;
        totalSeconds = Math.round(totalSeconds);

        if (totalSeconds > 3600) {
            hours = Math.floor(totalSeconds / 3600);
            totalSeconds = totalSeconds - (hours * 3600);
        }

        var minutes = 0;
        if (totalSeconds > 60) {
            minutes = Math.floor(totalSeconds / 60);
            totalSeconds = totalSeconds - (minutes * 60);
        }

        var seconds = Math.round(totalSeconds);

        // round seconds
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds < 10 ? "0" + seconds : seconds);
        return result;
    }





    render() {
        return (  
            <div>
                {(this.isHomeScreen && <HomeMenu tab={"0"} />)}
                {(!this.state.isHomeScreen && <AdminMenu cluster={true} scorecard={false} usecase={false} />)}
                <div ref={this.toolTipRef}>
                    {this.state.showRowTooltip ?
                        <spanhover style={{ top: 'inherit', left: 'inherit' }}>
                            {this.state.tooltipInfo.clusterStatus} :
                            {this.state.tooltipInfo.statusUpdateTs}</spanhover> : ''}
                </div>
                    { this.state.userinfo && !this.state.userinfo.hasAdminRole && <NavHeader selectedTab={"0"} /> }
                <div className="main-container">
                    <span className="heading-3">INSTANCES</span>
                    <span className="cluster-excel">
                        <span onClick={this.onClkExportToExcel}
                            data-toggle="tooltip-auto" title="Export to Excel"
                            className="mlp-cursor-pointer icon-md dls-icon-download"></span>
                    </span>
                    <span className="main-refresh">
                        <span className="mlp-cursor-pointer icon-md dls-icon-refresh"
                            data-toggle="tooltip-auto"
                            title="Reload" onClick={this.refreshPage}></span>
                    </span>
                    { this.state.userinfo && this.state.userinfo.hasAdminRole && <button className="btn-sm ucterminate" style={{ marginRight: "10px", float: "right" }} disabled={this.state.selectedCluster == null || this.state.selectedCluster.clusterStatus == "Terminated" || this.state.selectedCluster.clusterStatus == "Terminating" || this.state.selectedCluster.clusterId.toLowerCase().indexOf("mlp") == -1} onClick={this.onDeleteClick}>
                        <span className="invisible">Terminate</span>
                    </button> }
                    { this.state.userinfo && this.state.userinfo.hasAdminRole && <button className="btn-sm uccreate" style={{ marginRight: "25px" }} onClick={this.onCreateClick}>
                        <span className="invisible">Create</span>
                    </button> }
                </div>
                <div>
                    {this.state.clusterFormModal}
                </div>


                <div className="cluster-grid" height="500px">
                    <div id="myGrid" style={{ height: '89%', width: '100%' }} className="ag-mlp">
                        <AgGridReact id="clusterGridId" ref="clusterGridRef"
                            //rowData={this.state.clusterData}
                            components={this.state.components}
                            headerHeight='50' rowHeight='40' //rowSelection="single"
                            onRowDoubleClicked={this.onRowDoubleClicked.bind(this)}
                            onCellMouseOver={this.onCellMouseOver.bind(this)}
                            onCellClicked={this.onCellClicked.bind(this)}
                            onCellMouseOut={this.onCellMouseOut.bind(this)}
                            // enableRangeSelection={false}
                            animateRows
                            // debug={true}
                            enableSorting
                            enableServerSideSorting
                            enableServerSideFilter
                            enableColResize
                            onRowClicked={this.onSelectionChanged.bind(this)}
                            //rowSelection={this.state.rowSelection}
                            rowBuffer={this.state.rowBuffer}
                            rowDeselection={true}
                            suppressRowClickSelection={true}
                            rowModelType={this.state.rowModelType}
                            cacheBlockSize={this.state.cacheBlockSize}
                            cacheOverflowSize={this.state.cacheOverflowSize}
                            maxConcurrentDatasourceRequests={this.state.maxConcurrentDatasourceRequests}
                            infiniteInitialRowCount={this.state.infiniteInitialRowCount}
                            maxBlocksInCache={this.state.maxBlocksInCache}
                            pagination={true}
                            paginationPageSize={this.state.paginationPageSize}
                            getRowNodeId={this.state.getRowNodeId}
                            onFilterChanged={this.onFilterChanged}
                            refreshPage={this.refreshPage}
                            onFilterModified={this.onFilterModified}
                            onSortChanged={this.onSortChanged}
                            overlayLoadingTemplate={this.state.overlayLoadingTemplate}
                            overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                            frameworkComponents={this.state.frameworkComponents}
                            onGridReady={this.onGridReady}>
                             <AgGridColumn hide={!this.state.userinfo || !this.state.userinfo.hasAdminRole} headerComponentFramework={SnoozeHeader} cellRenderer={this.snoozeRenderer} field="snoozeFlag" width={100} suppressFilter="true"></AgGridColumn> 
                            <AgGridColumn headerName="ID" field="clusterId" width={175}
                                filterParams={{
                                    applyButton: true, clearButton: true, newRowsAction: 'keep',
                                    caseSensitive: true, filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith']
                                }}></AgGridColumn>
                            <AgGridColumn headerName="TYPE" field="clusterType" width={145}
                                filter="statusFilter" filterParams={{
                                    filterOptions: this.typeFilterOptions, filterKey: 'clusterType',
                                    filterResetCallback: this.resetFilter, filterType: 'equals'
                                }}></AgGridColumn>
                            <AgGridColumn headerName="CREATOR" field="createdByEmail" width={250}
                                filterParams={{
                                    applyButton: true, clearButton: true, newRowsAction: 'keep', caseSensitive: true,
                                    filterOptions: ['equals', 'notEqual', 'contains', 'startsWith', 'endsWith']
                                }}></AgGridColumn>
                            <AgGridColumn headerName="STATUS" field="clusterStatus" width={145}
                                filter="statusFilter" filterParams={{
                                    filterOptions: this.statusFilterOptions, filterKey: 'clusterStatus',
                                    filterResetCallback: this.resetFilter, filterType: 'equals'
                                }}></AgGridColumn>
                            <AgGridColumn headerName="TERMINATED BY" field="terminatedBy" width={165}
                                filter="statusFilter" filterParams={{
                                    filterOptions: this.terminatedFilterOptions, filterKey: 'clusterType',
                                    filterResetCallback: this.resetFilter, filterType: 'equals'
                                }}></AgGridColumn>
                            <AgGridColumn headerName="Running Time" valueFormatter={this.clusterRuntimeFormatter} field="clusterRunTime" width={150} filter="timeFilter" filterParams={{
                                clearButton: true, filterKey: 'clusterRunTime', filterType: 'equals', resetTimeFilter: this.resetTimeFilter
                            }} ></AgGridColumn>
                            <AgGridColumn headerName="Usage Cost ($)" field="clusterCost" width={150} filter="agNumberColumnFilter" filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep' }}></AgGridColumn>
                            <AgGridColumn headerName="Start Time" field="clusterStartTs" width={150} filter="dateFilter" filterParams={{
                                clearButton: true, filterKey: 'clusterStartTs', filterType: 'equals', resetDateFilter: this.resetDateFilter
                            }} ></AgGridColumn>
                            <AgGridColumn headerName="End Time" field="clusterEndTs" width={150} filter="dateFilter" filterParams={{
                                clearButton: true, filterKey: 'clusterEndTs', filterType: 'equals', resetDateFilter: this.resetDateFilter
                            }}></AgGridColumn>
                            <AgGridColumn headerName="TOTAL PROCESSES" field="processCount" width={180}
                                filter="agNumberColumnFilter" filterParams={{ applyButton: true, clearButton: true, newRowsAction: 'keep' }}></AgGridColumn>
                            <AgGridColumn headerName="NO. OF SESSIONS" field="usrSessionCount" width={160} suppressFilter="true" ></AgGridColumn>
                            <AgGridColumn headerName="AVG USER SESSION" valueFormatter={this.avgUsrFormatter} field="avgUsrSession"
                                width={170} suppressFilter="true" ></AgGridColumn>
                        </AgGridReact>
                    </div>
                    <div>
                        {this.state.suppressForm}</div>
                    {this.state.clusterInfo}

                    <div>
                        {this.state.statusPopup}
                    </div>
                    <div>
                        {this.state.snoozeModal}
                    </div>
                    <div>
                        { this.state.showLoaderPopup }
                    </div>
                </div >
            </div>
        );
    }
}