import React from 'react';
import Services from "../services/Services.js";
import StatusPopup from './statuspopup.jsx';
import moment from 'moment';
import LoaderPopup from './loaderpopup.jsx';

class SuppressForm extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showInvalidMsg: false,
      userinfo: props.userinfo,
      newSuppressTime: '',
      statusPopup: null,
      clusterId: props.clusterId,
      onModalClose: props.onClose,
      fields: {},
      errors: {},
      showLoaderPopup: null
    };
  }

  getPutPayload = (event) => {
    let reqPayload = {
      snoozeDuration: this.hhmmsstoTotalSeconds(this.state.newSuppressTime),
      //clusterId: this.state.clusterId,
      requestedBy: this.state.userinfo.emailId
    };
    return reqPayload;
  }

  onSubmitClick = () => {
    this.handleClose();

  }

  componentWillUnmount = () => {
    document.body.style.overflow = "auto"
  }

  componentDidMount = () => {
    document.body.style.overflow = "hidden"

  }

  handleClickOutside = (event) => {
    if (this.modalRef && !this.modalRef.contains(event.target)) {
      this.handleClose();
    }
  }

  handleClose = () => {
    this.props.onClose(1);
  }

  setModalRef = (node) => {
    this.modalRef = node;
  }


  onChangeValue = (event) => {
    this.setState({ newSuppressTime: event.target.value });
  }

  snoozeClose = () => {
    this.setState({ showLoaderPopup: <LoaderPopup /> });
    Services.update('/api/utility/notify/' + this.props.clusterId, { snoozeDuration: parseInt(this.state.snoozeTime) * 3600, requestedBy: this.state.userinfo.emailId }).then(res => {
      if (res.status == 200) {
        this.setState({ showLoaderPopup: null });
        //this.hhmmsstoTotalSeconds(this.state.newSuppressTime)
        this.props.onClose(0);
      }
      console.log(res);
      this.refreshPage();
    })
      .catch(function (err) {
        this.setState({ showLoaderPopup: null });
        if (err && err.response.status === 500) {
          parent.updateNoDataTemplate(err);

        } else if (err && err.response.status === 409) {
          parent.duplicateMsg(err);
        }
      })
  }

  updateNoDataTemplate = (info) => {
    let msg = "Please Enter only in HH:MM:SS";
    this.setState({ showInvalidMsg: true, invalidUrlMsg: msg })
  }

  handleValidation = (event) => {
    if (this.state.newSuppressTime !== "undefined") {
      if (!this.state.newSuppressTime.match(/^[0-9\:]+$/)) {
        this.updateNoDataTemplate();
      }
    }
    else {
      return this.onCloseForm();
    }
  }

  hhmmsstoTotalSeconds = (hhmmssStr) => {
    var res = 0;
    var arr = hhmmssStr.split(":");
    res = parseInt(arr[0]) * 3600 + parseInt(arr[1]) * 60 + parseInt(arr[2]) * 1;
    return res;
  }


  render() {
    return (
       <div className="clusterInfoDiv">
        <div className="modal-screen" style={{ zIndex: 10 }}></div>
        <div className="hidden-sm-down position-relative" style={{ "overflowY": "hidden" }}>
        { this.state.showLoaderPopup == null &&  <div className="modal" style={{ zIndex: 10, top: "18%" }}>
            <div className="project-container" style={{ "overflowY": "hidden" }} ref={this.setModalRef}>
          <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                <div className="mlp-modal-heading">
                  <h2 className="mlp-no-margin pad-std heading-3">SNOOZE NOTIFICATION</h2>
                </div>
                <div className="pad-std mlp-modal-close">
                  <image onClick={this.handleClose}
                    data-toggle="tooltip-auto" title="Close"
                    className="mlp-cursor-pointer dls-glyph-close"></image>
                </div>
              </div> 
              <div style={{ "paddingTop": "10px", "height": "225px", "overflowX": "hidden", overflowY: "hidden" }} className="usecase-form background-white">
                <div className="row">
                  <div style={{ display: "flex" }}>
                    <label htmlFor="egselect" style={{ width: "100%", marginLeft: "11px" , "marginTop":"14px" }}>Enter hours: </label>
                    <input className="form-control" type="text" placeholder="Enter hours" style = {{"marginLeft": "-12px"}} onChange={(e) => this.setState({ snoozeTime: e.target.value })}></input>
                    { this.state.snoozeTime > 100 && <span style={{ paddingLeft: "32%", color: "#AD3117" }}>Hours should be less than 100 hrs</span>}
                  </div>
                  {this.state.showInvalidMsg && <div style={{ paddingLeft: "32%", color: "#AD3117" }}> {this.state.invalidUrlMsg}</div>}
                  <div>
                  </div>
                  <button className="btn-md" style={{ marginLeft: "30%", top: "65px" }} disabled={!this.state.snoozeTime || this.state.snoozeTime > 100 } onClick={() => { this.snoozeClose() }}>Snooze</button>
                </div>
              </div>
            </div>
          </div>}
        </div>
        <div>
          {this.state.statusPopup}
        </div>
        <div>
          {this.state.showLoaderPopup}
        </div>
      </div >
    );
  }
}

export default SuppressForm;




