import React, {Component} from "react";
import * as d3 from "d3";
import ScoreClusterGrid from "./ScoreClusterGrid.jsx"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import validator from "validator";
import moment from "moment";
import Services from "../services/Services.js";
import BarChart from './../components/barChart/BarChart.jsx';
import SunBurstChart from './../components/sunBurst/SunBurstChart.jsx';
import ErrorMsg from './../components/notifications/ErrorMsg.jsx';
import InfoMsg from './../components/notifications/InfoMsg.jsx';
import LoadingMsg from "../components/notifications/LoadingMsg.jsx";
import Admin from '../components/Admin.js';
import StackedBarChart from '../components/stackedbarchart.jsx';
import MyTransfers from '../components/MyTransfers.jsx';
import UniqueUsers from '../components/UniqueUsers.jsx';

export default class ScoreCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            minStartDate: moment().subtract(7, "days"),
            chartComponent: null,
            chartNote: null,
            dataGridComponent: null,
            dataGridRows: null,
            date: moment().subtract(1, "days"),
            frequency: 'DAILY',
            clusterStartTs: moment().subtract(1, "days").format('YYYY-MM-DD'),
            clusterEndTs: moment().subtract(1, "days").format('YYYY-MM-DD'),
            insight: 1,
            dimension: "DAILY",
            startDate: null,
            start_date: moment().subtract(1, "days"),
            endDate: null,
            filterKey: null,
            filterType: null,
            filterValue: null,
            components: {
                loadingRenderer: function (params) {
                    if (params.value !== undefined) {
                        return params.value;
                    } else {
                        return '<div class="progress-circle progress-indeterminate progress-sm"></div>';
                    }
                }
            }
        }
    }

    updateClusterGrid = () => {
        this.setState({showScoreClusterGrid: false});
    }

    showClusterGrid = (data) => {
        this.updateClusterGrid();
        this.setState({showScoreClusterGrid: true, scoreClusterData: data});
    }

    getChartNote = (str) => {
        var formatterFrequencyText = {
            "DAILY": "6 Days",
            "WEEKLY": "6 Weeks",
            "MONTHLY": "6 Months",
            "AGG": "12 Months"
        };
        return "Note : The data displayed in the above chart is for last " + formatterFrequencyText[str];
    }
    getTerminatedByData = () => {
        var parent = this;
        parent.setState({chartComponent: <div style={{textAlign: 'center'}}><LoadingMsg/></div>});
        Services.get("/api/insights/terminatedby?clusterEndTs=" + parent.state.clusterEndTs + "&frequency=" + parent.state.frequency)
            .then(function (response) {

                if (response.data && response.data.children > []) {
                    var chartComponent = <SunBurstChart data={response.data}
                                                        onDoubleClickChart={parent.doubleClickChart}
                                                        frequency={parent.state.frequency}/>
                    var chartNote = parent.getChartNote(parent.state.frequency);
                    parent.setState({chartComponent: chartComponent, chartNote: chartNote});

                } else {
                    chartComponent = <InfoMsg msg="No data found matching this criteria"/>
                    parent.setState({chartComponent: chartComponent, chartNote: ''});
                }
                window.scrollBy(0, 400);
            })
            .catch(function (error) {
                var chartComponent = <ErrorMsg msg={error.message}/>
                parent.setState({chartComponent: chartComponent, chartNote: ''});
            });
    }

    getCostSavingsData = () => {
        var parent = this;
        parent.setState({chartComponent: <div style={{textAlign: 'center'}}><LoadingMsg/></div>});
        Services.get("/api/insights/costsaves?clusterEndTs=" + parent.state.clusterEndTs + "&frequency=" + parent.state.frequency)
            .then(function (response) {
                if (response.data.clusters) {
                    var chartComponent = <BarChart data={response.data.clusters} visibility="true"
                                                   frequency={parent.state.frequency}/>
                    var chartNote = parent.getChartNote(parent.state.frequency);
                    parent.setState({chartComponent: chartComponent, chartNote: chartNote});
                }
                else {
                    chartComponent = <InfoMsg msg="No data found matching this criteria"/>
                    parent.setState({chartComponent: chartComponent, chartNote: ''});
                }
            })
            .catch(function (error) {
                var chartComponent = <ErrorMsg msg={error.message}/>
                parent.setState({chartComponent: chartComponent, chartNote: ''});
            })
    }

    getDataTransferData = (callback) => {
        var parent = this;
        // actual data
        var freq = (this.state.frequency === "AGG") ? 'DAILY' : this.state.frequency;
        Services.get("/api/dm/insightsdatatransfers?startDate=" + parent.state.clusterStartTs + "&endDate=" + parent.state.clusterEndTs + "&frequency=" + freq)
            .then(function (response) {
                if (callback) {
                    callback(response);
                }
                else {
                    if (response.data && Object.keys(response.data).length > 0) {
                        var chartComponent = <StackedBarChart data={response.data}/>
                        parent.setState({chartComponent: chartComponent, chartNote: null});
                    }
                    else {
                        chartComponent = <InfoMsg msg="No data found matching this criteria"/>
                        parent.setState({chartComponent: chartComponent, chartNote: null});
                    }
                }
            })
            .catch(function (error) {
                var chartComponent = <ErrorMsg msg={error.message}/>
                parent.setState({chartComponent: chartComponent});
            });
    }

    getUniqueUserData = () => {
        var parent = this;
        parent.setState({chartComponent: <div style={{textAlign: 'center'}}><LoadingMsg/></div>});
        Services.get("/api/dm/uniqueusers?startDate=" + parent.state.clusterStartTs + "&endDate=" + parent.state.clusterEndTs)
            .then(function (response) {


                if (response.data) {
                    var chartComponent = <UniqueUsers data={response.data}/>
                    parent.setState({chartComponent: chartComponent});
                } else {
                    chartComponent = <InfoMsg msg="No data found matching this criteria"/>
                    parent.setState({chartComponent: chartComponent, chartNote: ''});
                }

                if (response.data && Object.keys(response.data).length > 0) {
                    chartComponent = <UniqueUsers data={response.data}/>
                    parent.setState({chartComponent: chartComponent, chartNote: null});
                } else {
                    chartComponent = <InfoMsg msg="No data found matching this criteria"/>
                    parent.setState({chartComponent: chartComponent, chartNote: null});
                }

            })
            .catch(function (error) {
                var chartComponent = <ErrorMsg msg={error.message}/>
                parent.setState({chartComponent: chartComponent});
            });
    }

    getMyTransfersData = () => {
        var parent = this;
        parent.setState({chartComponent: <div style={{textAlign: 'center'}}><LoadingMsg/></div>});
        Services.get("/api/insights/myTransfers?clusterEndTs=" + parent.state.clusterEndTs + "&frequency=" + parent.state.frequency)
            .then(function (response) {
                if (response.data) {
                    var chartComponent = <myTransfers data={response.data}/>
                    parent.setState({chartComponent: chartComponent, chartNote: null});
                } else {
                    chartComponent = <InfoMsg msg="No data found matching this criteria"/>
                    parent.setState({chartComponent: chartComponent, chartNote: null});
                }
            })
            .catch(function (error) {
                var chartComponent = <ErrorMsg msg={error.message}/>
                parent.setState({chartComponent: chartComponent});
            });
    }

    onChangeInsight = (event) => {
        if (event.target.value === "4" || event.target.value === "5") {
            this.setState({insight: event.target.value, frequency: "AGG"})
        }
        else {
            this.setState({insight: event.target.value, frequency: "DAILY"})
        }
    }


    onChangeDimensions = (event) => {
        this.setState({frequency: event.target.value});
        this.enableDate(this.state.date, event.target.value);
    }

    ongenerateAgg = () => {
        if (this.state.frequency === "AGG") {
            this.getDataTransferData(function (resp) {
                var chartComponent = <InfoMsg msg="No data found matching this criteria"/>;
                if (resp && resp.data.length > 0) {
                    // aggregate data for all the dates.
                    var dataMatrix = {};
                    for (var i = 0; i < resp.data.length; i++) {
                        var keys = Object.keys(resp.data[i]);
                        for (var j = 0; j < keys.length; j++) {
                            if (keys[j].toLowerCase() === "date")
                                continue;
                            if (dataMatrix[keys[j]] == null) {
                                dataMatrix[keys[j]] = 0;
                            }
                            dataMatrix[keys[j]] += resp.data[i][keys[j]];
                        }
                    }
                    chartComponent = <MyTransfers data={dataMatrix}/>
                }
                parent.setState({chartComponent: chartComponent, chartNote: null});
            })
        }
        else {
            this.getDataTransferData();
        }
    }

    onGenerateClick = () => {
        var parent = this;
        this.setState({chartComponent: null, dataGridComponent: null});

        switch (this.state.insight.toString()) {
            case "1":
                this.getTerminatedByData();
                break;
            case "2":
                this.getCostSavingsData();
                break;
            case "3":
                this.ongenerateAgg();
                break;
            case "4":
                this.setState({chartComponent: null});
                var chartComponent = <MyTransfers data={null}/>
                this.setState({chartComponent: chartComponent, chartNote: null});
                break;
            case "5":
                this.getUniqueUserData();
                this.setState({chartComponent: chartComponent, chartNote: null});
                break;
            default:
                this.getDataTransferData();
                break;

        }
    }

    

    doubleClickChart = (d, frequency) => {
        var parent = this;
        var svcReq = {};
        var startDate = null;
        var endDate = null;

        if ("DAILY" === frequency || "WEEKLY" === frequency) {
            if (d.depth === 3) {
                if ("WEEKLY" === frequency) {
                    var week = d.parent.parent.data.name;
                    startDate = week.split("-")[0];
                    startDate = "2018-" + startDate.replace("/", "-");
                    endDate = week.split("-")[1];
                    endDate = "2018-" + endDate.replace("/", "-");
                    if (validator.isAfter(endDate, parent.state.clusterEndTs)) {
                        endDate = parent.state.clusterEndTs;
                    }
                    svcReq = {
                        startDate: startDate,
                        endDate: endDate
                    }
                } else {
                    svcReq = {
                        startDate: d.parent.parent.data.name,
                        endDate: d.parent.parent.data.name
                    }
                }

                endDate = moment(svcReq.endDate, "YYYY-MM-DD");
                if (endDate >= moment()) {
                    svcReq.endDate = moment().subtract(1, "days").format("YYYY-MM-DD");
                }

                var timeStamp = "&startTimestamp=" + svcReq.startDate + "_00:00:01&endTimestamp=" + svcReq.endDate + "_23:59:59";
                var uri = "/api/clusters/terminated_aws?by=" + d.data.name + "&type=" + d.parent.data.name + timeStamp;
                uri = uri + "&timezone=LOCAL&timezoneOffset=" + new Date().getTimezoneOffset();
                Services.get(uri, null)


                    .then(function (response) {
                        if (response.data && response.data.clusters) {
                            parent.setState({dataGridRows: response.data.clusters})
                            var dataGridComponent = <ScoreClusterGrid data={parent.state.dataGridRows}/>
                            parent.setState({dataGridComponent: dataGridComponent});

                        } else {
                            dataGridComponent = <InfoMsg msg="No data found matching this criteria"/>
                            parent.setState({dataGridComponent: dataGridComponent});
                        }
                        parent.refs.sunburstGrid.scrollIntoView()
                    })
                    .catch(function (error) {
                        var dataGridComponent = <ErrorMsg msg={error.message}/>
                        parent.setState({dataGridComponent: dataGridComponent});
                        parent.refs.sunburstGrid.scrollIntoView()
                    })
            }
            else {
                parent.setState({dataGridComponent: null});
            }
        }
        else {
            var dataGridComponent = null;
            parent.setState({dataGridComponent: dataGridComponent});
        }
    }
    onChangeDate = (date) => {
        this.setState({date: date});
        this.setState({clusterEndTs: date.format('YYYY-MM-DD')});
        this.enableDate(date);
    }
    onChangeStartDate = (date) => {
        this.setState({start_date: date});
        this.setState({clusterStartTs: date.format('YYYY-MM-DD')});
    }

    enableDate = (date, frequency) => {
        var freq = frequency == null ? this.state.frequency.toLowerCase() : frequency.toLowerCase();
        if (freq === "daily") {
            freq = "days";
        } else if (freq === "weekly") {
            freq = "weeks";
        }
        else {
            freq = "months";
        }

        var tmpdate = moment(date);
        tmpdate = tmpdate.subtract(6, freq);
        this.setState({minStartDate: tmpdate})
    }

    render() {
        return (
            <div>
                <Admin cluster={false} scorecard={true}/>
                { this.state.userinfo && !this.state.userinfo.hasAdminRole && <NavHeader selectedTab={"3"} /> }
                <div className="main-container">
                    <span className="heading-3">SCORE CARD</span>
                    <div className="row border">
                        <div className="flex-column pad-tb col-md-3 pad-lr" style={{'paddingTop': '20px'}}>
                            <label htmlFor="egSelect1">Insight</label>
                            <div className="select form-control" data-toggle="select">
                                <select id="insightSelect" onChange={this.onChangeInsight} value={this.state.insight}>
                                    <option value="1">Cluster Termination</option>
                                    <option value="2" disabled={this.state.frequency === "AGG" ? true : false}>Cost
                                        Saving
                                    </option>
                                    <option value="3">Data Management</option>
                                    {/*<option value="4">My Transfers</option>*/}
                                    <option value="5">Data Management Overview</option>
                                </select>
                            </div>
                        </div>
                        <div className="flex-column pad-tb col-md-3 pad-lr" style={{'paddingTop': '20px'}}>
                            <label htmlFor="egSelect">Dimensions</label>
                            <div className="select form-control">
                                <select id="egSelect" disabled={this.state.insight === "5"}
                                        onChange={this.onChangeDimensions} defaultValue={this.state.frequency}>
                                    <option value="DAILY" selected={this.state.frequency === "DAILY"}
                                            disabled={this.state.insight === "5"}>Daily
                                    </option>
                                    <option selected={this.state.frequency === "WEEKLY"} value="WEEKLY"
                                            disabled={this.state.insight === "5"}>Weekly
                                    </option>
                                    <option value="MONTHLY" selected={this.state.frequency === "MONTHLY"}
                                            disabled={this.state.insight === "5"}>Monthly
                                    </option>
                                    <option value="AGG" selected={this.state.frequency === "AGG"}
                                            disabled={this.state.insight === "2"}>Aggregate
                                    </option>
                                </select>
                            </div>
                        </div>

                        {(this.state.insight === "3" || this.state.insight === "4" || this.state.insight === "5") &&
                        <div className="flex-column pad-tb col-md-2 pad-lr" style={{'paddingTop': '20px'}}>
                            <label>Insight Start Date</label>
                            <DatePicker
                                selected={this.state.start_date}
                                maxDate={this.state.date}
                                onChange={this.onChangeStartDate}
                                // minDate={this.state.minStartDate}
                            />
                        </div>}

                        <div className="flex-column pad-tb col-md-2 pad-lr" style={{'paddingTop': '20px'}}>
                            <label>Insight End Date</label>
                            {(this.state.insight === "3" || this.state.insight === "4" || this.state.insight === "5") ?
                                <DatePicker selected={this.state.date}
                                            maxDate={moment()}
                                            onChange={this.onChangeDate}
                                            minDate={this.state.start_date}
                                />
                                :
                                <DatePicker selected={this.state.date}
                                            maxDate={moment()}
                                            onChange={this.onChangeDate}
                                />
                            }
                        </div>

                        <div className="form-group flex-column pad-tb col-md-2 pad-lr"
                             style={{'paddingTop': '52px', 'textAlign': 'center'}}>
                            <button onClick={this.onGenerateClick.bind(this)}>Generate</button>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="chart-container">
                        <div>{this.state.chartComponent}</div>
                        <div className="chart-note">{this.state.chartNote}</div>
                    </div>
                </div>
                <div ref="sunburstGrid">
                    {this.state.dataGridComponent}
                </div>
            </div>
        );
    }
}