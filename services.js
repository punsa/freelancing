import axios from 'axios';
import SessionValidation from '../containers/SessionValidation';



class Services {
    constructor() {
        this.api = axios.create({});
        SessionValidation.startTicker();

        this.api.interceptors.response.use(function (response) {
            SessionValidation.resetTimeout();
            return response;
        }, function (error) {
            return Promise.reject(error);
        });
        this.apiHASH = {};
    }

    get(url, payload) {
        var queryString = "";
        if (payload) {

            if (payload.startRow || payload.startRow >= 0) {
                queryString += "startRow=" + payload.startRow;
            }

            if (payload.endRow) {
                queryString += "&endRow=" + payload.endRow;
            }
            if (payload.startDate) {
                queryString += "&startDate=" + payload.startDate;
            }
            if (payload.endDate) {
                queryString += "&endDate=" + payload.endDate;
            }

            if (payload.filterType) {
                queryString += "&filterType=" + payload.filterType;
            }

            if (payload.filterKey) {
                queryString += "&filterKey=" + payload.filterKey;
            }

            if (payload.filterValue !== undefined && payload.filterValue !== null) {
                queryString += "&filterValue=" + payload.filterValue;
            }

            if (payload.sortType) {
                queryString += "&sortType=" + payload.sortType;
            }

            if (payload.sortBy) {
                queryString += "&sortBy=" + payload.sortBy;
            }

            if (payload.timezone) {
                queryString += (queryString == "" ? "" : "&") + "timezone=" + payload.timezone;
            }

            if (payload.timezoneOffset) {
                queryString += (queryString == "" ? "" : "&") + "timezoneOffset=" + payload.timezoneOffset;
            }

            url += "?" + queryString;

        }
        if(this.apiHASH && this.apiHASH[url]) {
            return Promise.resolve(this.apiHASH[url]);
        }
        else {
            return this.api.get(url).then(res => {
                this.apiHASH[url] = res;
                return Promise.resolve(res);
            })
            .catch(err => {
                return Promise.reject(err);
            })
        }
    }

    getLogout() {
        var uri = "/api/logout";
        return this.api.get(uri);
    }

    create(url, payload) {
        return this.api.post(url, payload);
    }

    update(url, payload) {
        return this.api.put(url, payload);
    }

    delete(url, payload = null) {
        if (payload)
            return this.api.delete(url, payload);
        else {
            return this.api.delete(url);
        }
    }

    getOwnerEmail(applicationId) {
        var uri = "/api/applications/" + applicationId;
        return this.get(uri);
    }

    getUserInfo() {
        var uri = "/api/users/info";
        return this.get(uri);
    }

    //cost saving
    getCostSavingsData(params) {
        var queryString = "";
        var uri = "/api/insights/costsaves";
        for (var key in params) {
            queryString += key + "=" + params[key] + "&";
        }
        uri += "?" + queryString
        return this.get(uri);
    }
}

export default new Services;
