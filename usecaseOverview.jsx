import React from 'react';
import "./UsecaseForm.css";
import "./usecaseOverview.css";
import "./commentbox.css";
import Services from "../services/Services.js";
import StatusPopup from './statuspopup';

class UsecaseOverview extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            userInfo: props.userInfo,
            useCaseForm: props.data,
            child1status: 'none',
            prcGroupsData: null,
            prcGrps: "",
            workstreamGroup: [],
            statusPopup: null
        };

        this.setWorkstreamGroup(true);

        if (this.state.useCaseForm.prcGroups && this.state.useCaseForm.prcGroups.length > 0) {
            this.state.prcGrps = this.state.useCaseForm.prcGroups.join(", ");
        }
        else {

            this.state.prcGrps = "";
        }

        this.getprcgroupData();
    }


    componentDidMount() {
        document.body.style.overflow = 'hidden';
    }

    componentWillUnmount() {
        document.body.style.overflow = 'auto';
    }

    onPopupClose = (status) => {
        this.setState({statusPopup: null});
        if (status === 1) {
            this.onConfirmDelete();
        }
    }

    getprcgroupData = () => {
        if (!this.state.prcGroupsData && this.state.prcGrps !== "") {
            var endpoint = "/api/usecases/prcgroups_users/" + this.state.prcGrps;
            Services.get(endpoint, null).then(res => {
                this.setState({prcGroupsData: res["data"]});
            })
                .catch(err => {
                })
        }

    }

    getUserrole = (email, callback) => {
        var endpoint = "/api/users/role/?email=" + email;
        Services.get(endpoint, null).then(res => {
            callback(true, res["data"]["adsId"]);
        })
            .catch(err => {
                callback(false, "");
            })
    }

    getPrcgroupsData = (callback) => {

        var endpoint = "/api/usecases/prcgroups_users/IMDC-SPLK-500000723-CDAT-READONLY-E1";
        Services.get(endpoint, null).then(res => {
            callback(res["data"])
        })
            .catch(err => {
            })
    }

    setWorkstreamGroup = (initial = false) => {
        var workstreamMap = {};
        for (var i = 0; i < this.state.useCaseForm.comments.length; i++) {
            if (!workstreamMap[this.state.useCaseForm.comments[i].workStreamName]) {
                workstreamMap[this.state.useCaseForm.comments[i].workStreamName] = [];
            }
            workstreamMap[this.state.useCaseForm.comments[i].workStreamName].push({
                author: this.state.useCaseForm.comments[i].author,
                comment: this.state.useCaseForm.comments[i].comment,
                timestamp: this.state.useCaseForm.comments[i].timestamp
            });
        }
        var workstreamKeys = Object.keys(workstreamMap);
        var workstreamArr = [];
        for (i = 0; i < workstreamKeys.length; i++) {
            workstreamArr.push({workstreamName: workstreamKeys[i], comments: workstreamMap[workstreamKeys[i]]});
        }
        if (initial) {
            this.state.workstreamGroup = workstreamArr;
        }
        else {
            this.setState({workstreamGroup: workstreamArr});
        }

    }

    getSplunkAccess = () => {
        if (this.state.userInfo["emailId"]) {
            Services.get("api/usecases/prcgroups_users/IMDC-SPLK-500000723-CDAT-READONLY-E1").then(res => {
            });
        }
    }

    splunkAccess = (email) => {
        var splunkAccess = false;


        this.getPrcgroupsData((res) => {
            for (var i = 0; i < res.length; i++) {
                if (res[i].emailId === this.state.userInfo.emailId) {
                    splunkAccess = true;
                    this.getUserrole(email, (valid, adsId) => {
                        if (valid) {
                            var url = "https://splunk-isl001-gui-e1-vip.phx.aexp.com/en-US/app/splunk_app_aws/" +
                                "monthly_billing?form.billingAccountId=*&form.accountId=*&form.region=*&form.currency=USD&form.tags=Owner%3D" + adsId + "@ads.aexp.com";
                            window.open(url, "_blank");
                            return true;
                        }
                        else {
                            var ele = <StatusPopup onClose={this.onPopupClose}
                                                   message="This is an Invalid Owner's Email or not found in splunk dashboard"/>
                            this.setState({statusPopup: ele});
                        }
                    });
                    if (splunkAccess) {
                        break;
                    }
                }
            }
            if (!splunkAccess) {
                var ele = <StatusPopup onClose={this.onPopupClose} message="You do not have access to splunk!!"/>
                this.setState({statusPopup: ele});
            }
        })
        return false;
    }

    changeChild1Status = () => {
        if (this.state.child1status === 'none') {
            this.setState({child1status: 'block'});
        }
        else {
            this.setState({child1status: 'none'});
        }
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({useCaseForm: nextProps.data});
    }

    componentWillMount = () => {
        var tmpusecaseform = this.state.useCaseForm;
        if (this.state.useCaseForm.cmptEnv && this.state.useCaseForm.cmptEnv.length > 0) {
            tmpusecaseform.cmptEnv = this.state.useCaseForm.cmptEnv.join(",");
        }
        else {
            tmpusecaseform.cmptEnv = "";
        }
        this.setState({useCaseForm: tmpusecaseform});
    }

    handleClickOutside = (event) => {
        if (this.modalRef && !this.modalRef.contains(event.target)) {
            this.handleClose();
        }
    }

    handleClose = (status) => {
        this.props.onClose(status);
    }

    onOkClick = () => {
        this.handleClose();

    }

    setModalRef = (node) => {
        this.modalRef = node;
    }

    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen style={{ width: 50px}}"></div>
                <div className="hidden-sm-down position-relative" style={{"minHeight": 700}}>
                    <div className="modal" style={{top: "5%", left: "165px", width: "75%"}}>
                        <div className="container" ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">UseCase Overview</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.handleClose}
                                           data-toggle="tooltip-auto" title="Close"
                                           className="mlp-cursor-pointer dls-glyph-close"></image>
                                </div>
                            </div>
                            <div className="usecase-form background-white"
                                 style={{"padding": "15px", "height": "500px"}}>
                                {/* name */}
                                <div class="overflow-auto" style={{"marginTop": "0px"}}>
                                    <div className="row overview-rowmargin">
                                        <div className={"col-md-6 "}>
                                            <label htmlFor="name">Name : </label>&nbsp;
                                            <span>{this.state.useCaseForm.useCaseName}</span>
                                        </div>

                                        <div className="col-md-6">
                                            <label htmlFor="egSelect">Type : </label>&nbsp;
                                            <span
                                                style={{"textTransform": "uppercase"}}>{this.state.useCaseForm.useCaseType}</span>
                                        </div>
                                    </div>

                                    {/* description */}
                                    <div className="row overview-rowmargin">
                                        <div className="col-md-6">
                                            <label htmlFor="description">Description : </label>&nbsp;
                                            <span>{this.state.useCaseForm.useCaseDesc}</span>
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="requesterEmailId">Requestor's Email : </label>&nbsp;
                                            <span>{this.state.useCaseForm.requesterEmailId}</span>
                                        </div>
                                    </div>

                                    {/* Application ID / AIM ID / CAR ID  */}
                                    <div className="row overview-rowmargin">
                                        <div className={"col-md-6"}>
                                            <label htmlFor="app_id">Application ID : </label> &nbsp;
                                            <span>{this.state.useCaseForm.applicationId}</span>
                                        </div>
                                        <div className={"col-md-6"}>
                                            <label htmlFor="email">Owner's Email : </label> &nbsp;
                                            <span>{this.state.useCaseForm.ucOwnerEmailId}</span>
                                        </div>
                                    </div>

                                    {/*useCase status*/}
                                    <div className="row overview-rowmargin">
                                        <div className="col-md-6">
                                            <label htmlFor="egSelect">Status : </label> &nbsp;
                                            <span>{this.state.useCaseForm.onBoardStatus}</span>
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="environment">Environment : </label> &nbsp;
                                            <span>{this.state.useCaseForm.cmptEnv}</span>
                                        </div>
                                    </div>

                                    <div className="row overview-rowmargin">
                                        <div className="col-md-6">
                                            <label htmlFor="egSelect">Role : </label> &nbsp;
                                            <span>{this.state.useCaseForm.awsIamRole}</span>
                                        </div>
                                    </div>

                                    <div class="border">
                                        <div class="accordion border-b">
                                            <div
                                                class="collapsible accordion-toggle flex-align-center dls-accent-white-01-bg"
                                                data-toggle="accordion"
                                                role="button" aria-haspopup="true" aria-expanded="false" tabIndex="0">
                                                <span class="collapsible-caret"></span>
                                                <span><label>PRC Groups</label></span>
                                            </div>
                                            <div class="accordion-content display-none" role="tabpanel">
                                                <div class="flex-align-center border-dark-t pad dls-accent-gray-01-bg">
                                                    {
                                                        <div key="prcgrp" className="prcgrp-div" id="prcgrp">

                                                            <p style={{"fontWeight": "bold"}}>{this.state.prcGrps}</p>
                                                            {
                                                                (this.state.prcGroupsData) && (
                                                                    <div class="stack-md-down">
                                                                        <div key="prcGroupsData"
                                                                             style={{"overflowX": "auto"}}>
                                                                            <div className="table-fluid">
                                                                                <table
                                                                                    className="table table-bordered border table-group pad-lr">
                                                                                    <thead>
                                                                                    <tr style={{"background": "#e0f2fa"}}>
                                                                                        {/*<div className="header" style={{"fontWeight":"bold"}}>*/}
                                                                                        <th style={{"fontWeight": "bold"}}>Usecase
                                                                                            group
                                                                                        </th>
                                                                                        <th style={{"fontWeight": "bold"}}>First
                                                                                            Name
                                                                                        </th>
                                                                                        <th style={{"fontWeight": "bold"}}>Last
                                                                                            Name
                                                                                        </th>
                                                                                        <th style={{"fontWeight": "bold"}}>Email
                                                                                            Id
                                                                                        </th>
                                                                                        <th style={{"fontWeight": "bold"}}>Cost</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    {
                                                                                        this.state.prcGroupsData.map((user, index) => {
                                                                                            return (
                                                                                                <tr key={"prcgrptr" + index}
                                                                                                    className="table-row-link"
                                                                                                    href={"#data" + index}>
                                                                                                    <td><pre style={{
                                                                                                        "fontFamily": "Helvetica Neue,Helvetica,Arial,sans-serif",
                                                                                                        "fontSize": ".9375rem",
                                                                                                        "marginBottom": "0rem"
                                                                                                    }}>
                                                                                                        {user.prcGroup}</pre>
                                                                                                    </td>
                                                                                                    <td className="text-align-left">{user.firstName}</td>
                                                                                                    <td className="text-align-left">{user.lastName}</td>
                                                                                                    <td>{user.emailId}</td>
                                                                                                    <td><a
                                                                                                        onClick={(e) => this.splunkAccess(user.emailId)}
                                                                                                        target='_blank'>Splunk
                                                                                                        Cost</a></td>
                                                                                                </tr>
                                                                                            )
                                                                                        })
                                                                                    }
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>)
                                                            }
                                                        </div>

                                                    }
                                                    {(!this.state.useCaseForm.prcGroups || this.state.useCaseForm.prcGroups.length === 0 || this.state.useCaseForm.prcGroups[0] == "" || this.state.useCaseForm.prcGroups[0] == null) &&
                                                    (
                                                        <div>No PRC Groups to display.</div>
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {
                                        this.state.workstreamGroup.map((workstream, indexer) => {
                                            return (
                                                <div class="border" style={{"marginTop": "10px"}}
                                                     key={'workstream' + indexer}>
                                                    <div class="accordion border-b">
                                                        <div
                                                            class="collapsible accordion-toggle flex-align-center dls-accent-white-01-bg"
                                                            data-toggle="accordion" role="button" aria-haspopup="true"
                                                            aria-expanded="false" tabindex="0">
                                                            <span class="collapsible-caret"></span>
                                                            <span><label>{workstream.workstreamName}</label></span>
                                                        </div>
                                                        <div class="accordion-content display-none" role="tabpanel">
                                                            <div
                                                                class="flex-align-center border-dark-t pad dls-accent-gray-01-bg">
                                                                <div class="row stack-md-down">
                                                                    <div class="col-xs-12 col-md-12 stack">
                                                                        {
                                                                            workstream.comments.map(function (comment, index) {
                                                                                return (
                                                                                    <div key={"commentdiv" + index}
                                                                                         style={{
                                                                                             "margin": "10px",
                                                                                             "borderRadius": "15px"
                                                                                         }}
                                                                                         className="comment-div"
                                                                                         id={index + "comment"}>
                                                                                        <div className="row" style={{
                                                                                            "marginRight": "12px",
                                                                                            "marginLeft": "12px",
                                                                                            "border": "1px #e9e9e9"
                                                                                        }}>
                                                                                            <div className="col-md-12"
                                                                                                 style={{
                                                                                                     "fontWeight": "bold",
                                                                                                     "marginTop": "5px"
                                                                                                 }}>
                                                                                                <span
                                                                                                    style={{"color": "#006FCF"}}>{comment.author}</span>
                                                                                                <span style={{
                                                                                                    "float": "right",
                                                                                                    "fontWeight": "bold"
                                                                                                }}>{comment.timestamp}</span>
                                                                                            </div>
                                                                                            <div className="col-md-12"
                                                                                                 style={{"padding": "10px"}}>
                                                                                                {comment.comment}
                                                                                            </div>
                                                                                        </div>
                                                                                        <br/>
                                                                                    </div>
                                                                                )
                                                                            })
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                    <div className="row">
                                        <div className="col-md-4"></div>
                                        <div className="col-md-2">
                                            <button className="btn btn-primary"
                                                    style={{"marginLeft": "60px", "marginTop": "60px"}}
                                                    onClick={this.onOkClick}>OK
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UsecaseOverview;

