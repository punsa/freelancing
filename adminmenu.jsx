import React, { Component } from 'react';
import { Link } from 'react-router-dom';
export default class AdminMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="nav-primary">
                <div className="nav nav-bordered nav-chevron" role="navigation" data-toggle="nav"
                     aria-current="horizontal">
                    <div className="nav-overlay"></div>
                    <ul className="nav-menu">
                      <li className="nav-item" role="presentation">
                          <Link to="/app/admin/clusters" className="nav-link caret" role="navigation-section"
                                aria-selected={this.props.cluster}>AUTUMN</Link>
                      </li>
                      <li className="nav-item" role="presentation">
                          <Link to="/app/admin/datamanagement" className="nav-link caret" role="navigation-section"
                      aria-selected={this.props.datamanagement}>Data Management</Link>
                      </li>
                      <li className="nav-item" role="presentation">
                          <Link to="/app/admin/scorecard" className="nav-link caret" role="navigation-section"
                      aria-selected={this.props.scorecard}>Score Card</Link>
                      </li>
                      <li className="nav-item" role="presentation">
                          <Link to="/app/admin/usecase" className="nav-link caret" role="navigation-section"
                      aria-selected={this.props.usecase}>Use Case</Link>
                      </li>
                    </ul>
                </div>
            </div>
        )
    }
}
