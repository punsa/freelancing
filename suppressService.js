'use strict'

const path = require('path')
const express = require('express')
const router = express.Router()
const validator = require('validator')
const mlp = require('./mlpService');
const crypto = require('./cryptoService');
const appConfig = require('./../config/app-config');
const env = process.env.EPAAS_ENV || 'e0';
var moment = require('moment');
const clusterForm = require('./../config/config.json');
const mlpLogger = require('./loggerService');
const logger = mlpLogger.createLogger();


function setFilters(filterType, filterValue) {

    logger.debug("filtertype: :- " + filterType);
    logger.debug("filtervalue: :- " + filterValue);
    if (filterType == null || filterValue == null)
        return [filterType, filterValue];
    var pattern = null;
    var a = filterValue;
    switch (filterType) {
        case "equals": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "notEqual": pattern = new RegExp("^" + a + "$", "i");
            break;
        case "startsWith": pattern = new RegExp("^" + a + ".*$", "i");
            break;
        case "endsWith": pattern = new RegExp("^.*" + a + "$", "i");
            break;
        case "contains": pattern = new RegExp("^.*" + a + ".*$", "i");
            break;
        default: logger.debug("Not Found Filter Type : " + filterType);
    }
    if (pattern == null)
        return [filterType, filterValue];


    logger.debug("filterVal :- " + filterValue + " does not match any key in dmTransferStatusMapper");
    return [filterType, filterValue];
}

function updateSuppressDetails(token, clusterId, scvReq, callback) {
  let endPoint = "/api/v1/utility/notify/" + clusterId;
  mlp.executeRequest(endPoint, 'PUT', token, scvReq, callback);
}

router.put('/notify/:clusterId', function (req, res) {
  try {
      var token = req.userContext.tokens['access_token'];
      logger.info("in getttt" + req.params.clusterId);
      var endPoint = "/api/v1/utility/notify/"+ req.params.clusterId;
      mlp.putCall(endPoint, token, req.body, function (err, results) {
        if (!err) {
          logger.debug(results);
          var status = '200';
          if (results && results.statusCode == 409) {
              status = results.statusCode;
          }
          res.status(status).send('{"status": "created"}');
      } else {
              res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
          }
      })
  }
  catch (e) {
      logger.error('Error ' + e);
      res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
  }
})

module.exports = router