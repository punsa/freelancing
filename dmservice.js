'use strict'

const path = require('path');
const express = require('express');
const router = express.Router();
const validator = require('validator');
const logger = require('winston');
const mlp = require('./mlpService');
const crypto = require('./cryptoService');
const appConfig = require('./../config/app-config');
const env = process.env.EPAAS_ENV || 'e0';
var moment = require('moment');

function setFilters(filterType, filterValue) {
    var dmTransferStatusMapper = {
        'SUBMITTED': 'SUBMITTED',
        'IN PROGRESS': 'IN_PROGRESS',
        'TRANSFER FAILED': 'XFER_FAILED',
        'TRANSFER HOLD': 'XFER_ON_HOLD',
        'TRANSFER COMPLETED': 'XFER_COMPLETED',
        'CREATED': 'CREATED',
        'PIICHECK COMPLETED': 'PIICHECK_COMPLETED',
        'PIICHECK HOLD': 'PIICHECK_HOLD',
        'TRANSFER AUTO REJECT': 'FAILED_LONG_PNDG_APRVL',
        'TRANSFER INITIATION FAILED': 'XFER_INITIATION_FAILED',
        'TRANSFER REJECTED': 'XFER_REJECTED',
        'TRANSFER HOLD': 'XFER_ON_HOLD_FIRST_REMINDER',
        'TRANSFER FAILED':'CLI_EXECUTION_FAILED'
    };
    logger.debug("filtertype: :- " + filterType);
    logger.debug("filtervalue: :- " + filterValue);
    if (filterType == null || filterValue == null)
        return [filterType, filterValue];
    var pattern = null;
    var a = filterValue;
    switch (filterType) {
        case "equals":
            pattern = new RegExp("^" + a + "$", "i");
            break;
        case "notEqual":
            pattern = new RegExp("^" + a + "$", "i");
            break;
        case "startsWith":
            pattern = new RegExp("^" + a + ".*$", "i");
            break;
        case "endsWith":
            pattern = new RegExp("^.*" + a + "$", "i");
            break;
        case "contains":
            pattern = new RegExp("^.*" + a + ".*$", "i");
            break;
        default:
            logger.debug("Not Found Filter Type : " + filterType);
    }
    if (pattern == null)
        return [filterType, filterValue];
    for (var key in dmTransferStatusMapper) {
        if (pattern.test(key)) {
            if (filterType === "equals")
                return ["contains", dmTransferStatusMapper[key]];
            return [filterType, dmTransferStatusMapper[key]];
        }
    }

    logger.debug("filterVal :- " + filterValue + " does not match any key in dmTransferStatusMapper");
    return [filterType, filterValue];
}

function convertWeeknumberIntoDateRange(week_no) {
    var startdate = moment("01-01-2018").add(week_no - 1, "weeks").format('MM/DD');
    var enddate = moment("01-01-2018").add(week_no, "weeks").subtract(1, "days").format('MM/DD');
    return (startdate + "-" + enddate);
}

function formatDataTransfers(arr) {
    var dmTransferStatusMapper = {
        'SUBMITTED': 'SUBMITTED',
        'IN_PROGRESS': 'IN PROGRESS',
        'XFER_FAILED': 'TRANSFER FAILED',
        'XFER_ON_HOLD': 'TRANSFER HOLD',
        'XFER_COMPLETED': 'TRANSFER COMPLETED',
        'CREATED': 'CREATED',
        'PIICHECK_COMPLETED': 'PIICHECK COMPLETED',
        'PIICHECK_HOLD': 'PIICHECK HOLD',
        'FAILED_LONG_PNDG_APRVL': 'TRANSFER AUTO REJECT',
        'XFER_INITIATION_FAILED': 'TRANSFER INITIATION FAILED',
        'XFER_REJECTED': 'TRANSFER REJECTED',
        'XFER_ON_HOLD_FIRST_REMINDER': 'TRANSFER HOLD',
        'CLI_EXECUTION_FAILED':'TRANSFER FAILED'

    }
    for (var i = 0; i < arr.length; i++) {
        arr[i]["dmTransferStatus"] = dmTransferStatusMapper[arr[i]["dmTransferStatus"]];
    }
    return arr;
}


function getDataTransfer(userId, svcReq, callback) {
    let endpoint = "/api/v1/dm/dataTransfer";
    if (userId !== 'ALL') {
      endpoint += "/user/" + userId;
    }
    if (svcReq) {
      endpoint = endpoint + "?"
    }
    for (var key in svcReq) {
      if (svcReq[key]) {
        endpoint = endpoint + key + "=" + svcReq[key] + "&"
      }
    }
    mlp.executeRequest(endpoint, 'GET', '1234567', {}, callback)
  
  }
  
  function updateDMdetails(transferId, svcReq, callback) {
    let endPoint = '/api/v1/dm/dataTransfer/' + transferId
    mlp.executeRequest(endPoint, 'PUT', '', svcReq, callback);
  }
  
  function getDMdetails(transferId, svcReq, callback) {
    let baseURL = '/api/v1/dm/dataTransfer/' + transferId;
    if (svcReq && svcReq.extended !== undefined) {
      baseURL += "?extended=" + svcReq.extended;
    }
    mlp.executeRequest(baseURL, 'GET', '1234567', {}, callback)
  }
  

router.use(function validate(req, res, next) {
    logger.info('Request : ' + req.url);
    next()
})


router.get('/dataTransfer/user/:id', function (req, res) {
    logger.info("IN route /datatransfer");
    try {
        if (!validateFilterInput(req)) {
            res.status('400').send('{"status" : "failed", "statusCode" :"MLP701"}');
            return;
        }

        var filterType = null;

        if (req.query.filterType && req.query.filterType === 'lessThanOrEqual') {
            filterType = "lessThanEqual"
        } else if (req.query.filterType && req.query.filterType === 'greaterThanOrEqual') {
            filterType = "greaterThanEqual"
        } else {
            filterType = req.query.filterType;
        }

        var obj = setFilters(filterType, req.query.filterValue);

        var startTimestamp = null;
        var endTimestamp = null;

        let svcReq = {
            startRow: req.query.startRow, endRow: req.query.endRow,
            filterKey: req.query.filterKey, filterType: obj[0], filterValue: obj[1],
            sortBy: req.query.sortBy, sortType: req.query.sortType,
            startTimestamp: startTimestamp, endTimestamp: endTimestamp
        }

        getDataTransfer(req.params.id, svcReq, function (err, results) {
            if (!err) {
                results = JSON.parse(results);
                if (results) {
                    results = formatDataTransfers(results);
                }
                results = mlp.convertTimeFieldAsTimezone(results, ["dmTransferStartTime", "dmTransferEndTime"], req.query.timezone, req.query.timezoneOffset);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});

router.get('/dataTransfer/:transferId', function (req, res) {
    //?extended=true
    try {
        logger.info("IN GET ##rrrrrrrr###" + req.params.transferId, req.query.extended);
        getDMdetails(req.params.transferId, req.query, function (err, results) {
            if (!err) {
                results = JSON.parse(results);
                logger.info(results);
                results["extendedTransResult"]["moduleStats"] = mlp.convertTimeFieldAsTimezone(results["extendedTransResult"]["moduleStats"],["startTime", "endTime"],req.query.timezone, req.query.timezoneOffset);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })

    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})


router.put('/dataTransfer/:transferId', function (req, res) {
    try {
        logger.info("in gettt" + req.params.transferId);
        var endPoint = "/api/v1/dm/dataTransfer/" + req.params.transferId;
        mlp.putCall(endPoint, req.body, function (err, results) {
          if (!err) {
            logger.debug(results);
            var status = '200';
            if (results && results.statusCode == 403) {
                status = results.statusCode;
            } else if(results && results.statusCode == 401) {
              status = results.statusCode;
            }
            res.status(status).send('{"status": "created"}');
        } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/update', function (req, res) {
    try {
        var data = JSON.parse(req.query.data);
        mlp.putDMdetails(req.query.transferId, data, function (err, results) {
            if (!err) {
                res.status('201').send('{"status":  "updated"}');
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        });
    }
    catch (e) {
        logger.error('Error ' + e);
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/uniqueusers', function (req, res) {
    try {
        let endpoint = "/api/v1/dm/uniqueusers?startDate=" + req.query.startDate + "&endDate=" + req.query.endDate;
        mlp.getCall(endpoint, function (err, results) {
            if (!err) {
                results = JSON.parse(results);
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
})

router.get('/insightsdatatransfers', function (req, res) {
    try {
        let endpoint = "/api/v1/dm/insightsdatatransfers?startDate=" + req.query.startDate + "&endDate=" + req.query.endDate + "&frequency=" + req.query.frequency;
        mlp.getCall(endpoint, function (err, results) {
            if (!err) {
                results = JSON.parse(results);
                if (results && req.query.frequency === "WEEKLY") {
                    for (var i = 0; i < results.length; i++) {
                        var tmpDate = parseInt(results[i].date);
                        results[i].date = convertWeeknumberIntoDateRange(tmpDate);
                    }
                }
                res.status('200').send(results);
            } else {
                res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
            }
        })
    } catch (e) {
        res.status('500').send('{"status" : "failed", "statusCode" :"MLP701" }');
    }
});


function validateFilterInput(req) {
    var data = req.query;
    var filterValueRegex = /^[a-zA-Z0-9\-\_~\.@:/]+$/
    if ((!data.filterKey || data.filterKey && data.filterKey.length <= 25 && validator.isAlpha(data.filterKey)) &&
        (!data.filterValue || data.filterValue && data.filterValue.length <= 50 && filterValueRegex.test(data.filterValue)) &&
        (!data.filterType || data.filterType && data.filterType.length <= 25 && validator.isAlpha(data.filterType)) &&
        (!data.startRow || data.startRow && validator.isNumeric(data.startRow) && data.startRow <= 100000) &&
        (!data.endRow || data.endRow && validator.isNumeric(data.endRow) && data.endRow <= 100000) &&
        (!data.sortBy || data.sortBy && data.sortBy.length <= 25 && validator.isAlpha(data.sortBy)) &&
        (!data.sortType || data.sortType && data.sortType.length <= 5 && validator.isAlpha(data.sortType))) {
        return true;
    } else {
        logger.info("Validation Failed");
        return false;
    }
}


module.exports = router
